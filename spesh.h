/*
 *   Copyright (c) 2023,2024 Talas
 *   Copyright (c) 2022,2023 ktye
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 *  Based on ktye/k and k+, which are in the Public Domain.
 */
#include<stdlib.h>
#include<stdint.h>
#include<string.h>
#include "mathlib.h"
#define SPESH_MAX_MEM 30
#define SPESH_MAX_RECURSION 16
#define SPESH_MAX_MODULES 8
typedef int32_t I;
typedef int64_t J;
typedef uint32_t UI;
typedef uint64_t UJ;
typedef char C;
typedef double F; //64 bit double
typedef uint64_t K; // K value
typedef uint8_t T; // K type
typedef struct { K a,b,x,y; I p,e,i,j,op; } frame_t;
static K key_;
static K val_[SPESH_MAX_MODULES+1];
static I mod_=0;
//  20..127  free list
// 128..135  src (aligned)
// 256..511  stack
#define POS_SRC 128
#define POS_STACK 256
#define QUOTE 288
static C *M_;
static I *I_;
static UJ*U_;
static C ERR[256];
static I memorysize_, memorycount_;
static K loc_=0, xyz_=0;
static I PP=0, PE=0, SP=0, SRCP=0, PS=0; // parse position/end, stack position, src pointer
static I IP=0, IE=0; // execution position/end
static I rand_=0;
static frame_t frames[SPESH_MAX_RECURSION+1];
static frame_t*frame=&frames[0];
static frame_t*frame_end=&frames[SPESH_MAX_RECURSION];

void panic(I ecode); // Must be defined by user.
#ifdef SPESH_IO_VERBS // Optional IO provided by user
K readio(K x); // <`myfile
K writeio(K x, K y); // `myfile<"hello world"
#endif

enum { E_Err=1, E_Type, E_Value, E_Index, E_Length, E_Rank, E_Parse,
  E_Stack, E_Grow, E_Unref, E_Io, E_Limit, E_Nyi };

//#include <sys/mman.h>
static I Memory(void){
    if(SPESH_MAX_MEM>30) return 0;
    memorysize_=1<<(SPESH_MAX_MEM-16);
    U_=(UJ*)calloc((size_t)memorysize_,64*1024);
    //U_=(UJ*)mmap(0,memorysize_,PROT_READ|PROT_WRITE,MAP_NORESERVE|MAP_ANONYMOUS|MAP_PRIVATE,-1,0);
    if(!U_) return 0;
    M_=(C*)U_; I_=(I*)U_;
    for(I i=9,p=1<<9; i<SPESH_MAX_MEM; ++i,p*=2) I_[i]=p;
    memorycount_=14;
    return 1;
}

static void Memorycopy(I dst, I src, I n){ memcpy(M_+dst, M_+src, (size_t)n); }
static void Memoryfill(I p, I v, I n){ memset(M_+p, (int)v, (size_t)n); }
#define Iclz(x) ((I)__builtin_clz((unsigned int)(I)(x)))


static I bucket(I size){
    I r=32-Iclz(15+size);
    return (r<5) ? 5 : r;
}

K trap(I x);
static I alloc(I n, I s){
    I t,i,a,u;
    if(((J)(n)*(J)(s))>0x7FFFFFFFll) trap(E_Grow);
    i=t=bucket(n*s);
    while(!(a=I_[i])){ if(++i>=SPESH_MAX_MEM) trap(E_Grow); }
    memorycount_=(i>=memorycount_) ? i+1 : memorycount_;
    I_[i]=I_[a>>2];
    for(I j=i-1; j>=t; j--){
      u=a + (1<<j);
      I_[u>>2]=I_[j];
      I_[j]=u;
    }
    if(a&31) trap(E_Unref); // memory corruption
    return a;
}

static void mfree(I x, I bs){
    x-=16; I t=bs;
    if(x&31) trap(E_Unref);
    I_[x>>2]=I_[t];
    I_[t]=x;
}

#define xC(x)  ((C*)(M_+(I)x))
#define xI(x)  ((I*)(M_+(I)x))
#define xK(x)  ((K*)(M_+(I)x))
#define xJ(x)  ((J*)(M_+(I)x))
#define xF(x)  ((F*)(M_+(I)x))

static I sz(T t){
    if(t<16) return 8;
    if(t<19) return 1;
    if(t<21) return 4;
    return 8;
}

static K mk(T t, I n){
    if(t<17) return trap(E_Value);
    K r=(K)(t)<<59;
    I x=16+alloc(n,sz(t)); //nnnn00F0rrrr
    xI(x)[-1]=1; xI(x)[-3]=n; xC(x)[-6]=0;
    return r | (K)(x);
}

static const I nai=(I)0x80000000; // 0N

// typeof(x K): t=x>>59
// isatom:      t<16
// isvector:    t>16
// isflat:      t<22
// basetype:    t&15  0..9
// istagged:    t<5
// haspointers: t>5   (recursive unref)
// elementsize: $[t<19;1;t<21;4;8]

//                  name     bytes  vector
#define T_ct  2 //  char     1      18
#define T_it  3 //  int      4      19
#define T_st  4 //  symbol   4      20
#define T_ft  5 //  float    8      21

#define T_cf  10 // comp    (8)
#define T_df  11 // derived (8)
#define T_pf  12 // proj    (8)
#define T_lf  13 // lambda  (8)
#define T_xf  14 // native
//                           atom
#define T_Ct  18 // vec<c>   2
#define T_It  19 // vec<i>   3
#define T_St  20 // vec<s>   4
#define T_Ft  21 // vec<f>   5

#define T_Lt  23 // list
#define T_Dt  24 // dict
#define T_Tt  25 // table

//                                    1111111111222222
//                          01234567890123456789012345
static const C Types[27] = "v0cisf0000mdplx000CISF0LDT";
static const C Verbs[24] = ":+-*%&|<>=~!,^#_$?@.'/\\";
//                          00000000011111111112222
//                          12345678901234567890123

static I isfunc(T t){ return (I)(t==0 || (t<16 && t>9)); }

/* 0       :     +     -     *     %     &     |     <     >     =     ~     !     ,     ^     #     _     $     ?     @     .     '     /     \ */
enum fns{
F_nul=0,F_Idy,F_Flp,F_Neg,F_Fst,F_Sqr,F_Wer,F_Rev,F_Asc,F_Dsc,F_Grp,F_Not,F_Til,F_Enl,F_Srt,F_Cnt,F_Flr,F_Str,F_Unq,F_Typ,F_Val,F_ech,F_rdc,F_scn,
F_Fwh=24,F_Las,F_Exp,F_lst,F_Uqs,F_Log,F_Sin,F_Cos,
F_Asn=0,F_Dex,F_Add,F_Sub,F_Mul,F_Div,F_Min,F_Max,F_Les,F_Mor,F_Eql,F_Mtc,F_Key,F_Cat,F_Cut,F_Tak,F_Drp,F_Cst,F_Fnd,F_Atx,F_Cal,F_Ech,F_Rdc,F_Scn,
F_Bin=24,F_Mod,F_Pow,            F_Lgn=29,F_In,                                                                  F_Amd=19,F_Dmd,
F_com=59,F_prj};

const I spcNeg=0, spcSqr=5;
const I spcAdd=0, spcSub=8, spcMul=16, spcDiv=24, spcMin=32, spcMax=40;
const I spcEql=0, spcLes=11, spcMor=22;
const I spcGdu=0, spcGdl=6;


enum ops{
  O_NUL,O_ASN,O_KEY,O_ATX,O_AMD1,O_AMD2,O_AMD,O_DMD1,O_DMD,O_MUL,O_WER,O_NOT,O_PUSH,O_POP,
  O_FLP,O_LAMBDA,O_TRAIN,O_ECH,O_RDC,O_SCN,O_ECR,O_ECL,O_EC2,O_RDN,O_FIX,O_CAL};

static K src(void){ return xK(POS_SRC)[0]; }
static K Kc(I x){ return ((K)((UI)(x))|((K)(T_ct)<<59)); }
static K Ki(I x){ return ((K)((UI)(x))|((K)(T_it)<<59)); }
static K Ks(I x){ return ((K)((UI)(x))|((K)(T_st)<<59)); }

static K Kf(F x){
    K r=mk(T_Ft,1); xF(r)[0]=x;
    return (K)((I)r) | ((K)(T_ft)<<59);
}

static K l1(K x){
    K r=mk(T_Lt,1); xK(r)[0]=x;
    return r;
}

static K l2t(K x, K y, T t){
    K r=mk(T_Lt,2); xK(r)[0]=x; xK(r)[1]=y;
    return (K)((UI)r) | ((K)(t)<<59);
}

static K l2(K x, K y){ return l2t(x,y,T_Lt); }
static K cat1(K x, K y);
static K l3(K x, K y, K z){ return cat1(l2(x,y),z); }
#define tp(x) ((T)(x>>59))
#define nn(x) (xI(x)[-3])
#define rr(x) (xC(x)[-6]&0x1)
static I arity(K f){ return (tp(f)>T_df) ? nn(f) : 2; }

static I conform(K x, K y){
    I r=2*(I)(tp(x)>16) + (I)(tp(y)>16);
    if(r==3 && nn(x)!=nn(y)) trap(E_Length);
    return r;
}

static K rx(K x){
    if(tp(x)<T_ft) return x;
    ++xI(x)[-1];
    return x;
}

static void dx(K x){
    T t=tp(x);
    if(t<T_ft) return;
    I rc=xI(x)[-1]--;
    if(!rc) trap(E_Unref);
    if(rc>1) return;
    I n=nn(x);
    if(t==T_It && rr(x)){ n=1; }
    if((t&15)>6){
      if(t==T_xf || t==T_Dt || t==T_Tt){ n=2; }
      else if(t==T_pf){ n=3; }
      else if(t==T_lf){ n=4; }
      K* xp=xK(x); for(I i=0;i<n;i++) dx(xp[i]);
    }
    mfree((I)x,bucket(sz(t)*n));
}

static void rl(K x){ // ref list elements
    K* xp=xK(x); I n=nn(x);
    for(I i=0;i<n;i++) rx(xp[i]);
}

static K er(K x){ // expand range
    I n=nn(x),a=xI(x)[0]; dx(x); x=mk(T_It,n);
    for(I i=0;i<n;i++) xI(x)[i]=a+i;
    return x;
}

static void lfree(K x){ mfree((I)(x), bucket(8*nn(x))); }

static K uspc(K x, T xt, I ny){
    K r=0;
    I nx=nn(x), s=sz(xt);
    if(xt==T_It && rr(x)){
      I a=xI(x)[0];
      dx(x); r=mk(xt, nx+ny); I* rp=xI(r);
      for(I i=0;i<nx;i++){ rp[i]=a+i; }
      return r;
    }
    if(xI(x)[-1]==1 && bucket(s*nx)==bucket(s*(nx+ny))){ r=x; }
    else {
      r=mk(xt, nx+ny);
      Memorycopy((I)r, (I)x, s*nx);
      if(xt==T_Lt) rl(x);
      dx(x);
    }
    xI(r)[-3] = nx+ny;
    return r;
}

static K x0(K x){ return rx(xK(x)[0]); }
static K x1(K x){ return rx(xK(x)[1]); }
static K x2(K x){ return rx(xK(x)[2]); }
static K x3(K x){ return rx(xK(x)[3]); }
static K r0(K x){ K r=x0(x); dx(x); return r; }
static K r1(K x){ K r=x1(x); dx(x); return r; }
static K r3(K x){ K r=x3(x); dx(x); return r; }

static K uptype(K x, T dst){
    T xt=tp(x);
    I xp=(I)x;
    if((xt&15)==dst) return x;
    if(xt<16){
      if(dst==T_ct) return Kc(xp);
      if(dst==T_it) return Ki(xp);
      if(dst==T_ft) return Kf((F)xp);
      return trap(E_Type);
    }
    if(xt<T_It && dst==T_ft){
      x=uptype(x,T_it);
      xt=T_It;
      xp=(I)x;
    }
    I xn=nn(x);
    K r=mk(dst+16,xn);
    if(dst==T_it){ I* rp=xI(r); C* xp=xC(x);
      for(I i=0;i<xn;i++) rp[i]=xp[i];
    } else if(dst==T_ft){ F* rp=xF(r); I* xp=xI(x);
      if(rr(x)) for(I i=0,a=xp[0];i<xn;i++) rp[i]=(F)a+i;
      else for(I i=0;i<xn;i++) rp[i]=(F)xp[i];
    } else { return trap(E_Type); }
    dx(x);
    return r;
}

static K KCn(const C *x, I n){
    K r=mk(T_Ct,n);
    if(x)memcpy(M_+(I)r, x, (size_t)n);
    return r;
}

static K KC(const C *x){ return KCn(x, (I)strlen(x)); }

static K Ku(UJ x){
    K r=mk(T_Ct,0);
    C* rp=xC(r); I i=0;
    for(; x!=0; ++i){
      rp[i]=(C)x;
      x=(x>>(UJ)8);
    }
    xI(r)[-3]=i;
    return r;
}

static K Asn(K x, K y){
    if(tp(x)!=T_st) return trap(E_Type);
    dx(xK(val_[mod_])[(I)x]);
    xK(val_[mod_])[(I)x]=rx(y);
    return y;
}

static K ucats(K x){
    K xi,r,*xp=xK(x);
    I xn=nn(x),rn=0,s;
    T rt=0, t;
    if(!xn) return x;
    for(I i=0;i<xn;i++, rn+=nn(xi)){
      xi=xp[i];
      t=tp(xi);
      if(!i) rt=t;
      if(rt!=t || rt<16 || t>=T_Lt) return (K)0;
    }
    r=mk(rt,rn);
    s=sz(rt);
    for(I i=0, rp=(I)r; i<xn; i++, rp+=rn){
      xi=xp[i];
      rn=s*nn(xi);
      Memorycopy(rp,(I)xi,rn);
    }
    dx(x); return r;
}

static K use1(K x){ return (xI(x)[-1]==1) ? rx(x) : mk(tp(x),nn(x)); }
static K use2(K x, K y){ return (xI(y)[-1]==1) ? rx(y) : use1(x); }

static K seq(I n);
static K use(K x){
    T xt=tp(x);
    if(xt<16 || xt>T_Lt) return trap(E_Type);
    if(xI(x)[-1]==1) return x;
    I nx=nn(x);
    if(xt==T_It && rr(x)){
      I a=xI(x)[0];
      dx(x); K r=seq(nx);
      xI(r)[0]=a;
      return r;
    }
    K r=mk(xt,nx);
    Memorycopy((I)r,(I)x,sz(xt)*nx);
    if(xt==T_Lt) rl(r);
    dx(x); return r;
}

static K missing(T t){
    switch(t){
      case T_ct: return Kc(' ');
      case T_it: return Ki(nai);
      case T_st: return Ks(0);
      case T_ft: return Kf(F64na);
      default: return mk(T_Ct,0);
    }
}

static K ov0(K f, K x){
    dx(f); dx(x);
    return missing(tp(x));
}

static I mtC(I xp, I yp, I n);
static K sc(K c){
    K* xp=xK(key_);
    I xn=nn(key_),cn=nn(c),n;
    for(I i=0; i<xn; i++){
      n=nn(xp[i]);
      if(cn==n && mtC((I)c,(I)xp[i],n)){
        dx(c);
        return (K)(i) | ((K)(T_st)<<59);
      }
    }
    key_=cat1(key_,c);
    for(I m=0;m<SPESH_MAX_MODULES;++m) val_[m]=cat1(val_[m],0);
    return (K)(xn) | ((K)(T_st)<<59);
}

static K cs(K x){ return rx(xK(key_)[(I)x]); }



static I maxi(I x, I y){ return (x>y) ? x : y; }
static F maxf(F x, F y){ return F64max(x,y); }

static K ndyad(I f, K ff, K x, K y);
static K Max(K x, K y){
    if(tp(y)<16) return ndyad(spcMax,F_Max,y,x);
    return ndyad(spcMax,F_Max,x,y);
}

static void maxcC(C x, C*y, C*r, I n){ I i=0; do r[i]=(C)maxi(x,y[i]); while(++i<n); }
static void maxiI(I x, I*y, I*r, I n){ I i=0; do r[i]=maxi(x,y[i]); while(++i<n); }
static void maxfF(F x, F*y, F*r, I n){ I i=0; do r[i]=F64max(x,y[i]); while(++i<n); }
static void maxC(C*x, C*y, C*r, I n){ I i=0; do r[i]=(C)maxi(x[i],y[i]); while(++i<n); }
static void maxI(I*x, I*y, I*r, I n){ I i=0; do r[i]=maxi(x[i],y[i]); while(++i<n); }
static void maxF(F*x, F*y, F*r, I n){ I i=0; do r[i]=F64max(x[i],y[i]); while(++i<n); }

static K max(K y, T t, I n){
    F f;
    I xp;
    switch(t){
      case T_Ct:
        xp=-128;
        for(I i=0;i<n;i++) xp=maxi(xp,xC(y)[i]);
        return Kc(xp);
      case T_It:
        if(rr(y)) return Ki(xI(y)[0]+ n-1);
        xp=nai;
        for(I i=0;i<n;i++) xp=maxi(xp,xI(y)[i]);
        return Ki(xp);
      case T_St:
        xp=0;
        for(I i=0;i<n;i++) xp=maxi(xp,xI(y)[i]);
        return Ks(xp);
      case T_Ft:
        f=-F64inf;
        for(I i=0;i<n;i++) f=F64max(f,xF(y)[i]);
        return Kf(f);
      default: return (K)0;
    }
}

static T maxtype(K x, K y){
    T t=(T)maxi((I)(tp(x)&15), (I)(tp(y)&15));
    return t ? t : T_it;
}

static K Min(K x, K y){
    if(tp(y)<16) return ndyad(spcMin,F_Min,y,x);
    return ndyad(spcMin,F_Min,x,y);
}

static I mini(I x, I y){ return (x<y) ? x : y; }
static F minf(F x, F y){ return F64min(x,y); }
static void mincC(C x, C*y, C*r, I n){ I i=0; do r[i]=(C)mini(x,y[i]); while(++i<n); }
static void miniI(I x, I*y, I*r, I n){ I i=0; do r[i]=mini(x,y[i]); while(++i<n); }
static void minfF(F x, F*y, F*r, I n){ I i=0; do r[i]=minf(x,y[i]); while(++i<n); }
static void minC(C*x, C*y, C*r, I n){ I i=0; do r[i]=(C)mini(x[i],y[i]); while(++i<n); }
static void minI(I*x, I*y, I*r, I n){ I i=0; do r[i]=mini(x[i],y[i]); while(++i<n); }
static void minF(F*x, F*y, F*r, I n){ I i=0; do r[i]=minf(x[i],y[i]); while(++i<n); }

static K min(K y, T t, I n){
    I xp;
    F f;
    switch(t){
      case T_Ct:
        xp=127;
        for(I i=0;i<n;i++) xp=mini(xp,xC(y)[i]);
        return Kc(xp);
      case T_It:
        if(rr(y)) return Ki(xI(y)[0]);
        xp=2147483647;
        for(I i=0;i<n;i++) xp=mini(xp,xI(y)[i]);
        return Ki(xp);
      case T_St:
        xp=nn(key_) - 1;
        for(I i=0;i<n;i++) xp=mini(xp,xI(y)[i]);
        return Ks(xp);
      case T_Ft:
        f=F64inf;
        for(I i=0;i<n;i++) f=F64min(f,xF(y)[i]);
        return Kf(f);
      default: return (K)0;
    }
}

static K Cat(K x, K y);
static K Add(K x, K y){
    if((tp(x)&15)==T_ct && (tp(y)&15)==T_ct) return Cat(x,y);
    if(tp(y)<16) return ndyad(spcAdd,F_Add,y,x);
    return ndyad(spcAdd,F_Add,x,y);
}

static I addi(I x, I y){ return (x+y); }
static F addf(F x, F y){ return (x+y); }
static void addcC(C x, C*y, C*r, I n){ I i=0; do r[i]=x+y[i]; while(++i<n); }
static void addiI(I x, I*y, I*r, I n){ I i=0; do r[i]=x+y[i]; while(++i<n); }
static void addfF(F x, F*y, F*r, I n){ I i=0; do r[i]=x+y[i]; while(++i<n); }
static void addC(C*x, C*y, C*r, I n){ I i=0; do r[i]=x[i]+y[i]; while(++i<n); }
static void addI(I*x, I*y, I*r, I n){ I i=0; do r[i]=x[i]+y[i]; while(++i<n); }
static void addF(F*x, F*y, F*r, I n){ I i=0; do r[i]=x[i]+y[i]; while(++i<n); }
static K nmonad(I f, K x);
static K Neg(K x){ return nmonad(spcNeg,x); }
static I negi(I x){ return -x; }
static F negf(F x){ return -x; }
static void negC(C*x, C*r, I n){ I i=0; do r[i]=-x[i]; while(++i<n); }
static void negI(I*x, I*r, I n){ I i=0; do r[i]=-x[i]; while(++i<n); }
static void negF(F*x, F*r, I n){ I i=0; do r[i]=-x[i]; while(++i<n); }

static K atn(K x, I a, I n);
static K trims(K x, K s){
    T xt=tp(x),st=tp(s);
    if(xt==st && xt==T_ct){
      if((C)x==(C)s) return mk(T_Ct,0);
      return x;
    } else if(xt==T_ct){
      dx(s); return x;
    }
    I xn=nn(x),sn=(st==T_ct) ? 1 : nn(s);
    if(sn>xn || !sn || !xn){ dx(s); return use(x); }
    if(st==T_ct){
      if(xC(x)[xn-1] == (C)s) return atn(x, 0, xn-1);
      return use(x);
    } else {
      for(I i=xn-sn,j=0; i<xn; i++, j++){
        if(xC(x)[i] != xC(s)[j]){
          dx(s); return use(x);
        }
      }
      dx(s); return atn(x, 0, xn-sn);
    }
}
static K trimp(K p, K x){
    T xt=tp(x),pt=tp(p);
    if(xt==pt && xt==T_ct){
      if((C)x==(C)p) return mk(T_Ct,0);
      return x;
    } else if(xt==T_ct){
      dx(p); return x;
    }
    I xn=nn(x),pn=(pt==T_ct) ? 1 : nn(p);
    if(pn>xn || !pn || !xn){ dx(p); return use(x); }
    if(pt==T_ct){
      if(xC(x)[0]==(C)p) return atn(x, 1, xn-1);
      return use(x);
    } else {
      for(I i=0; i<pn; i++){
        if(xC(x)[i]!=xC(p)[i]){
          dx(p); return use(x);
        }
      }
      dx(p); return atn(x, pn, xn-pn);
    }
}


static K Sub(K x, K y){
    if((tp(x)&15)==T_ct && (tp(y)&15)==T_ct) return trims(x,y);
    if(tp(y)<16) return ndyad(spcAdd,F_Add,Neg(y),x);
    return ndyad(spcSub,F_Sub,x,y);
}

static I subi(I x, I y){ return (x-y); }
static F subf(F x, F y){ return (x-y); }
static void subcC(C x, C*y, C*r, I n){ I i=0; do r[i]=x-y[i]; while(++i<n); }
static void subiI(I x, I*y, I*r, I n){ I i=0; do r[i]=x-y[i]; while(++i<n); }
static void subfF(F x, F*y, F*r, I n){ I i=0; do r[i]=x-y[i]; while(++i<n); }
static void subC(C*x, C*y, C*r, I n){ I i=0; do r[i]=x[i]-y[i]; while(++i<n); }
static void subI(I*x, I*y, I*r, I n){ I i=0; do r[i]=x[i]-y[i]; while(++i<n); }
static void subF(F*x, F*y, F*r, I n){ I i=0; do r[i]=x[i]-y[i]; while(++i<n); }

static K repeat(K x, I n){
    if(!n) return mk(T_Ct,0);
    if(n==1) return x;
    if(n<0) return trap(E_Value);
    if(tp(x)==T_ct){
      K r=mk(T_Ct,n);
      memset(xC(r), (C)x, (size_t)n);
      return r;
    }
    I xn=nn(x);
    K r=uspc(x, T_Ct, xn*n-xn);
    for(I i=xn,j=0,k=0; k<n; i++, j++){
      if(j>=xn){ j=0;++k; }
      xC(r)[i]=xC(r)[j];
    }
    return r;
}

static K Mul(K x, K y){
    T yt=tp(y);
    if((tp(x)&15)==T_ct && (yt==T_it)) return repeat(x,(I)y);
    if(yt<16){ return ndyad(spcMul,F_Mul,y,x); }
    return ndyad(spcMul,F_Mul,x,y);
}

static I muli(I x, I y){ return (x*y); }
static F mulf(F x, F y){ return (x*y); }
static void mulcC(C x, C*y, C*r, I n){ I i=0; do r[i]=x*y[i]; while(++i<n); }
static void muliI(I x, I*y, I*r, I n){ I i=0; do r[i]=x*y[i]; while(++i<n); }
static void mulfF(F x, F*y, F*r, I n){ I i=0; do r[i]=x*y[i]; while(++i<n); }
static void mulC(C*x, C*y, C*r, I n){ I i=0; do r[i]=x[i]*y[i]; while(++i<n); }
static void mulI(I*x, I*y, I*r, I n){ I i=0; do r[i]=x[i]*y[i]; while(++i<n); }
static void mulF(F*x, F*y, F*r, I n){ I i=0; do r[i]=x[i]*y[i]; while(++i<n); }

static K scale(F s, K x){
    K r;
    if(tp(x)<T_Ft) x=uptype(x,T_ft);
    r=use1(x);
    if(nn(r)) mulfF(s,xF(x),xF(r),nn(r));
    dx(x); return r;
}

static I divi(I x, I y){ return (y==0) ? nai : x/y; }
static I modi(I x, I y){ return (y==0) ? nai : x%y; }
static F divf(F x, F y){ return (x/y); }
static void divfF(F x, F*y, F*r, I n){ I i=0; do r[i]=x/y[i]; while(++i<n); }
static void divF(F*x, F*y, F*r, I n){ I i=0; do r[i]=x[i]/y[i]; while(++i<n); }

static void divIi(I*x, I y, I n){
    I s=31-Iclz(y),i=0;
    if(y==(1<<s)){
      do x[i]=x[i]>>s; while(++i<n);
    } else {
      for(;i<n;++i){ x[i]=divi(x[i],y); }
    }
}

static K idiv(K x, K y, I mod){
    K r=0;
    I av,xp,yp,n;
    T t=maxtype(x,y);
    if(t!=T_it) return trap(E_Type);
    x=uptype(x,t); y=uptype(y,t);
    av=conform(x,y);
    xp=(I)x; yp=(I)y;
    switch(av){
      case 0: return Ki(mod ? modi(xp,yp) : divi(xp,yp));
      case 1:
        if(rr(y)){
          I b=xI(y)[0]; n=nn(y);
          dx(y); r=mk(T_It,n);
          if(mod) for(I i=0;i<n;i++){ xI(r)[i]=modi(xp,b+i); }
          else for(I i=0;i<n;i++){ xI(r)[i]=divi(xp,b+i); }
          return r;
        }
        r=use(y); n=nn(r);
        if(mod) for(I i=0;i<n;++i){ xI(r)[i]=modi(xp,xI(r)[i]); }
        else for(I i=0;i<n;++i){ xI(r)[i]=divi(xp,xI(r)[i]); }
        return r;
      case 2:
        if(rr(x)){
          I a=xI(x)[0]; n=nn(x);
          if(mod && a+n<=yp) return x;
          dx(x); r=mk(T_It,n);
          if(mod) for(I i=0;i<n;i++){ xI(r)[i]=modi(a+i,yp); }
          else for(I i=0;i<n;i++){ xI(r)[i]=divi(a+i,yp); }
          return r;
        }
        x=use(x); xp=(I)x; n=nn(x);
        if(yp>0 && n>0 && mod==0){ divIi(xI(x),yp,n); }
        if(mod) for(I i=0;i<n;++i){ xI(x)[i]=modi(xI(x)[i],yp); }
        return x;
      default:
        if(rr(x)){
          I a=xI(x)[0]; n=nn(x);
          r=mk(T_It,n);
          if(rr(y)){
            I b=xI(y)[0];
            if(mod) for(I i=0;i<n;i++){ xI(r)[i]=modi(a+i,b+i); }
            else for(I i=0;i<n;i++){ xI(r)[i]=divi(a+i,b+i); }
            dx(x); dx(y); return r;
          }
          if(mod) for(I i=0;i<n;i++){ xI(r)[i]=modi(a+i,xI(y)[i]); }
          else for(I i=0;i<n;i++){ xI(r)[i]=divi(a+i,xI(y)[i]); }
          dx(x); dx(y); return r;
        }
        if(rr(y)){
          I b=xI(y)[0]; n=nn(y);
          r=mk(T_It,n);
          if(mod) for(I i=0;i<n;i++){ xI(r)[i]=modi(xI(x)[i],b+i); }
          else for(I i=0;i<n;i++){ xI(r)[i]=divi(xI(x)[i],b+i); }
          dx(x); dx(y); return r;
        }
        r=use2(x,y);
        n=nn(r);
        if(mod) for(I i=0;i<n;++i){ xI(r)[i]=modi(xI(x)[i],xI(y)[i]); }
        else for(I i=0;i<n;++i){ xI(r)[i]=divi(xI(x)[i],xI(y)[i]); }
        dx(x); dx(y);
        return r;
    }
}

static K pads(K x, I s){
    K r;
    I xn,n=(s<0)?-s:s;
    if(!n) return use(x);
    if(tp(x)==T_ct){
      K r=mk(T_Ct, n);
      if(s>0){
        xC(r)[0]=(C)x;
        for(I i=1;i<n;i++){ xC(r)[i] = ' '; }
      } else {
        xC(r)[n-1]=(C)x;
        for(I i=0;i<n-1;i++){ xC(r)[i] = ' '; }
      }
      return r;
    }
    xn=nn(x);
    I c=n-xn;
    if(0>=c) return use(x);
    if(s>0){
      r=uspc(x,T_Ct,c);
      for(I i=xn;i<n;i++){ xC(r)[i] = ' '; }
    } else {
      r=mk(T_Ct,n);
      for(I i=0,j=c;i<xn;i++,j++){ xC(r)[j]=xC(x)[i]; }
      for(I i=0;i<c;i++){ xC(r)[i] = ' '; }
    }
    return r;
}

static K Mod(K x, K y){
    T xt=tp(x),yt=tp(y);
    if(yt==T_it && (xt&15)==T_ct) return pads(x, (I)y);
    if((xt&15)<T_ft && (yt&15)<T_ft) return idiv(x,y,1);
    return (xt>=T_Lt || yt>=T_Lt) ? ndyad(0,F_Mod,x,y) : trap(E_Type);
}

static K Div(K x, K y){
    F s;
    T xt=tp(x),yt=tp(y);
    if((xt&15)<T_ft && (yt&15)<T_ft) return idiv(x,y,0);
    if(yt<16 && xt>16 && xt<T_Lt){
      if(yt<T_ft){ y=uptype(y,T_ft); }
      s=1. / xF(y)[0];
      dx(y);
      return scale(s,x);
    }
    return ndyad(spcDiv,F_Div,x,y);
}

static I eqi(I x, I y){ return (I)(x==y); }
static I eqf(F x, F y){ return (I)((x!=x && y!=y) || F64eq(x,y)); }
static void eqcC(C x, C*y, I*r, I n){ I i=0; do r[i]=(I)(x==y[i]); while(++i<n); }
static void eqiI(I x, I*y, I*r, I n){ I i=0; do r[i]=(I)(x==y[i]); while(++i<n); }
static void eqfF(F x, F*y, I*r, I n){ I i=0; do r[i]=eqf(x,y[i]); while(++i<n); }
static void eqCc(C*x, C y, I*r, I n){ I i=0; do r[i]=(I)(x[i]==y); while(++i<n); }
static void eqIi(I*x, I y, I*r, I n){ I i=0; do r[i]=(I)(x[i]==y); while(++i<n); }
static void eqFf(F*x, F y, I*r, I n){ I i=0; do r[i]=eqf(x[i],y); while(++i<n); }
static void eqC(C*x, C*y, I*r, I n){ I i=0; do r[i]=(I)(x[i]==y[i]); while(++i<n); }
static void eqI(I*x, I*y, I*r, I n){ I i=0; do r[i]=(I)(x[i]==y[i]); while(++i<n); }
static void eqF(F*x, F*y, I*r, I n){ I i=0; do r[i]=eqf(x[i],y[i]); while(++i<n); }

static K eqle(K x, K y, T t){ // t == xt == yt-16
    if(t==T_it){
      if(rr(y)){
        K r=mk(T_It, nn(y));
        memset(xC(r), 0, (size_t)nn(r)*4);
        I a=xI(y)[0];
        if((I)x>=a && (I)x<a+nn(r)){ xI(r)[(I)x-a]=1; }
        dx(y); return r;
      }
      eqiI((I)x, xI(y), xI(y), nn(y)); return y;
    }
    I n=nn(y);
    K r=mk(T_It,n);
    switch(t){
      case T_ct: eqcC((C)x, xC(y), xI(r), n); break;
      case T_ft: eqfF(xF(x)[0], xF(y), xI(r), n); dx(x); break;
      default: return trap(E_Type);
    }
    dx(y); return r;
}

static K ncmp(I f, K ff, K x, K y);
static K Eql(K x, K y){ return ncmp(spcEql,F_Eql,x,y); }
static K Mor(K x, K y){ return ncmp(spcMor,F_Mor,x,y); }
static K Les(K x, K y){ return ncmp(spcLes,F_Les,x,y); }

static I lti(I x, I y){ return (I)(x<y); }
static I ltf(F x, F y){ return (I)(x<y || x!=x); }
static void ltcC(C v, C*y, I*r, I n){ I i=0; do r[i]=(I)(v<y[i]); while(++i<n); }
static void ltiI(I x, I*y, I*r, I n){ I i=0; do r[i]=(I)(x<y[i]); while(++i<n); }
static void ltfF(F x, F*y, I*r, I n){ I i=0; do r[i]=ltf(x,y[i]); while(++i<n); }
static void ltCc(C*x, C v, I*r, I n){ I i=0; do r[i]=(I)(x[i]<v); while(++i<n); }
static void ltIi(I*x, I y, I*r, I n){ I i=0; do r[i]=(I)(x[i]<y); while(++i<n); }
static void ltFf(F*x, F y, I*r, I n){ I i=0; do r[i]=ltf(x[i],y); while(++i<n); }
static void ltC(C*x, C*y, I*r, I n){ I i=0; do r[i]=(I)(x[i]<y[i]); while(++i<n); }
static void ltI(I*x, I*y, I*r, I n){ I i=0; do r[i]=(I)(x[i]<y[i]); while(++i<n); }
static void ltF(F*x, F*y, I*r, I n){ I i=0; do r[i]=ltf(x[i],y[i]); while(++i<n); }

static I gti(I x, I y){ return (I)(x>y); }
static I gtf(F x, F y){ return (I)(x>y || y!=y); }
static void gtcC(C x, C*y, I*r, I n){ I i=0; do r[i]=(I)(x>y[i]); while(++i<n); }
static void gtiI(I x, I*y, I*r, I n){ I i=0; do r[i]=(I)(x>y[i]); while(++i<n); }
static void gtfF(F x, F*y, I*r, I n){ I i=0; do r[i]=gtf(x,y[i]); while(++i<n); }
static void gtCc(C*x, C y, I*r, I n){ I i=0; do r[i]=(I)(x[i]>y); while(++i<n); }
static void gtIi(I*x, I y, I*r, I n){ I i=0; do r[i]=(I)(x[i]>y); while(++i<n); }
static void gtFf(F*x, F y, I*r, I n){ I i=0; do r[i]=gtf(x[i],y); while(++i<n); }
static void gtC(C*x, C*y, I*r, I n){ I i=0; do r[i]=(I)(x[i]>y[i]); while(++i<n); }
static void gtI(I*x, I*y, I*r, I n){ I i=0; do r[i]=(I)(x[i]>y[i]); while(++i<n); }
static void gtF(F*x, F*y, I*r, I n){ I i=0; do r[i]=gtf(x[i],y[i]); while(++i<n); }

static K ipow(K x, I y){
    if(tp(x)==T_It){
      I n=nn(x);
      K r=mk(T_It,n);
      if(rr(x)) for(I i=0,a=xI(x)[0];i<n;++a,++i){ xI(r)[i]=iipow(a,y); }
      else for(I i=0;i<n;i++){ xI(r)[i]=iipow(xI(x)[i],y); }
      dx(x); return r;
    } else {
      return Ki(iipow((I)x,y));
    }
}

static F fk(K x){
    T t=tp(x);
    if(t==T_it) return (F)((I)x);
    if(t!=T_ft) trap(E_Type);
    F f=xF(x)[0]; dx(x);
    return f;
}

static I maxcount(I x, I n){
    K y;
    I r=0;
    for(I i=0; i<n; i++){
      y=xK(x)[i];
      r=(tp(y)<16) ? maxi(1,r) : maxi(nn(y),r);
    }
    return r;
}

static T dtypes(K x, K y){ return (T)maxi((I)tp(x),(I)tp(y)); }
static K dkeys(K x, K y){ return (tp(x)>T_Lt) ? x0(x) : x0(y); }
static K dvals(K x){ return (tp(x)>T_Lt) ? r1(x) : x; }

static K seq(I n){
    n=maxi(n,0);
    if(!n){ return mk(T_It,0); }
    K r=mk(T_It,1);
    xI(r)[-3] = n;
    xI(r)[0] = 0;
    xC(r)[-6] = 1;
    if(!rr(r)) trap(E_Err);
    return r;
}

static K key(K x, K y, T t);
static K uf(K x);
static K ati(K x, I i);
static K Val(K x);
static K Flp(K x);
static I match(K x, K y);
static K ufd(K x){ // T_Lt of T_Dt to T_Tt
    K k=r0(x0(x)),v;
    if(tp(k)!=T_St){
      dx(k); return x;
    }
    I n=nn(x);
    for(I i=0; i<n; i++){
      I t=(I)xJ(x)[i];
      if(!match(k,xK(t)[0])){
        dx(k); return x;
      }
    }
    v=mk(T_Lt,n);
    for(I i=0;i<n;i++){ xK(v)[i]=Val(ati(rx(x),i)); }
    dx(x);
    return key(k,Flp(uf(v)),T_Tt);
}

static K uf(K x){ // Vectorize T_Lt
    K r;
    T rt=0,t;
    I xn=nn(x);
    for(I i=0; i<xn; i++){
      t=tp(xK(x)[i]);
      if(!i) rt=t;
      else if(t!=rt) return x;
    }
    if(rt==T_Dt) return ufd(x);
    if((rt==0)||(rt>T_ft)) return x;
    rt+=16;
    /*if(rt==T_It){ // TODO: try vectorizing to rr?
      // Too expensive..
      I a=xI(x)[0], y=1;
      for(I i=0; i<xn; i++, a++) if(a!=(I)xK(x)[i]){ y=0; break; }
      if(y){
        r=seq(xn);
        xI(r)[0]=xI(x)[0];
        dx(x); return r;
      }
    }*/
    r=mk(rt,xn);
    switch(sz(rt)>>2){
      case 0: for(I i=0;i<xn;i++){ xC(r)[i]=(C)xK(x)[i]; }
        break;
      case 1: for(I i=0;i<xn;i++){ xI(r)[i]=(I)xK(x)[i]; }
        break;
      case 2: for(I i=0;i<xn;i++){ xF(r)[i]=xF(xK(x)[i])[0]; }
        break;
    }
    dx(x); return r;
}

static K stv(K x, K i, K y){
    // x[ni] = y[n]
    I n,xn,s;
    T xt;
    if(T_It!=tp(i)) return trap(E_Type);
    n=nn(i);
    if(!n){
      dx(y); dx(i);
      return x;
    }
    if(n!=nn(y)) return trap(E_Length);
    if(rr(x)) x=er(x);
    if(rr(y)) trap(E_Nyi);
    x=use(x); xt=tp(x); xn=nn(x);
    s=sz(xt);
    if(rr(i)){
      I a=xI(i)[0];
      if((UI)(a+n-1)>=(UI)xn) return trap(E_Index);
      switch(s>>2){
        case 0: for(I j=0;j<n;j++){ xC(x)[a+j]=xC(y)[j]; }
          break;
        case 1: for(I j=0;j<n;j++){ xI(x)[a+j]=xI(y)[j]; }
          break;
        case 2:
          if(xt==T_Lt){
            rl(y);
            for(I j=0;j<n;j++){ dx(xK(x)[a+j]); }
          }
          for(I j=0;j<n;j++){ xK(x)[a+j]=xK(y)[j]; }
          if(xt==T_Lt) x=uf(x);
          break;
      }
    } else {
      for(I j=0; j<n; j++){
        if((UI)xI(i)[j]>=(UI)xn) return trap(E_Index);
      }
      switch(s>>2){
        case 0: for(I j=0;j<n;j++){ xC(x)[xI(i)[j]]=xC(y)[j]; }
          break;
        case 1: for(I j=0;j<n;j++){ xI(x)[xI(i)[j]]=xI(y)[j]; }
          break;
        case 2:
          if(xt==T_Lt){
            rl(y);
            for(I j=0;j<n;j++){ dx(xK(x)[xI(i)[j]]); }
          }
          for(I j=0;j<n;j++){ xK(x)[xI(i)[j]]=xK(y)[j]; }
          if(xt==T_Lt) x=uf(x);
          break;
      }
    }
    dx(i); dx(y);
    return x;
}

static K sti(K x, I i, K y){
    I xn,s;
    x=use(x);
    T xt=tp(x);
    xn=nn(x);
    if(i<0 || i>=xn) return trap(E_Index);
    if(rr(x)) x=er(x);
    s=sz(xt);
    switch(s>>2){
      case 0: xC(x)[i]=(C)y; break;
      case 1: xI(x)[i]=(I)y; break;
      case 2:
        if(xt==T_Lt){
          dx(xK(x)[i]);
          xK(x)[i]=rx(y);
          x=uf(x);
        } else {
          xF(x)[i]=xF(y)[0];
        }
        break;
    }
    dx(y); return x;
}

static K Fst(K x){
    T t=tp(x);
    if(t<16) return x;
    return (t==T_Dt) ? Fst(Val(x)) : ati(x,0);
}

static K Cnt(K x){
    T t=tp(x);
    dx(x);
    return (t<16) ? Ki(1) : Ki(nn(x));
}

static K notI(K x){
    T xt=tp(x);
    if((xt&15)!=T_it) return trap(E_Type);
    if(xt==T_it) return Ki(0==(I)x);
    if(rr(x)) x=er(x);
    x=use(x); I n=nn(x);
    for(I i=0;i<n;i++){ xI(x)[i]=(0==xI(x)[i]); }
    return x;
}

static void ff(I f, I r, I x, I n, F yf){
    I i=0;
    if(f==F_Exp) do xF(r)[i]=exp_(xF(x)[i]); while(++i<n);
    else if(f==F_Log) do xF(r)[i]=log_(xF(x)[i]); while(++i<n);
    else if(f==F_Pow+32) do xF(r)[i]=pow_(xF(x)[i],yf); while(++i<n);
    else do xF(r)[i]=cosin_(xF(x)[i],1+(I)(f==F_Cos)); while(++i<n);
}

static K nfmath(I f, K x, K y);
static K rec_nfmath(I f, K x, K y){
    switch(f-32){
      case F_Pow:
        if((tp(x)&15)==T_it && tp(y)==T_it && (I)(y)>=0){
          return ipow(x,(I)y);
        }
        break;
    }
    return nfmath(f, x, y);
}

static K explode(K x);
static K nfmath(I f, K x, K y){
    K r=0;
    F yf=0.;
    I xp,n;
    T t=tp(x);
    if(t>=T_Lt){
      if(y==0){
        if(t==T_Dt){
          r=x0(x);
          return key(r,nfmath(f,r1(x),0),T_Dt);
        }
        if(t==T_Tt) x=explode(x);
        n=nn(x);
        r=mk(T_Lt,n);
        for(I i=0;i<n;i++){ xK(r)[i]=nfmath(f,ati(rx(x),i),0); }
        dx(x);
      } else {
        t=dtypes(x,y);
        if(t>T_Lt){
          r=dkeys(x,y);
          return key(r,rec_nfmath(f,dvals(x),dvals(y)),t);
        }
        n=conform(x,y);
        switch(n){
          case 0: return rec_nfmath(f,x,y);
          case 1: n=nn(y); break;
          default: n=nn(x); break;
        }
        r=mk(T_Lt,n);
        for(I i=0;i<n;i++){ xK(r)[i]=rec_nfmath(f,ati(rx(x),i),ati(rx(y),i)); }
        dx(x); dx(y);
      }
      return uf(r);
    }
    if(y!=0){ yf=fk(y); }
    if((t&15)<T_ft){
      x=uptype(x,T_ft); t=tp(x);
    }
    xp=(I)x;
    if(t==T_ft){
      r=Kf(0.);
      ff(f,(I)r,xp,1,yf);
    } else {
      n=nn(x);
      r=mk(T_Ft,n);
      if(n>0){ ff(f,(I)r,xp,n,yf); }
    }
    dx(x); return r;
}

static K rtrim(K x){
    for(I i=nn(x)-1; i>=0; i--){
      switch(xC(x)[i]){
        case ' ':case '\r':case '\n':case '\t':
          continue;
        default:
          return atn(x,0,i+1);
      }
    }
    return use(x);
}

static void nyi(K x){ (void)x; trap(E_Nyi); }

static F sqrf(F x){ return F64sqrt(x); }
static void sqrF(F*x, F*r, I n){ I i=0; do r[i]=F64sqrt(x[i]); while(++i<n); }

static K nmonad(I f, K x){
    static void *Fm_[3*5]={
#define $ (void*)
      $ negi,$ negf,$ negC,$ negI,$ negF,
      $  nyi,$ sqrf,$  nyi,$  nyi,$ sqrF};
    K r=0;
    I xp,n;
    T xt=tp(x);
    if(xt>T_Lt){
      r=x0(x);
      return key(r,nmonad(f,r1(x)),xt);
    }
    xp=(I)x;
    if(xt==T_Lt){
      n=nn(x);
      r=mk(T_Lt,n);
      for(I i=0;i<n;i++){ xK(r)[i]=nmonad(f,rx(xK(x)[i])); }
      dx(x); return uf(r);
    }
    if(xt<16){
      switch(xt){
        case T_ct: return Kc(((I(*)(I))Fm_[f])(xp));
        case T_it: return Ki(((I(*)(I))Fm_[f])(xp));
        case T_ft:
          r=Kf(((F(*)(F))Fm_[1+f])(xF(x)[0]));
          dx(x);
          return r;
        default: return trap(E_Type);
      }
    }
    if(f==spcNeg && xt==T_Ct) return rtrim(x);
    if(rr(x)){
      x=er(x); xp=(I)x;
    }
    r=use1(x);
    n=nn(r);
    if(0==n){
      dx(x); return r;
    }
    switch(xt){
      case T_Ct: ((void(*)(C*,C*,I))Fm_[2+f])(xC(x),xC(r),n); break;
      case T_It: ((void(*)(I*,I*,I))Fm_[3+f])(xI(x),xI(r),n); break;
      case T_St: return trap(E_Type);
      case T_Ft: ((void(*)(F*,F*,I))Fm_[4+f])(xF(x),xF(r),n); break;
    }
    dx(x); return r;
}

static K rec_ndyad(K ff, K x, K y){
    switch(ff){
      case F_Max: return Max(x,y);
      case F_Min: return Min(x,y);
      case F_Add: return Add(x,y);
      case F_Sub: return Sub(x,y);
      case F_Mul: return Mul(x,y);
      case F_Mod: return Mod(x,y);
      case F_Div: return Div(x,y);
      default: return trap(E_Nyi);
    }
}
static K rr_ndyad(K ff, I x, K y){
    switch(ff){
      case F_Add: xI(y)[0]=x+xI(y)[0]; return y;
      case F_Sub: xI(y)[0]=x-xI(y)[0]; return y;
      case F_Min:
      case F_Mod: if(xI(y)[0]+nn(y)<=x) return y; break;
      case F_Max: if(xI(y)[0]>=x) return y; break;
      default: break;
    }
    return rec_ndyad(ff, Ki(x), er(y));
}

static K ndyad(I f, K ff, K x, K y){
    static void *Fd_[6*8]={
#define $ (void*)
      $ addi,$ addf,$ addcC,$ addiI,$ addfF,$ addC,$ addI,$ addF,
      $ subi,$ subf,$ subcC,$ subiI,$ subfF,$ subC,$ subI,$ subF,
      $ muli,$ mulf,$ mulcC,$ muliI,$ mulfF,$ mulC,$ mulI,$ mulF,
      $ divi,$ divf,$   nyi,$   nyi,$ divfF,$  nyi,$  nyi,$ divF,
      $ mini,$ minf,$ mincC,$ miniI,$ minfF,$ minC,$ minI,$ minF,
      $ maxi,$ maxf,$ maxcC,$ maxiI,$ maxfF,$ maxC,$ maxI,$ maxF};
    K r=0;
    I av;
    T t=dtypes(x,y);
    if(t>T_Lt){
      r=dkeys(x,y);
      return key(r, rec_ndyad(ff,dvals(x),dvals(y)), t);
    }
    if(t==T_Lt){
      if(x==0ull) return trap(E_Value);
      I n=conform(x,y);
      switch(n){
        case 1: n=nn(y); break;
        default: n=nn(x); break;
      }
      r=mk(T_Lt,n); K* rp=xK(r);
      switch(ff){
        case F_Max: for(I i=0;i<n;i++){ rp[i]=Max(ati(rx(x),i),ati(rx(y),i)); }
          break;
        case F_Min: for(I i=0;i<n;i++){ rp[i]=Min(ati(rx(x),i),ati(rx(y),i)); }
          break;
        case F_Add: for(I i=0;i<n;i++){ rp[i]=Add(ati(rx(x),i),ati(rx(y),i)); }
          break;
        case F_Sub: for(I i=0;i<n;i++){ rp[i]=Sub(ati(rx(x),i),ati(rx(y),i)); }
          break;
        case F_Mul: for(I i=0;i<n;i++){ rp[i]=Mul(ati(rx(x),i),ati(rx(y),i)); }
          break;
        case F_Mod: for(I i=0;i<n;i++){ rp[i]=Mod(ati(rx(x),i),ati(rx(y),i)); }
          break;
        case F_Div: for(I i=0;i<n;i++){ rp[i]=Div(ati(rx(x),i),ati(rx(y),i)); }
          break;
        default: return trap(E_Nyi);
      }
      dx(x); dx(y);
      return uf(r);
    }
    t=maxtype(x,y);
    x=uptype(x,t); y=uptype(y,t);
    if(!x || !y) return trap(E_Type);
    av=conform(x,y);
    if(!av){ // ie. i * i
      switch(t){
        case T_ct: return Kc(((I(*)(C ,C))Fd_[f])((C)x,(C)y));
        case T_it: return Ki(((I(*)(I ,I))Fd_[f])((I)x,(I)y));
        case T_st: return trap(E_Type);
        case T_ft:
          dx(x); dx(y);
          return Kf(((F(*)(F,F))Fd_[1+f])(xF(x)[0],xF(y)[0]));
      }
    }
    if(av==1){ // ie. i * I
      if(rr(y)) return rr_ndyad(ff,(I)x,y);
      r=use1(y);
      I n=nn(r);
      if(!n){
        dx(x); dx(y);
        return r;
      }
      switch(t){
        case T_ct: ((void(*)(C,C*,C*,I))Fd_[2+f])((C)x,xC(y),xC(r),n); break;
        case T_it: ((void(*)(I,I*,I*,I))Fd_[3+f])((I)x,xI(y),xI(r),n); break;
        case T_st: return trap(E_Type);
        case T_ft: ((void(*)(F,F*,F*,I))Fd_[4+f])(xF(x)[0],xF(y),xF(r),n); break;
      }
      dx(x); dx(y);
      return r;
    } else { // ie. I * I
      if(rr(x)) x=er(x);
      if(rr(y)) y=er(y);
      r=use2(x,y);
      I n=nn(r);
      if(!n){
        dx(x); dx(y);
        return r;
      }
      switch(t){
        case T_ct: ((void(*)(C*,C*,C*,I))Fd_[5+f])(xC(x),xC(y),xC(r),n); break;
        case T_it: ((void(*)(I*,I*,I*,I))Fd_[6+f])(xI(x),xI(y),xI(r),n); break;
        case T_st: return trap(E_Type);
        case T_ft: ((void(*)(F*,F*,F*,I))Fd_[7+f])(xF(x),xF(y),xF(r),n); break;
      }
      dx(x); dx(y);
      return r;
    }
}

static K rr_ncmp(K ff, K x, K y){
    if(tp(x)==T_It && rr(x)){
      I n=nn(x); I a=xI(x)[0];
      dx(x); K r=mk(T_It, n); I* rp=xI(r);
      switch(ff){
        case F_Les: for(I i=0;i<n;i++){ rp[i]=lti(a+i,(I)y); }
          break;
        case F_Eql: for(I i=0;i<n;i++){ rp[i]=eqi(a+i,(I)y); }
          break;
        case F_Mor: for(I i=0;i<n;i++){ rp[i]=gti(a+i,(I)y); }
          break;
        default: return trap(E_Nyi);
      }
      return r;
    }
    if(ff==F_Les) return rr_ncmp(F_Mor, y, x);
    else if(ff==F_Mor) return rr_ncmp(F_Les, y, x);
    return rr_ncmp(ff, y, x);
}

static K ncmp(I f, K ff, K x, K y){
    static void *Fc_[3*11]={
#define $ (void*)
      $ eqi,$ eqf,$ eqcC,$ eqiI,$ eqfF,$ eqCc,$ eqIi,$ eqFf,$ eqC,$ eqI,$ eqF,
      $ lti,$ ltf,$ ltcC,$ ltiI,$ ltfF,$ ltCc,$ ltIi,$ ltFf,$ ltC,$ ltI,$ ltF,
      $ gti,$ gtf,$ gtcC,$ gtiI,$ gtfF,$ gtCc,$ gtIi,$ gtFf,$ gtC,$ gtI,$ gtF};
    I av,yn,xn,n;
    K r=0;
    T t=dtypes(x,y);
    if(t>T_Lt){
      r=dkeys(x,y);
      return key(r,ncmp(f,ff,dvals(x),dvals(y)),t);
    }
    if(t==T_Lt){
      if(x==0ull) return trap(E_Value);
      n=conform(x,y);
      switch(n){
        case 1: n=nn(y); break;
        default: n=nn(x); break;
      }
      r=mk(T_Lt,n); K* rp=xK(r);
      switch(ff){
        case F_Les: for(I i=0;i<n;i++){ rp[i]=Les(ati(rx(x),i),ati(rx(y),i)); }
          break;
        case F_Eql: for(I i=0;i<n;i++){ rp[i]=Eql(ati(rx(x),i),ati(rx(y),i)); }
          break;
        case F_Mor: for(I i=0;i<n;i++){ rp[i]=Mor(ati(rx(x),i),ati(rx(y),i)); }
          break;
        default: return trap(E_Nyi);
      }
      dx(x); dx(y);
      return uf(r);
    }
    t=maxtype(x,y);
    x=uptype(x,t); y=uptype(y,t);
    av=conform(x,y);
    if(!av){
      switch(t){
        case T_ct: return Ki(((I(*)(C,C))Fc_[f])((C)x,(C)y));
        case T_st:
        case T_it: return Ki(((I(*)(I,I))Fc_[f])((I)x,(I)y));
        case T_ft:
          r=Ki(((I(*)(F,F))Fc_[1+f])(xF(x)[0],xF(y)[0]));
          dx(x); dx(y);
          return r;
      }
      return trap(E_Type);
    } else if(av==1){ // ie i < I
      if(rr(y)) return rr_ncmp(ff, x, y);
      yn=nn(y);
      r=mk(T_It,yn);
      if(!yn){
        dx(x); dx(y);
        return r;
      }
      switch(t){
        case T_ct: ((void(*)(C,C*,I*,I))Fc_[2+f])((C)x,xC(y),xI(r),yn); break;
        case T_st:
        case T_it: ((void(*)(I,I*,I*,I))Fc_[3+f])((I)x,xI(y),xI(r),yn); break;
        case T_ft: ((void(*)(F,F*,I*,I))Fc_[4+f])(xF(x)[0],xF(y),xI(r),yn);
          dx(x);
          break;
      }
      dx(y); return r;
    } else if(av==2){ // ie I < i
      if(rr(x)) return rr_ncmp(ff, x, y);
      xn=nn(x);
      r=mk(T_It,xn);
      if(!xn){
        dx(x); dx(y);
        return r;
      }
      switch(t){
        case T_ct: ((void(*)(C*,C,I*,I))Fc_[5+f])(xC(x),(C)y,xI(r),xn); break;
        case T_st:
        case T_it: ((void(*)(I*,I,I*,I))Fc_[6+f])(xI(x),(I)y,xI(r),xn); break;
        case T_ft: ((void(*)(F*,F,I*,I))Fc_[7+f])(xF(x),xF(y)[0],xI(r),xn);
          dx(y);
          break;
      }
      dx(x); return r;
    } else { // ie I < I
      if(rr(x)) x=er(x);
      if(rr(y)) y=er(y);
      n=nn(x);
      r=(t==T_it) ? use2(x,y) : mk(T_It,nn(x));
      if(n){
        switch(t){
          case T_ct: ((void(*)(C*,C*,I*,I))Fc_[f+8])(xC(x),xC(y),xI(r),n); break;
          case T_st:
          case T_it: ((void(*)(I*,I*,I*,I))Fc_[f+9])(xI(x),xI(y),xI(r),n); break;
          case T_ft: ((void(*)(F*,F*,I*,I))Fc_[f+10])(xF(x),xF(y),xI(r),n); break;
        }
      }
      dx(x); dx(y);
      return r;
    }
}

static K taki(I x, K y);
static K td(K x){
    K r=x0(x); x=r1(x);
    I m;
    if(tp(r)!=T_St || tp(x)!=T_Lt) return trap(E_Type);
    m=maxcount((I)x,nn(x));
    I n=nn(x);
    K v=mk(T_Lt,n); K* vp=xK(v);
    for(I i=0;i<n;i++){ vp[i]=taki(m,ati(rx(x),i)); }
    dx(x);
    x=l2(r,uf(v));
    xI(x)[-3]=m;
    return (K)((I)x) | ((K)(T_Tt)<<59);
}

static K explode(K x){
    K r=0,k;
    I xn;
    T xt=tp(x);
    if(xt<16){
      r=l1(x);
    } else if(xt==T_Dt){
      r=mk(T_Lt,1);
      xK(r)[0]=x;
    } else if(xt<T_Lt){
      xn=nn(x);
      r=mk(T_Lt,xn);
      if(!xn){
        dx(x);
        return r;
      }
      K* rp=xK(r);
      if(rr(x)){
        for(I i=0,a=xI(x)[0]; i<xn; i++) rp[i]=Ki(a+i);
        dx(x); return r;
      }
      I i=0;
      switch(xt){
        case T_Ct: do rp[i]=Kc(xC(x)[i]); while(++i<xn);
          break;
        case T_It: do rp[i]=Ki(xI(x)[i]); while(++i<xn);
          break;
        case T_St: do rp[i]=Ks(xI(x)[i]); while(++i<xn);
          break;
        case T_Ft: do rp[i]=Kf(xF(x)[i]); while(++i<xn);
          break;
      }
      dx(x);
    } else if(xt==T_Lt){
      r=x;
    } else if(xt==T_Tt){
      xn=nn(x);
      k=x0(x); x=r1(x);
      r=mk(T_Lt,xn);
      x=Flp(x); K*xp=xK(x),*rp=xK(r);
      for(I i=0;i<xn;i++){ rp[i]=key(rx(k),rx(xp[i]),T_Dt); }
      dx(x); dx(k);
    }
    return r;
}

static K Enl(K x){
    K r;
    I s;
    T t=tp(x);
    if(t>0 && t<7){
      t+=16;
      r=mk(t,1);
      s=sz(t);
      switch(s>>2){
        case 0: xC(r)[0]=(C)x; break;
        case 1: xI(r)[0]=(I)x; break;
        case 2: xK(r)[0]=xK(x)[0]; break;
        case 3: break;
        case 4:
          xK(r)[0]=xK(x)[0];
          xK(r)[1]=xK(x)[1];
          break;
      }
      dx(x);
      return r;
    }
    if(t==T_Dt && tp(xK(x)[0])==T_St){
      K k=x0(x); I xn;
      x=r1(x); xn=nn(x);
      r=mk(T_Lt,xn); K* rp=xK(r);
      for(I i=0;i<xn;i++){ rp[i]=Enl(ati(rx(x),i)); }
      dx(x);
      return Flp(key(k, uf(r), T_Dt));
    }
    return l1(x);
}

static K Drp(K x, K y);
static K atd(K x, K y);
static K keyt(K x, K y){
    if(tp(x)!=T_st || tp(y)!=T_Tt) return trap(E_Type);
    x=rx(x); y=rx(y);
    K a=atd(y,x);
    K t=key(Enl(x), Enl(a), T_Tt);
    return key(t,Drp(x,y),T_Dt);
}

static K key(K x, K y, T t){
    I xn;
    T xt=tp(x);
    T yt=tp(y);
    if(xt<16){
      if(xt==T_st && yt==T_Tt) return keyt(x,y);
      x=Enl(x);
    }
    xn=nn(x);
    if(t==T_Tt){
      if(xn>0) xn=nn(xK(y)[0]);
    }
    else if(yt<16) return trap(E_Type);
    else if(xn!=nn(y)) return trap(E_Length);
    x=l2(x,y);
    xI(x)[-3]=xn;
    return (K)((I)(x)) | ((K)(t)<<59);
}

static K df_ech(K x){ return l2t(x,0,T_df); }
static K df_rdc(K x){ return l2t(x,1,T_df); }
static K df_scn(K x){ return l2t(x,2,T_df); }

static K rd0(I, I, I){ return (K)0; }

static F sumf(I x, I a, I n){
    F r=0.;
    I c=n-a;
    if(c<128){ F* xp=xF(x);
      for(I i=a; i<n; r+=xp[i], i++) {}
      return r;
    }
    c=c/2;
    return sumf(x, a, a+c) + sumf(x, a+c, n);
}

static I sumi(I x, I xn){
    I*xp=xI(x),r=0,i=0;
    for(i=0; i<xn; i++){ r+=xp[i]; }
    return r;
}

static K sum(K y, T t, I n){
    I s;
    switch(t){
      case T_Ct:
        s=0; for(I i=0;i<n;i++) s+=xC(y)[i];
        return Kc(s);
      case T_It:
        if(rr(y)){
          I a=xI(y)[0];
          s=n/2 * (2*a + (n-1));
          return Ki(s);
        } else return Ki(sumi((I)y,n));
      case T_St: return (K)0;
      case T_Ft:
        return Kf(0.+sumf((I)y,0,n));
      default: return (K)0;
    }
}

static K prd(K y, T t, I n){
    F f;
    I xp=1;
    switch(t){
      case T_Ct:
        for(I i=0;i<n;i++) xp=xp*xC(y)[i];
        return Kc(xp);
      case T_It:
        if(rr(y)){
          I a=xI(y)[0]; if(!a) return Ki(0);
          for(I i=0; i<n; ++i){ xp=xp*(a+i); }
        } else {
          for(I i=0; i<n; i++){ xp=xp*xI(y)[i]; }
        }
        return Ki(xp);
      case T_St: return (K)0;
      case T_Ft:
        f=1.;
        for(I i=0; i<n; i++){ f=f*xF(y)[i]; }
        return Kf(f);
      default: return (K)0;
    }
}

static K Rev(K x);
static K Enc(K x, K y){
    K r,xi;
    T xt=tp(x);
    I n=0,yn;
    if(xt==T_It){ n=nn(x); }
    r=mk(T_It,0);
    yn=(tp(y)<16) ? 1 : nn(y);
    for(;;){
      xi=ati(rx(x),--n);
      r=Cat(r,Enl(idiv(rx(y),xi,1)));
      y=idiv(y,xi,0);
      if(!n || (n<0 && (I)(y)==0)) break;
      if(tp(y)>16 && n<0 && !sumi((I)y, yn)) break;
    }
    dx(x); dx(y);
    return Rev(r);
}

static K Dec(K x, K y){
    if(tp(y)<16) return trap(E_Type);
    K r=Fst(rx(y));
    I n=nn(y);
    for(I i=1; i<n; i++) r=Add(ati(rx(y),i),Mul(ati(rx(x),i),r));
    dx(x); dx(y);
    return r;
}

static K sums(I y, T t, I n){
    if(t!=T_It) return (K)0;
    K r=mk(T_It,n);
    I *yp=xI(y),*rp=xI(r),s=0,i=0;
    if(rr(y)){
      I a=xI(y)[0];
      do rp[i]=(s+=a+i); while(++i<n);
    } else {
      do rp[i]=(s+=yp[i]); while(++i<n);
    }
    return r;
}

static K prds(I y, T t, I n){
    if(t!=T_It) return (K)0;
    K r=mk(T_It,n);
    I *yp=xI(y),*rp=xI(r),s=1,i=0;
    if(rr(y)){
      I a=yp[0]; if(!a){ memset(rp,0,(size_t)n*4); return r; }
      do rp[i]=(s*=a+i); while(++i<n);
    } else {
      do rp[i]=(s*=yp[i]); while(++i<n);
    }
    return r;
}

static void Atx_(K x, K y);
static void Ecl_(K x, K y);
static K atv(K x, K y);
static K Atx(K x, K y);
static K ndrop(I n, K y);
static void atdepth_(K x, K y){
    K f;
    T xt=tp(x);
    if(xt<16) trap(E_Type);
    f=Fst(rx(y));
    if(!f) f=seq(nn(x));
    x=Atx(x,f);
    if(nn(y)==1){
      dx(y);
      frame->a=x;
      return;
    }
    y=ndrop(1,y);
    if(tp(f)>16){
      if(nn(y)==1 && xt==T_Tt) Atx_(x,Fst(y));
      else Ecl_(F_Cal,l2(x,y));
    } else atdepth_(x,y);
}

static void cal_(K x, K y);
static void train_(K f, K x, K y){
    I n=nn(f);
    if(frame==frame_end){ trap(E_Limit); return; }
    ++frame;
    frame->op=O_TRAIN;
    frame->p=1; frame->e=n;
    frame->b=f;
    if(y==0) cal_(r0(f),l1(x));
    else cal_(x0(f),l2(x,y));
}

static void prj_(K x, K y);
static I issue(K x);
static void lambda_(K f, K x){
    K c,lo,sa;
    I fn,xn,nl,p,md;
    fn=nn(f); xn=nn(x);
    if(xn<fn){
      prj_(rx(f),x);
      return;
    }
    if(xn!=fn){ trap(E_Rank); return; }
    if(frame==frame_end){ trap(E_Limit); return; }
    c=xK(f)[0]; lo=xK(f)[1]; md=(I)xK(f)[3];
    nl=nn(lo);
    sa=mk(T_It,2*nl);
    K *sap=xK(sa),*vp=xK(val_[md]),*xp=xK(x);
    I *lp=xI(lo);
    rl(x); dx(x);
    for(I i=0; i<nl; ++i){
      p=lp[i];
      sap[i]=vp[p];
      vp[p]=(i<fn)?*xp++:0;
    }
    ++frame;
    frame->op=O_LAMBDA;
    frame->b=sa;
    frame->x=lo; frame->j=nl;
    frame->p=IP; frame->e=IE;
    frame->i=mod_;
    mod_=md;
    issue(rx(c));
    dx(f);
}
static void epi_(void){
    K *sap=xK(frame->b),*vp=xK(val_[mod_]);
    I *lp=xI(frame->x);
    for(I i=0; i<frame->j; i++){
      I p=lp[i];
      dx(vp[p]);
      vp[p]=sap[i];
      sap[i]=0;
    }
    dx(frame->b);
    mod_=frame->i;
    IP=frame->p; IE=frame->e;
}

K Native(K x, K y);
static void native_(K f, K x){
    I fn=nn(f),xn=nn(x);
    if(xn<fn){
      prj_(rx(f),x);
      return;
    }
    if(xn!=fn) trap(E_Rank);
    frame->a=Native(r0(f),x);
}

static K dcat(K x, K y);
static K ucat(K x, K y){
    K r;
    I xn,yn,s;
    T xt=tp(x);
    if(xt>T_Lt) return dcat(x,y);
    xn=nn(x); yn=nn(y);
    if(rr(x) && rr(y)){
      I x0=xI(x)[0],y0=xI(y)[0];
      if(y0==x0+xn){
        dx(y); x=use(x);
        xI(x)[-3]=xn+yn;
        return x;
      }
    }
    r=uspc(x,xt,yn);
    s=sz(xt);
    if(xt==T_Lt){ rl(y); }
    if(rr(y)){
      I a=xI(y)[0]; I* rp=xI(r);
      for(I i=0; i<yn; i++) rp[xn+i]=a+i;
    } else Memorycopy((I)(r)+s*xn, (I)y, s*yn);
    dx(y); return r;
}

static K dcat(K x, K y){
    K r,q;
    T t=tp(x),yt=tp(y);
    if(t==T_Tt && !match(xK(x)[0],xK(y)[0])){
      return ucat(explode(x),explode(y));
    }
    r=x0(x); x=r1(x);
    q=x0(y); y=r1(y);
    if(t==T_Dt){
      r=Cat(r,q);
      return key(r,Cat(x,y),T_Dt);
    } else if(t==T_Tt && yt==T_Tt){
      dx(q);
      I n=nn(x);
      if(nn(y)!=n) return trap(E_Length);
      K c=mk(T_Lt,n); K* cp=xK(c);
      for(I i=0; i<n; i++){ cp[i]=Cat(ati(rx(x),i),ati(rx(y),i)); }
      dx(x); dx(y);
      return key(r,uf(c),t);
    } else return trap(E_Type);
}

static K Cat(K x, K y){
    T xt=tp(x), yt=tp(y);
    if((xt==T_Tt)&&(yt==T_Dt)){
      y=Enl(y); yt=T_Tt;
    }
    if((xt&15)==(yt&15)){
      if(xt<16){ x=Enl(x); }
      return (yt<16) ? cat1(x,y) : ucat(x,y);
    } else if(xt==T_Lt && yt<16 && nn(x)>0){
      return cat1(x,y);
    }
    x=uf(Cat(explode(x),explode(y)));
    if(!nn(x)){
      dx(x); return mk(xt|16,0);
    }
    return x;
}

static K flat(K x){
    K r=mk(T_Lt,0);
    I xn=nn(x); K* xp=xK(x);
    for(I i=0; i<xn; i++) r=Cat(r,rx(xp[i]));
    dx(x);
    return r;
}

static K cat1(K x, K y){
    T xt=tp(x);
    I xn=nn(x);
    K r=uspc(x,xt,1);
    switch(sz(xt)){
      case 1: xC(r)[xn]=(C)y; break;
      case 4: xI(r)[xn]=(I)y; break;
      case 8:
        if(xt==T_Ft){
          xF(r)[xn]=xF(y)[0];
          dx(y);
        } else { xK(r)[xn]=y; }
        break;
    }
    return r;
}

static K ncat(K x, K y){
    T xt=tp(x);
    if(xt<16) x=Enl(x);
    xt=maxtype(x,y);
    x=uptype(x,xt); y=uptype(y,xt);
    return cat1(x,y);
}

static K lup(K x){ return rx(xK(val_[mod_])[(I)x] ? xK(val_[mod_])[(I)x] : xK(val_[0])[(I)x]); }
static K Val(K x){
    K r=0;
    T xt=tp(x);
    if(xt==T_st) return lup(x);
    if(xt==T_lf || xt==T_xf){
      r=l2(x0(x),x1(x));
      if(xt==T_lf) r=cat1(r,x2(x));
      r=cat1(r,Ki(nn(x)));
      dx(x);
      return r;
    }
    if(xt>T_Lt){
      r=x1(x); dx(x); return r;
    }
    return trap(E_Type);
}

K trap(I x){
    frame=&frames[0];
    ERR[0]=0;
    if(U_==NULL){ panic(x); } // no memory
    K s;
    I a,b,off;
    if      (x == E_Type)   strcpy(ERR, "'type  \n");
    else if (x == E_Value)  strcpy(ERR, "'value \n");
    else if (x == E_Index)  strcpy(ERR, "'index \n");
    else if (x == E_Length) strcpy(ERR, "'length\n");
    else if (x == E_Rank)   strcpy(ERR, "'rank  \n");
    else if (x == E_Parse)  strcpy(ERR, "'parse \n");
    else if (x == E_Stack)  strcpy(ERR, "'stack \n");
    else if (x == E_Grow)   strcpy(ERR, "'grow  \n");
    else if (x == E_Unref)  strcpy(ERR, "'unref \n");
    else if (x == E_Io)     strcpy(ERR, "'io    \n");
    else if (x == E_Limit)  strcpy(ERR, "'limit \n");
    else if (x == E_Nyi)    strcpy(ERR, "'nyi   \n");
    else                    strcpy(ERR, "'err   \n");
    if(IP>0 && IP-8<IE){
      K u=xK(IP-8)[0];
      SRCP=0xffffff&(I)(u>>32);
    } else if(PP>=0 && PP<PE){
      K u=xK(PP)[0];
      SRCP=0xffffff&(I)(u>>32);
    }
    if(!SRCP){
      strcpy(ERR+8, "0\n"); off=2;
    } else {
      s=src(); C* sp=xC(s);
      a=maxi(SRCP-30, 0);
      for(I i=a;i<SRCP;i++){
        if(sp[i]=='\n'){ a=1+i; }
      }
      b=mini(nn(s), SRCP+30);
      for(I i=SRCP;i<b;i++) {
        if(sp[i]=='\n'){
          b=i;
          break;
        }
      }
      memcpy(ERR+8, M_+(I)(s)+a, (size_t)(b-a));
      off=b-a;
      if(SRCP>a){
        strcpy(ERR+8+off, "\n"); ++off;
        b=(SRCP-a)-1;
        memset(ERR+8+off, ' ', (size_t)b); off+=b;
      }
    }
    strcpy(ERR+8+off, "^\n");
    panic(x);
    return (K)0;
}

static I isquotedv(K x){ return (I)((I)(x)>=QUOTE && tp(x)==0); }
static K quote(K x){ return (x + QUOTE); }
static K unquote(K x){ return (x - QUOTE); }

static void push(K x){
    xK(SP)[0]=x;
    SP+=8;
    if(SP==512){ trap(E_Stack); }
}

static K pop(void){
    SP-=8;
    if(SP<POS_STACK){ return trap(E_Stack); }
    K r=xK(SP)[0];
    return r;
}


static I issue(K x){
    SRCP=0;
    I xn=nn(x);
    if(!xn){
      dx(x); return 0;
    }
    if(frame>&frames[0]){
      if(frame==frame_end){ trap(E_Limit); return 0; }
      ++frame;
    }
    IP=(I)x; IE=IP+8*xn;
    frame->j=0;
    frame->a=(K)0; // accu
    frame->b=(K)0;
    frame->op=0;
    return 1;
}

static I step(void);
static K exec(K x){
    if(issue(x)!=1) return (K)0;
    do {} while(step());
    dx(pop()); dx(x);
    return frame->a;
}

static K lst(K n){
    I rn=(I)n;
    K r=mk(T_Lt,rn); K* rp=xK(r);
    for(I i=0; i<rn; i++) rp[i]=pop();
    return uf(r);
}

static I fndl(K x, K y){
    dx(y); K* xp=xK(x);
    for(I r=0,xn=nn(x); r<xn; r++){
      if(match(xp[r],y)) return r;
    }
    return nai;
}

static K uqf(K x){ // uniq elements in list (deep compare)
    I xn=nn(x);
    K r=mk(T_Lt,0),xi;
    if(xn>0) r=cat1(r,ati(rx(x),0));
    for(I i=1; i<xn; i++){
      xi=ati(rx(x), i);
      if(fndl(r,rx(xi))==nai) r=cat1(r,xi);
      else dx(xi);
    }
    dx(x);
    return uf(r);
}

static K grpdt(K x){
    K k=x0(x);
    K v=x1(x);
    K rk=mk(T_Lt, 0);
    K rv=mk(T_Lt, 0);
    for(I i=0; i<nn(k); i++){
      K a=ati(rx(v), i);
      K b=ati(rx(k), i);
      I j=fndl(rv, rx(a));
      if(j==nai){
        K e=Enl(b);
        rk=cat1(rk, e);
        rv=cat1(rv, a);
      } else {
        K e=xK(rk)[j];
        e=cat1(e, b);
        xK(rk)[j]=e;
        dx(a);
      }
    }
    dx(x);
    return key(uf(rv), rk, T_Dt);
}
static K Srt(K x);
static K grpl(K x){
    I xn;
    T xt=tp(x);
    if(xt<16 || xt>T_Lt) return trap(E_Type);
    // 74145: TODO: this can be optimized a lot
    K k=Srt(uqf(rx(x)));
    K v=mk(T_Lt,0);
    xn=nn(x);
    for(I i=0; i<nn(k); i++){
      K a=ati(rx(k), i);
      K r=mk(T_Lt,0);
      for(I j=0; j<xn; j++){
        K b=ati(rx(x), j);
        if((I)Eql(a,b)==1){
          r=cat1(r, Ki(j));
        }
      }
      v=cat1(v, uf(r));
    }
    return key(k, v, T_Dt);
}

static K grptt(K x, K y){
    x=rx(x); y=rx(y);
    T xt=tp(x);
    K a=Drp(x,y),b,c,r,k,v,vi;
    I nk;
    if(xt==T_st){
      b=atd(y,x); c=grpl(b);
      r=mk(T_Lt,0);
      k=x0(c); v=x1(c);
      nk=nn(k);
      for(I i=0; i<nk; i++){
        vi=ati(rx(v),i);
        r=cat1(r,atv(rx(a),vi));
      }
      c=r;
    } else if(xt==T_it){
      b=ati(y,(I)x); c=grpdt(b);
      r=mk(T_Lt,0);
      k=x0(c); v=x1(c);
      nk=nn(k);
      for(I i=0; i<nk; i++){
        vi=ati(rx(v),i);
        r=cat1(r,atd(rx(a),vi));
      }
      c=key(k, r, T_Dt);
    } else return trap(E_Type);
    return c;
}

static I inC(I x, I y, I n){
    C* yp=xC(y);
    for(I i=0; i<n; i++){
      if(x==yp[i]) return i;
    } return n;
}

static I inI(I x, I y, I n){
    I* yp=xI(y);
    for(I i=0; i<n; i++){
      if(x==yp[i]) return i;
    } return n;
}

static I inF(F x, I y, I n){
    F* yp=xF(y);
    for(I i=0; i<n; i++){
      if(eqf(x,yp[i])) return i;
    } return n;
}

static K ine(K x, K y, T xt){
    I n=nn(y);
    if(xt==T_it && rr(y)){
      I a=xI(y)[0];
      return Ki( (I)x>=a && (I)x<a+n );
    }
    I xp=(I)x,yp=(I)y,i=0;
    switch(xt){
      case T_ct: i=inC(xp,yp,n); break;
      case T_st:
      case T_it: i=inI(xp,yp,n); break;
      case T_ft:
        dx(x);
        i=inF(xF(x)[0],yp,n);
        break;
    }
    return Ki((I)(i!=n));
}

static K In(K x, K y){
    T xt=tp(x),yt=tp(y);
    if(xt==yt && xt>16){
      I xn=nn(x);
      K r=mk(T_Lt,xn); K* rp=xK(r); // TODO: can we make T_It right away?
      for(I i=0;i<xn;i++){ rp[i]=In(ati(rx(x),i),rx(y)); }
      dx(x); dx(y);
      return uf(r);
    }
    if((xt+16)!=yt) return trap(E_Type);
    dx(y);
    return ine(x,y,xt);
}

static I rnd(void){
    I r=rand_;
    r=(r^(r<<13));
    r=(r^(r>>17));
    r=(r^(r<<5));
    rand_=r;
    return r;
}

static I randi(I n){
    UI low,thresh2,v=(UI)rnd();
    UJ prod=(UJ)(v) * (UJ)n;
    low=(UI)prod;
    if(low<(UI)n){
      thresh2=(UI)(-n) % (UI)n;
      for(;low<thresh2;){
        v=(UI)rnd();
        prod=(UJ)(v) * (UJ)n;
        low=(UI)prod;
      }
    }
    return (I)(prod>>32);
}

static K randI(I i, I n){
    K r=mk(T_It,n);
    I j=0; I* rp=xI(r);
    if(!i) for(j=0;j<n;j++) rp[j]=rnd();
    else for(j=0;j<n;j++) rp[j]=randi(i);
    return r;
}

static K shuffled(I n, I m){
    K r=mk(T_It,n); I* rp=xI(r);
    I i,j,t;
    m=mini(n-1,m);
    for(i=0;i<n;i++) rp[i]=i;
    for(i=0;i<m;i++){
      j=randi(n-i);
      t=rp[i];
      rp[i]=rp[j];
      rp[j]=t;
    }
    return r;
}

static K ntake(I n, K y);
static K deal(K x, K y){
    T yt=tp(y);
    if(yt>16){ return trap(E_Type); }
    if(tp(x)!=T_it){ return trap(E_Type); }
    if(yt==T_ct){ // 10?"z"
      x=deal(x,Ki((I)(y)-96)); I* xp=xI(x);
      K r=mk(T_Ct, nn(x)); C* rp=xC(r);
      for(I i=0; i<nn(x); i++){
        I v=xp[i];
        rp[i]=(C)v+97;
      } dx(x); dx(y); return r;
    }
    if(yt==T_st){ // 10?`z
      x=deal(x,Fst(cs(y))); C* xp=xC(x);
      K r=mk(T_St, nn(x)); I* rp=xI(r);
      for(I i=0; i<nn(x); i++){
        I v=xp[i];
        rp[i]=(I)sc(Enl(Kc(v)));
      } dx(x); dx(y); return r;
    }
    if(yt!=T_it) return trap(E_Type);
    if((I)x>0) return randI((I)y,(I)x); // 10?100
    return ntake(-(I)x,shuffled((I)y,-(I)x)); // -10?100
}

static K fndXs(K x, K y, T t, I yn){
    K r;
    I xn,a,b;
    xn=nn(x);
    a=(I)min(x,t,xn);
    b=1+(((I)max(x,t,xn)-a));
    if(b>256 && b>yn) return (K)0;
    if(t==T_St){
      x=use(x);
      x=(K)((I)x) | ((K)(T_It)<<59);
      y=use(y);
      y=(K)((I)y) | ((K)(T_It)<<59);
    }
    r=ntake(b,Ki(nai)); I* rp=xI(r);
    xn=nn(x);
    if(t==T_Ct){ C* xp=xC(x);
      for(I i=xn-1;i>=0;i--) rp[xp[i]-a]=i;
    } else { I* xp=xI(x);
      for(I i=xn-1;i>=0;i--) rp[xp[i]-a]=i;
    }
    dx(x);
    switch(tp(y)){
      case T_Ct:{ C* yp=xC(y); I yn=nn(y);
        for(I i=0;i<yn;i++) yp[i]=yp[i]-(C)a;
      } break;
      case T_It:{ I* yp=xI(y); I yn=nn(y);
        if(rr(y)){
          I y0=yp[0]; dx(y);
          y=mk(T_It, yn); yp=xI(y);
          for(I i=0;i<yn;i++) yp[i]=y0+i-a;
        } else for(I i=0;i<yn;i++) yp[i]=yp[i]-a;
      } break;
      default: return trap(E_Type);
    }
    return Atx(r,y);
}

static I fnde(K x, K y, T t){
    I r=0,xn,xp,yp;
    xn=nn(x);
    if(!xn) return nai;
    xp=(I)x; yp=(I)y;
    if(rr(x)){
      for(I i=0,a=xI(x)[0];i<xn;i++){ if((I)y==a+i) return i; }
      return nai;
    }
    switch(t){
      case T_ct: r=inC(yp,xp,xn); break;
      case T_st:
      case T_it: r=inI(yp,xp,xn); break;
      case T_ft: r=inF(xF(y)[0],xp,xn); break;
    }
    if(r==xn) return nai;
    return r;
}

static I findat(I x, I y, I n, I yoff){
    C *xp=xC(x),*yp=xC(y); I i=0;
    do{ if(xp[i]!=yp[i+yoff]) return 0;
    }while(++i<n);
    return 1;
}

static K Find(K x, K y){
    K r;
    I xn,yn,xp,yp,e;
    T xt=tp(x); T yt=tp(y);
    if(xt!=yt || xt!=T_Ct) return trap(E_Type);
    xn=nn(x); yn=nn(y);
    if(xn==0 || yn==0){
      dx(x); dx(y);
      return mk(T_It,0);
    }
    r=mk(T_It,0);
    xp=(I)x; yp=(I)y;
    I yoff=0;
    e=yn+1-xn;
    do{
      if(findat(xp,yp,xn,yoff)){
        r=cat1(r, Ki(yoff));
        yoff+=xn;
      } else yoff++;
    }while(yoff<e);
    dx(x); dx(y);
    return r;
}

static K Fnd(K x, K y){
    I yn;
    K r=0,yi;
    T xt=tp(x),yt=tp(y);
    if(xt<16) return trap(E_Type);
    if(xt>T_Lt){
      if(xt==T_Tt) return trap(E_Nyi); // TODO: implement?
      r=x0(x);
      return Atx(r,Fnd(r1(x),y));
    } else if(xt==T_Ct && yt==xt){
      return Find(y, x);
    } else if(xt==yt){
      yn=nn(y);
      if(xt<T_Ft && ((yn>4 && xt==T_Ct) || yn>8)){
        r=fndXs(x,y,xt,yn);
        if(r) return r;
      }
      r=mk(T_It,yn); I* rp=xI(r);
      if(xt==T_Lt){
        for(I i=0; i<yn; i++){
          rp[i]=fndl(x, rx(xK(y)[i]) );
        }
      } else {
        for(I i=0; i<yn; i++){
          yi=ati(rx(y),i);
          rp[i]=fnde(x, yi, xt-16);
          dx(yi);
        }
      }
    } else if(xt==yt+16){
      r=Ki(fnde(x,y,yt));
    } else if(xt==T_Lt){
      r=Ki(fndl(x,rx(y)));
    } else if(yt==T_Lt){
      yn=nn(y);
      r=mk(T_Lt,yn); K* p=xK(r);
      for(I i=0;i<yn;i++){ p[i]=Fnd(rx(x),ati(rx(y),i)); }
      dx(x); dx(y);
      return uf(r);
    } else { return trap(E_Type); }
    dx(x); dx(y);
    return r;
}


static I mtC(I xp, I yp, I n){
    I vn=n>>3,i=0;
    for(; i<vn; ++i){
      if(xJ(xp)[i]!=xJ(yp)[i]) return 0;
    }
    C *cx=xC(xp),*cy=xC(yp);
    for(; i<n; ++i){
      if(cx[i]!=cy[i]) return 0;
    }
    return 1;
}

static I mtI(I xp, I yp, I n){
    I vn=n>>2,i=0;
    for(; i<vn; ++i){
      if(xJ(xp)[i]!=xJ(yp)[i]) return 0;
    }
    I *ix=xI(xp),*iy=xI(yp);
    for(; i<n; ++i){
      if(ix[i]!=iy[i]) return 0;
    }
    return 1;
}

static I mtF(I xp, I yp, I n){
    F *fx=xF(xp),*fy=xF(yp);
    I i=0;
    do{ if(!eqf(fx[i],fy[i])) return 0;
    }while(++i<n);
    return 1;
}

static I match(K x, K y){
    I yn=0,xn;
    T xt, yt;
    if(x==y) return 1;
    xt=tp(x); yt=tp(y);
    if(xt!=yt) return 0;
    if(xt<T_ft) return (I)((I)x==(I)y);
    if(xt>16){
      xn=nn(x); yn=nn(y);
      if(xn!=yn) return 0;
      if(!xn) return 1;
      if(rr(x)){
        I a=xI(x)[0]; I* yp=xI(y);
        if(rr(y)){ return (I)(a==yp[0]); }
        for(I i=0;i<xn;i++,a++) if(a!=yp[i]) return 0;
        return 1;
      }
      if(rr(y)){
        I b=xI(y)[0]; I* xp=xI(x);
        for(I i=0;i<xn;i++,b++) if(b!=xp[i]) return 0;
        return 1;
      }
      switch(xt){
        case T_Ct: return mtC((I)x,(I)y,xn);
        case T_It: return mtI((I)x,(I)y,xn);
        case T_St: return mtI((I)x,(I)y,xn);
        case T_Ft: return mtF((I)x,(I)y,xn);
        case T_Lt:{
          K *xp=xK(x),*yp=xK(y);
          for(I i=0; i<xn; i++){
            if(!match(xp[i], yp[i])) return 0;
          } return 1;
        }
        default:
          if(match(xK(x)[0], xK(y)[0])){
            return match(xK(x)[1], xK(y)[1]);
          }
          return 0;
      }
    }
    switch(xt){
      case T_ft: return eqf(xF(x)[0], xF(y)[0]);
      case T_cf: yn=nn(y); break;
      case T_df: yn=2; break;
      case T_pf: yn=3; break;
      case T_lf: return match(xK(x)[2], xK(y)[2]);
      default: return (I)(xK(x)==xK(y));
    }
    for(;yn>0;){
      --yn;
      if(!match(xK(x)[yn], xK(y)[yn])) return 0;
    }
    return 1;
}

static K atv(K x, K y){
    K r,miss;
    I yn,xn,xi;
    T t=tp(x);
    yn=nn(y);
    if(tp(y)!=T_It) return trap(E_Type);
    if(t<16){
      dx(y); return ntake(yn,x);
    }
    if(t==T_Tt){
      K v=x1(x); I nv=nn(v);
      r=mk(T_Lt,0);
      for(I i=0; i<nv; i++){
        K vv=ati(rx(v), i);
        K rv=mk(T_Lt,0);
        if(rr(y)){
          for(I j=0,a=xI(y)[0]; j<yn; j++){
            rv=cat1(rv, ati(rx(vv), a+j));
          }
        } else {
          for(I j=0; j<yn; j++){
            rv=cat1(rv, ati(rx(vv), xI(y)[j]));
          }
        }
        r=cat1(r, uf(rv));
      }
      return key(x0(x),r,T_Tt);
    }
    xn=nn(x);
    miss=missing(t-16);
    if(rr(x)){
      I x0=xI(x)[0];
      if(rr(y)){ // (!5)@!5
        I y0=xI(y)[0];
        if(y0>=x0 && (y0+yn)<=(x0+xn)){
          dx(miss); dx(y);
          return atn(x,y0,yn);
        }
        r=mk(t,yn); I* rp=xI(r);
        for(I i=0; i<yn; i++){
          xi=y0+i;
          if((UI)(xi)>=(UI)(xn)) rp[i]=(I)miss;
          else rp[i]=x0+xi;
        }
      } else {
        r=mk(t,yn); I* rp=xI(r);
        for(I i=0; i<yn; i++){
          xi=xI(y)[i];
          if((UI)(xi)>=(UI)(xn)) rp[i]=(I)miss;
          else rp[i]=x0+xi;
        }
      }
      dx(miss); dx(x); dx(y);
      return r;
    }
    r=mk(t,yn);
    switch(sz(t)>>2){
      case 0:{ C *rp=xC(r),*xp=xC(x);
        if(rr(y)){
          for(I i=0,a=xI(y)[0]; i<yn; i++){
            xi=a+i;
            if((UI)(xi)>=(UI)(xn)) rp[i]=(C)miss;
            else rp[i]=xp[xi];
          }
        } else {
          for(I i=0; i<yn; i++){
            xi=xI(y)[i];
            if((UI)(xi)>=(UI)(xn)) rp[i]=(C)miss;
            else rp[i]=xp[xi];
          }
        }
      } break;
      case 1:{ I *rp=xI(r),*xp=xI(x);
        if(rr(y)){
          for(I i=0,a=xI(y)[0]; i<yn; i++){
            xi=a+i;
            if((UI)(xi)>=(UI)(xn)) rp[i]=(I)miss;
            else rp[i]=xp[xi];
          }
        } else {
          for(I i=0; i<yn; i++){
            xi=xI(y)[i];
            if((UI)(xi)>=(UI)(xn)) rp[i]=(I)miss;
            else rp[i]=xp[xi];
          }
        }
      } break;
      case 2: { K *rp=xK(r),*xp=xK(x);
        if(rr(y)){
          for(I i=0,a=xI(y)[0]; i<yn; i++){
            xi=a+i;
            if((UI)(xi)>=(UI)(xn)){
              if(t==T_Lt) rp[i]=miss;
              else rp[i]=xK(miss)[0];
            } else rp[i]=xp[xi];
          }
        } else {
          for(I i=0; i<yn; i++){
            xi=xI(y)[i];
            if((UI)(xi)>=(UI)(xn)){
              if(t==T_Lt) rp[i]=miss;
              else rp[i]=xK(miss)[0];
            } else rp[i]=xp[xi];
          }
        }
      } break;
    }
    if(t==T_Lt){
      rl(r); r=uf(r);
    }
    dx(miss); dx(x); dx(y);
    return r;
}

static K Atx(K x, K y){
    K r=0;
    T xt=tp(x),yt=tp(y);
    if(xt<16 && isfunc(xt)) return trap(E_Type);
    if(xt>T_Lt && yt<T_Lt){
      r=x0(x);
      x=r1(x);
      if(xt==T_Tt && (yt&15)==T_it){
        I xn=nn(x);
        K v=mk(T_Lt,xn); K* vp=xK(v);
        for(I i=0; i<xn; i++){ vp[i]=Atx(ati(rx(x),i),rx(y)); }
        dx(x); dx(y);
        return key(r,uf(v),T_Dt+(T)(yt==T_It));
      }
      return Atx(x,Fnd(r,y));
    }
    if(yt<T_It){
      y=uptype(y,T_it);
      yt=tp(y);
    }
    if(yt==T_It) return atv(x,y);
    if(yt==T_it) return ati(x,(I)y);
    if(yt==T_Lt){
      I yn=nn(y);
      r=mk(T_Lt,yn); K* rp=xK(r);
      for(I i=0;i<yn;i++){ rp[i]=Atx(rx(x),ati(rx(y),i)); }
      dx(x); dx(y);
      return uf(r);
    }
    if(yt==T_Dt){
      r=x0(y);
      return key(r,Atx(x,r1(y)),T_Dt);
    }
    return trap(E_Type);
}

static K ati(K x, I i){
    I s;
    K r=0;
    T t=tp(x);
    if(t<16) return x;
    if(t==T_Dt || t==T_Tt) return Atx(x,Ki(i));
    if(i<0 || i>=nn(x)){
      dx(x);
      return missing(t-16);
    }
    if(t==T_It && rr(x)){
      r=Ki(xI(x)[0]+i); dx(x);
      return r;
    }
    s=sz(t);
    switch(s>>2){
      case 0: r=(K)((UI)xC(x)[i]); break;
      case 1: r=(K)((UI)xI(x)[i]); break;
      case 2: r=(K)((UJ)xJ(x)[i]); break;
    }
    if(t==T_Ft){
      r=Kf(F64reinterpret_i64((K)r));
    } else if(t==T_Lt){
      r=rx(r); dx(x);
      return r;
    }
    dx(x);
    return r | ((K)(t-16)<<59);
}

static K atd(K x, K y){
    K r=0;
    T xt=tp(x),yt=tp(y);
    if(xt<T_Dt) return trap(E_Type);
    if((yt&15)>7 || yt>T_Lt) return trap(E_Type);
    if(xt==T_Tt && (yt&15)!=T_st) return trap(E_Type);
    r=x0(x);
    x=x1(x);
    if(yt<16) return ati(x,(I)Fst(Fnd(r,y)));
    else return atv(x,Fnd(r,y));
}

static K next(void){
    if(PP==PE) return (K)0;
    K r=xK(PP)[0];
    PS=(0xffffff&(I)(r>>32));
    PP+=8;
    return r&~((K)(0xffffff)<<32);
}

static I is(I x, I m){
    // 0x02 =       10 -> ABCDFGHIJLKMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
    // 0x06 =      110 -> 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
    // 0x08 =     1000 -> \/'  
    // 0x40 =  1000000 -> ")0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ]abcdefghijklmnopqrstuvwxyz}
    // 0x80 = 10000000  -> 0123456789abcdef
    // 0x10 =    10000  -> ([{
    // 0x20 =   100000  -> )]};
    // 0x30 =   110000  -> ([{}]);
    // 0x04 =      100  -> 0123456789
    static const I CharClass[95] = {
      //            ' '    !    "      #     $     %     &     '     (     )     *     +     ,     -     .     /     0     1
                  0x00, 0x01, 0x40, 0x01, 0x01, 0x01, 0x01, 0x09, 0x10, 0x60, 0x01, 0x01, 0x01, 0x01, 0x01, 0x09, 0xc4, 0xc4,
      // 2     3     4     5     6     7     8     9     :     ;     <     =     >     ?     @     A     B     C     D     E
      0xc4, 0xc4, 0xc4, 0xc4, 0xc4, 0xc4, 0xc4, 0xc4, 0x01, 0x20, 0x01, 0x01, 0x01, 0x01, 0x01, 0x42, 0x42, 0x42, 0x42, 0x42,
      // F     G     H     I     J     K     L     M     N     O     P     Q     R     S     T     U     V     W     X     Y
      0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42,
      // Z     [     \     ]     ^     _     `     a     b     c     d     e     f     g     h     i     j     k     l     m
      0x42, 0x10, 0x09, 0x60, 0x01, 0x01, 0x00, 0xc2, 0xc2, 0xc2, 0xc2, 0xc2, 0xc2, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42,
      // n     o     p     q     r     s     t     u     v     w     x     y     z     {     |     }    ~
      0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x10, 0x01, 0x60, 0x01
    };
    if(x<' ' || x>'~') return 0;
    return m & CharClass[x-' '];
}

static K clist(K x, I n){ // constant-fold list
    K xi;
    for(I i=0;i<n;i++){
      xi=xK(x)[i];
      if(tp(xi)!=T_Lt) return (K)0;
      if(nn(xi)!=1) return (K)0;
      if(!tp(xK(xi)[0])) return (K)0;
    }
    return uf(flat(x));
}

static K rlist(K x, K p){
    I n=nn(x);
    if(!n) return l1(x);
    if(n==1) return Fst(x);
    if(p!=2){
      p=clist(x,n);
      if(p) return l1(p);
    }
    return cat1(cat1(flat(Rev(x)),Ki(n)),F_lst);
}

static K whl(K x, I xn){
    K r,y;
    I p,sum=2;
    r=cat1(Fst(rx(x)),0);
    p=nn(r)-1;
    r=cat1(r,192); // jif
    r=cat1(r,128); // drop
    for(I i=0;i<xn;i++){
      y=rx(xK(x)[i+1]);
      if(!y) continue;
      if(i) r=cat1(r,128);
      sum+=1+nn(y);
      r=ucat(r,y);
    }
    r=cat1(cat1(r,Ki(-8*(2+nn(r)))), 160); // jmp back
    xK(r)[p]=Ki(8*sum);
    dx(x);
    return ucat(l1(0),r);
}

static K iff(K x, I xn){
    K r,y;
    I p,sum=1;
    r=cat1(Fst(rx(x)),0);
    p=nn(r)-1;
    r=cat1(r,192); // jif
    r=cat1(r,128); // drop
    for(I i=0;i<xn;i++){
      y=rx(xK(x)[i+1]);
      if(!y) continue;
      if(i) r=cat1(r,128);
      sum+=1+nn(y);
      r=ucat(r,y);
    }
    r=cat1(r,256); // snj
    xK(r)[p]=Ki(8*sum); // jif
    dx(x);
    return ucat(l1(0),r);
}

static K els(K x, I xn){
    K r,y;
    I sum=0;
    r=mk(T_Lt,0);
    r=cat1(r,0);
    r=cat1(r,224); // jnj
    r=cat1(r,128); // drop
    for(I i=0;i<xn;i++){
      y=rx(xK(x)[i]);
      if(y){
        if(i) r=cat1(r,128);
        sum+=1+nn(y);
        r=ucat(r,y);
      }
    }
    xK(r)[0]=Ki(8*sum); // jnj
    dx(x);
    return ucat(l1(0),r);
}

static K elif(K x, I xn){
    K r,y;
    I p,sum=1,add;
    r=mk(T_Lt,0);
    r=cat1(r,0);
    r=cat1(r,224); // jnj
    r=Cat(r,Fst(rx(x)));
    r=cat1(r,0ull);
    p=nn(r)-1;
    r=cat1(r,192); // jif
    add=nn(r);
    r=cat1(r,128); // drop
    for(I i=0;i<xn;i++){
      y=rx(xK(x)[1+i]);
      if(!y) continue;
      if(i) r=cat1(r,128);
      sum+=1+nn(y);
      r=ucat(r,y);
    }
    r=cat1(r,256); // snj
    xK(r)[0]=Ki(8*(sum+add)); // jnj
    xK(r)[p]=Ki(8*sum); // jif
    dx(x);
    return ucat(l1(0),r);
}

static K cond(K x, I xn){
    K r;
    I nxt=0,sum=0,state=1;
    for(I i=xn-1;i>=0;i--){
      r=xK(x)[i];
      if(!r) continue;
      if(sum>0){
        state=1-state;
        r=state ? cat1(cat1(r,Ki(nxt)),192) : cat1(cat1(r,Ki(sum)),160); // 192=jif 160=jmp
        xK(x)[i]=r;
      }
      nxt=8*nn(r);
      sum+=nxt;
    }
    return flat(x);
}

static K pspec(K r, K n){ // parse special [ ] syntax
    I ln=nn(n);
    K v=xK(r)[0];
    if(nn(r)==1 && ln>2 && tp(v)==0 && (I)(v)==(I)quote((K)F_Str)){ // Str = $
      dx(r);
      return cond(n,ln); // $[..] cond
    }
    if(nn(r)!=2 || tp(v)!=T_st) return (K)0;
    K c=xK(key_)[(I)v];
    if(ln>1 && nn(c)==5){
      I found=1;
      for(I i=0; i<5; i++){
        if(xC(c)[i]!="while"[i]){ found=0; break; }
      }
      if(found){
        dx(r);
        return whl(n, ln-1); // while[..]
      }
    }
    if(ln>1 && nn(c)==2){
      I found=1;
      for(I i=0; i<2; i++){
        if(xC(c)[i]!="if"[i]){ found=0; break; }
      }
      if(found){
        dx(r);
        return iff(n, ln-1); // if[..]
      }
    }
    if(ln>0 && nn(c)==4){
      I found=1;
      for(I i=0; i<4; i++){
        if(xC(c)[i]!="else"[i]){ found=0; break; }
      }
      if(found){
        dx(r);
        return els(n, ln); // else[..]
      }
    }
    if(ln>1 && nn(c)==4){
      I found=1;
      for(I i=0; i<4; i++){
        if(xC(c)[i]!="elif"[i]){ found=0; break; }
      }
      if(found){
        dx(r);
        return elif(n, ln-1); // elif[..]
      }
    }
    return (K)0;
}

static K lastp(K x){ return xK(x)[nn(x)-1]; }
static K ldrop(I n, K x){ return explode(ndrop(n,x)); }

static K dyadic(K x, K y){
    K l=lastp(y);
    if(isquotedv(l)){ return cat1(ucat(x,ldrop(-1,y)), 32+unquote(l)); } // monad + 32 = dyad
    return cat1(ucat(x,y),96);
}

static K monadic(K x){
    K l=lastp(x),r;
    if(isquotedv(l)){
      r=ldrop(-1,x);
      if((I)l == (I)quote(F_Idy)){ // :x return lambda
        l=(0xff000000ffffffff&lastp(r));
        if(l-(F_Atx+32) < 2){ // Atx(@) or Cal(.) // 51 or 52
          return cat1(ldrop(-1,r), 110+l); // tail (321 or 322) // 161, 162
        } else {
          return cat1(cat1(r, Ki(0x100000)), 160); // identity+long jump
        }
      } else {
        return cat1(r, unquote(l));
      }
    }
    return cat1(x,32+F_Atx);
}

static K pasn(K x, K y, K r){
    K l,s,lp;
    I v,sap,xn;
    l=xK(y)[0];
    v=(I)l;
    sap=(0xffffff&(I)(l>>32));
    if((nn(y)==1 && tp(l)==0 && v==(I)quote(1)) || (v>(I)quote(64) && v<(I)quote(85))){
      dx(y);
      xn=nn(x);
      if(xn>2){ // indexed amd/dmd
        if(v>(I)quote(64)){ // indexed-modified
          l=l-64; // 64 is added to verbs followed by a :
        }
        s=ati(rx(x), xn-3);
        lp=(0xff000000ffffffffull&lastp(x));
        // (+;.i.;`x;.;@) -> x:@[x;.i.;+;rhs] which is (+;.i.;`x;.;83 or 84)
        // lp+32 is @[amd..] or .[dmd..]
        if(lp==F_prj){
          lp=32+F_Cal; // x[i;]:.. no projection
        }
        x=cat1(cat1(ucat(l1(l),ldrop(-2,x)),20), ((K)(sap)<<32) | (lp+32));
        y=l2(s,quote(F_Asn)); // s:..
      } else {
        if(v==(I)quote(1) || v==(I)quote(65)){
          s=Fst(x); // (`x;.)
          if(loc_!=0ull && v==(I)quote(1)){
            loc_=Cat(loc_,rx(s));
          }
          x=l1(s);
          y=l1(quote(F_Asn));
        } else { // modified
          y=cat1(l2(unquote(l-32),Fst(rx(x))),quote(F_Asn));
        }
      }
      return dyadic(ucat(r,x),y);
    }
    return (K)0;
}

static K plist(K c);
static K plam(I s0);
static K es(void);
static K t(void){
    K r=next(),verb,n,ks,p,s;
    I a;
    T rt, tn;
    if(!r) return (K)0;
    if(tp(r)==0 && (I)(r)<127 && is((I)r,0x20)){ // )]};
      PP-=8;
      return (K)0;
    }
    verb=(K)0;
    if(r==(K)'('){
      r=plist(')');
      r=rlist(r&~1ull, 0);
    } else if(r==(K)'{'){
      r=plam(PS);
    } else if(r==(K)'['){
      r=es();
      if(next()!=(K)']') return trap(E_Parse);
      return r;
    } else if(tp(r)==T_st){
      r=l2(r, 20 | ((K)(PS)<<32)); // .`x (lookup)
    } else {
      rt=tp(r);
      if(rt==0){
        r=quote(r) | ((K)(PS)<<32);
        verb=1;
      } else if(rt==T_St && nn(r)==1){ r=Fst(r); }
      r=l1(r);
    }
    for(;;){
      n=next();
      if(!n) break;
      a=(I)n;
      tn=tp(n);
      ks=(K)(PS)<<32;
      if(tn==0 && a>=F_Ech && a<=F_Scn){ // +/
        r=cat1(r,n);
        verb=1;
      } else if(n=='['){
        verb=0;
        n=plist(']');
        p=(K)(n&1)?F_prj:32+F_Cal;
        n=(n&~1ull);
        s=pspec(r,n);
        if(s) return s;
        if(nn(n)==1){ r=cat1(ucat(Fst(n),r), (32+F_Atx)|ks); }
        else {
          n=rlist(n,2);
          r=cat1(Cat(n,r), p|ks);
        }
      } else {
        PP-=8;
        break;
      }
    }
    return r+verb;
}

static I svrb(K a, I b){
    K x=xK(a)[b];
    return (I)((I)(x)<32 && tp(x)==0) * (I)x;
}

static K idiom(K x){
    I l=nn(x)-2;
    I i=svrb(x,l),j=svrb(x,l+1);
    if(j==F_Mul && i==F_Min){ i=F_Fwh; } // *& -> Fwh
    else if(j==F_Mul && i==F_Max){ i=F_Las; } // *| -> Las
    else if(j==F_Unq && i==F_Srt){ i=F_Uqs; } // ?^ -> Uqs
    else { return x; }
    xJ(x)[l]=(J)i;
    return ndrop(-1,x);
}

static K e(K x){
    K r=0,xv,y,yv,ev,a;
    I xs;
    xv=(x&1); x=(x&~1ull);
    if(!x) return (K)0;
    xs=PS;
    y=t();
    yv=(y&1); y=(y&~1ull);
    if(!y) return x+xv;
    if(yv && !xv){
      r=e(t());
      ev=(r&1); r=(r&~1ull);
      a=pasn(x,y,r);
      if(a) return a;
      if(!r || ev==1){
        x=cat1(ucat(cat1(cat1(ucat(l1(0),x),Ki(2)),F_lst),y),F_prj);
        if(ev==1) return (cat1(ucat(r,x),F_com)+1);
        return x+1;
      }
      return dyadic(ucat(r,x),y);
    }
    r=e(rx(y)+yv);
    ev=(r&1); r=(r&~1ull);
    dx(y);
    if(!xv){
      return cat1(ucat(r,x),(32+F_Atx)|((K)(xs)<<32));
    } else if((r==y && (xv+yv)==2) || ev==1){
      return cat1(ucat(r,x),F_com) + 1;
    }
    return idiom(monadic(ucat(r,x)));
}

static K es(void){
    K r=mk(T_Lt,0),n,x;
    for(;;){
      n=next();
      if(!n) break;
      if(n==';') continue;
      PP-=8;
      x=(e(t()) &~1ull);
      if(!x) break;
      if(nn(r)) r=cat1(r,128); // drop
      r=Cat(r,x);
    }
    return r;
}

static K parse(K x){
    if(tp(x)!=T_Lt) return trap(E_Type);
    PP=(I)x;
    PE=PP+(8*nn(x));
    K r=es();
    if(PP!=PE) return trap(E_Parse);
    lfree(x);
    return r;
}

static void ws(void){
    I c=0;
    for(;PP<PE;PP++){ // skip over non-printing chars (c < 32 && c!= 10)
      c=xC(PP)[0];
      if(c=='\n' || c>' ') break;
    }
    for(;PP<PE;){ // skip over comments
      c=xC(PP)[0];
      if(c=='/' && xC(PP-1)[0]<=' '){ // a comment, " /"
        PP++;
        for(;PP<PE;PP++){
          c=xC(PP)[0];
          if(c=='\n') break;
        }
      }
      else return;
    }
}

static K tchr(void);static K tnms(void);static K tvrb(void);static K tpct(void);static K tvar(void);static K tsym(void);
static K tok(K x){ // string -> tokens/literals
    static void* tkf[6] = {(void*)tchr, (void*)tnms, (void*)tvrb, (void*)tpct, (void*)tvar, (void*)tsym};
    K s,r,y;
    s=cat1(src(), Kc('\n')); // append a \n to previous code
    PP=nn(s); // move Parser Pos to after end of old code
    s=Cat(s,x); // append new code after old code
    PP+=(I)s;
    PE=PP+nn(x); // new Parser End Pos
    r=mk(T_Lt,0);
    for(;;){ // look for valid tokens
      ws(); // skip over non-printing, ws and comments
      if(PP==PE) break;
      for(I i=0;i<6;i++){// tchr, tnms, tvrb, tpct, tvar, tsym
        y=((K(*)(void))tkf[i])(); // tchr&tnms gives literals, tvrb&tpct gives magic numbers, tvar gives a symbol, tsym gives symbol list
        if(y){ // take first hit
          y=y | (K)((J)(PP-(I)s)<<32);
          r=cat1(r,y); // add parsed value to output
          break;
        }
        if(i==5) return trap(E_Parse); // unknown token
      }
    }
    xK(POS_SRC)[0]=s;
    return r; // T_Lt of values parsed
}

static K slam(K r, I ar, I s0){
    I rp=(I)r;
    xI(r)[-3]=ar;
    return ((K)(rp) | ((K)(s0)<<32)) | ((K)(T_lf)<<59);
}

static K Unq(K x);
static K plam(I s0){
    K r=0,slo,l,c,s;
    I ar=-1,ln,cn,y;
    slo=loc_; loc_=0;
    if(next()=='['){
      l=(plist(']')&~1ull);
      ln=nn(l);
      // [a]->,(`a;.)  [a;b]->((`a;.);(`b;.))
      K v=mk(T_Lt,ln); K* vp=xK(v);
      for(I i=0;i<ln;i++) vp[i]=Fst(ati(rx(l),i));
      dx(l);
      loc_=uf(v);
      if(ln>0 && tp(loc_)!=T_St) return trap(E_Parse);
      ar=nn(loc_);
      if(!ar){
        dx(loc_);
        loc_=mk(T_St,0);
      }
    } else {
      PP-=8;
      loc_=mk(T_St,0);
    }
    c=es();
    if(next()!='}') return trap(E_Parse);
    cn=nn(c);
    if(ar<0){
      ar=0;
      K* cp=xK(c);
      for(I i=0; i<cn; i++){ // this parses lamda for implicit args
        r=cp[i];
        if(tp(r)==0 && (I)(r)==F_Val){
          r=cp[i-1];
          y=(I)r;
          if(tp(r)==T_st && y>0 && y<4) ar=maxi(ar,y); // if you use y you also get x and if you use z, you get all 3!
        }
      }
      loc_=Cat(ntake(ar,rx(xyz_)),loc_); // xyz_ are the default implicit args
    }
    s=atn(rx(src()), s0-1, (1+PS)-s0);
    r=l3(c,Unq(loc_),s);
    r=cat1(r, Ki(mod_));
    loc_=slo;
    return l1(slam(r,ar,s0));
}

static K plist(K c){
    K p=0,r,b,x;
    I n=0;
    r=mk(T_Lt,0);
    for(;;n++){
      b=next();
      if(!b || b==c) break;
      if(!n) PP-=8;
      if(n && b!=';') return trap(E_Parse);
      x=(e(t())&~1ull);
      if(!x) p=1;
      r=cat1(r,x);
    }
    return r+p;
}

static K rf(K x){
    I n=(I)x; dx(x);
    K r=mk(T_Ft,n); F* rp=xF(r);
    for(I i=0;i<n;i++){ rp[i]=(0.5 + rnd() / 4294967295.); }
    return r;
}

static K roll(K x){
    return (tp(x)==T_it && (I)x>0) ? rf(x) : trap(E_Type);
}

static void msrt(I x, I r, I a, I b, I p, I s, I f);
static K grade(K x, I f){
    K r,w,y;
    I n;
    T xt=tp(x);
    if(xt<16) return trap(E_Type);
    if(xt==T_Dt){
      r=x0(x);
      return Atx(r,grade(r1(x),f));
    }
    if(xt==T_Tt){
      w=x0(x1(x)); r=grade(w, f);
      dx(x);
      return r;
    }
    n=nn(x);
    if(n<2){
      dx(x);
      return seq(n);
    }
    if(xt==T_St){
      y=mk(T_Lt,0); I* kp=xI(x);
      for(I i=0;i<n;i++){ y=cat1(rx(y),cs((K)(kp[i]))); }
      dx(x); x=y; xt=T_Lt;
    }
    r=mk(T_It,n); w=mk(T_It,n);
    I *rp=xI(r),*wp=xI(w);
    for(I i=0;i<n;++i){ wp[i]=rp[i]=i; }
    msrt((I)w, (I)r, 0, n, (I)x, sz(xt), f+(I)xt-18);
    dx(w); dx(x);
    return r;
}

static K Asc(K x){ return grade(x,spcGdl); }
static K Dsc(K x){ return grade(x,spcGdu); }

static K Srt(K x){
    K r=0,i;
    I xt=tp(x);
    if(xt<16) return trap(E_Type);
    if(xt==T_Dt){
      r=x0(x); x=r1(x); i=rx(Asc(rx(x)));
      return key(atv(r,i),atv(x,i),T_Dt);
    }
    if(nn(x)<2) return x;
    return atv(x,Asc(rx(x)));
}

static I taoC(C* xp, C* yp, I n){
    for(I i=0; i<n; i++){
      if(xp[i]!=yp[i]) return (I)(xp[i]<yp[i]);
    }
    return 2;
}

static I taoI(I* xp, I* yp, I n){
    for(I i=0; i<n; i++){
      if(xp[i]!=yp[i]) return (I)(xp[i]<yp[i]);
    }
    return 2;
}

static I ltL(K x, K y);
static I taoL(K* xp, K* yp, I n){
    K x,y;
    for(I i=0; i<n; i++){
      x=xp[i]; y=yp[i];
      if(!match(x,y)) return ltL(x,y);
    }
    return 2;
}

static I taoF(F* xp, F* yp, I n){
    F x,y;
    for(I i=0; i<n; i++){
      x=xp[i]; y=yp[i];
      if(!eqf(x,y)) return ltf(x,y);
    }
    return 2;
}

static I ltL(K x, K y){
    K a,b;
    I r=0,xn,yn,n;
    T xt=tp(x);
    if(xt!=tp(y)){ return (I)(xt<tp(y)); }
    if(xt<16){ return (I)Les(rx(x),rx(y)); }
    if(xt>T_Lt){
      a=xK(x)[0]; b=xK(y)[0];
      if(!match(a,b)){ return ltL(a,b); }
      return ltL(xK(x)[1], xK(y)[1]);
    }
    xn=nn(x); yn=nn(y);
    n=mini(xn,yn);
    switch(sz(xt)>>2){
      case 0: r=taoC(xC(x),xC(y),n); break;
      case 1: r=taoI(xI(x),xI(y),n); break;
      case 2: r=(xt==T_Lt) ? taoL(xK(x),xK(y),n) : taoF(xF(x),xF(y),n);
        break;
    }
    return (r==2) ? (I)(xn<yn) : r;
}

static I guC(I xp, I yp){ return (I)(xC(xp)[0]<xC(yp)[0]); }
static I guI(I xp, I yp){ return (I)(xI(xp)[0]<xI(yp)[0]); }
static I guF(I xp, I yp){ return ltf(xF(xp)[0],xF(yp)[0]); }
static I guL(I xp, I yp){ return ltL(xK(xp)[0],xK(yp)[0]); }
static I gdC(I xp, I yp){ return (I)(xC(xp)[0]>xC(yp)[0]); }
static I gdI(I xp, I yp){ return (I)(xI(xp)[0]>xI(yp)[0]); }
static I gdF(I xp, I yp){ return guF(yp,xp); }
static I gdL(I xp, I yp){ return guL(yp,xp); }

static void mrge(I x, I r, I a, I b, I c, I p, I s, I f){
    static I(*Fg_[2*6])(I,I)={
      guC,guI,guI,guF,guL,guL,
      gdC,gdI,gdI,gdF,gdL,gdL};
    I q=0,i=a,j=c,k=a;
    I *xp=xI(x),*rp=xI(r);
    for(;k<b;k++){
      q=(i<c && j<b) ? Fg_[f](p+s*xp[i],p+s*xp[j]) : 0;
      if(i>=c || q) rp[k]=xp[j++];
      else rp[k]=xp[i++];
    }
}
static void msrt(I x, I r, I a, I b, I p, I s, I f){
    if((b-a)<2) return;
    I c=(a+b)>>1;
    msrt(r, x, a, c, p, s, f);
    msrt(r, x, c, b, p, s, f);
    mrge(x, r, a, b, c, p, s, f);
}

static K emb(I a, I b, K x){ return cat1(Cat(Kc(a),x),Kc(b)); }

static K si(I x){
    K r;
    if(!x) return Ku('0');
    else if(x==nai) return KCn("0N",2);
    else if(x<0) return ucat(Ku('-'),si(-x));
    r=mk(T_Ct,0);
    for(;x;x/=10) r=cat1(r,Kc('0'+x%10));
    return Rev(r);
}

static K sf(F x);
static K se(F x){
    F f=x;
    J e=0; I ei;
    if(frexp1(x)) f=frexp2(x,&e);
    x=0.3010299956639812*(F)e;
    ei=(I)F64floor(x);
    x=x-(F)ei;
    return ucat(cat1(sf(f*pow_(10.,x)),Kc('e')),si(ei));
}

static K sf(F x){
    UJ u; K r; J i;
    I c=0,n;
    if(x!=x) return KCn("0n",2);
    u=(UJ)I64reinterpret_f64(x);
    if(u==(UJ)I64reinterpret_f64(F64inf)) return KCn("0w",2);
    else if(u==(UJ)I64reinterpret_f64(-F64inf)) return KCn("-0w",3);
    if(x<0.) return ucat(Ku('-'),sf(-x));
    if(x>0. && (x>=1.e6 || x<=1e-06)){ return se(x); }
    r=mk(T_Ct,0);
    i=(J)x;
    if(i==0ll){ r=cat1(r,Kc('0')); }
    for(;i!=0; i/=10){ r=cat1(r,Kc((I)('0'+i%10))); }
    r=Rev(r);
    r=cat1(r,Kc('.'));
    x=x-F64floor(x);
    for(I j=0; j<6; j++){
      x=x*10.; r=cat1(r,Kc('0'+((I)(x)%10)));
    }
    n=nn(r); C* rp=xC(r);
    for(I j=0;j<n;j++) c=(rp[j]=='0')?c+1:0;
    return ndrop(-c,r);
}

static K Kst(K x);
static K Str(K x){
    K r=0,p,f,l,i;
    I xp,ip;
    T xt=tp(x);
    T ft;
    if(xt>16){
      I xn=nn(x);
      r=mk(T_Lt,xn); K* rp=xK(r);
      if(rr(x)) for(I i=0,a=xI(x)[0];i<xn;i++){ rp[i]=Str(Ki(a+i)); }
      else for(I i=0;i<xn;i++){ rp[i]=Str(ati(rx(x),i)); }
      dx(x);
      return uf(r);
    }
    xp=(I)x;
    if(xt!=0 && isfunc(xt)){
      switch(xt){
        case T_cf:
          rx(x);
          r=ucats(Rev(Str((K)(xp) | ((K)(T_Lt)<<59))));
          break;
        case T_df:
          r=Str(x0(x));
          p=x1(x);
          p=Str(21+p);
          r=ucat(r,p);
          break;
        case T_pf:
          f=x0(x); l=x1(x); i=x2(x);
          ft=tp(f); f=Str(f);
          if(nn(i)==1 && xI(i)[0]==1 && (!ft || ft==T_df)){ r=ucat(Kst(Fst(l)),f); }
          else { r=ucat(f,emb('[',']',ndrop(-1,ndrop(1,Kst(l))))); }
          dx(i);
          break;
        case T_lf: r=x2(x); break;
        default: r=x1(x); break; // native
      }
      dx(x);
      return r;
    } else {
      switch(xt){
        case 0:
          if(xp>(I)quote(F_nul)){ return Str(unquote((K)xp)); }
          ip=xp;
          switch(xp>>5){
            case 0:
              if(!xp) return mk(T_Ct,0); // 0..31 monadic
              break;
            case 1: ip=ip-32; break; // 32..63 dyadic
            case 2: ip=ip-64; break; // 64 tetradic
            case 3: ip=ip-96; break; // 96 dyadic indirect
          }
          if(ip>23 || ip==0){ return ucat(Ku('`'),si(xp)); }
          r=Ku((UJ)Verbs[ip-1]);
          break;
        case 1: r=0; break; // not reached
        case T_ct: r=Ku((K)xp); break;
        case T_it: r=si(xp); break;
        case T_st: r=cs(x); break;
        case T_ft: r=sf(xF(x)[0]); break;
      }
    }
    dx(x);
    return r;
}

static K fmtI(I x, K r, I n){
    for(I i=0; i<n; i++){
      if(i) r=ucat(r,Ku(' '));
      r=ucat(r,si(xI(x)[i]));
    } return r;
}

static K fmtF(I x, K r, I n){
    F* xp=xF(x);
    for(I i=0; i<n; i++){
      if(i) r=ucat(r,Ku(' '));
      r=ucat(r,sf(xp[i]));
    } return r;
}

static K fmtD(K x, K r){
    K k=x0(x),v=x1(x); I kn=nn(k);
    if(kn<2 || tp(k)>T_Lt) r=ucat(r,Ku('('));
    r=ucat(r,Kst(k)); // key list
    if(kn<2 || tp(k)>T_Lt) r=ucat(r,Ku(')'));
    r=ucat(r,Ku('!'));
    return ucat(r,Kst(v)); // value list
}

static K fmtL(I x, K r, I n){
    if(n!=1) r=ucat(r,Ku('('));
    for(I i=0; i<n; i++){
      if(i) r=ucat(r,Ku(';'));
      K v=Kst(xK(x)[i]);
      r=ucat(r, v);
    }
    if(n!=1) r=ucat(r, Ku(')'));
    return r;
}

static K fmtS(I x, K r, I n){
    I* xp=xI(x);
    for(I i=0; i<n; i++){
      r=ucat(r, Ku('`'));
      r=ucat(r, cs((K)xp[i]));
    } return r;
}

static K fmthx(I c, K r, I s){
    r=uspc(r,T_Ct,s?4:2);
    C*rp=xC(r)+nn(r)-1;
    if(s)rp[-3]='0',rp[-2]='x';
    if(c<0)c=256+c;
    if(c<16)rp[-1]='0';
    for(I x; c; c/=16){
      x=c%16;
      *(rp--)=(C)(x+(x>9?'W':'0'));
    } return r;
}

static K fmtc(I c, K r, I s){
    if(c>'~' || (c<' ' && c!='\n' && c!='\r' && c!='\t')){
      return fmthx(c,r,s);
    } else {
      I cc=c;
      if(s){ r=ucat(r, Ku('"')); }
      if     (c=='\n'){ r=ucat(r, Ku('\\')); cc='n' ; }
      else if(c=='\r'){ r=ucat(r, Ku('\\')); cc='r' ; }
      else if(c=='\t'){ r=ucat(r, Ku('\\')); cc='t' ; }
      else if(c=='"' ){ r=ucat(r, Ku('\\')); cc='"' ; }
      else if(c=='\\'){ r=ucat(r, Ku('\\')); cc='\\'; }
      r=cat1(r, (K)cc);
      return (s) ? ucat(r, Ku('"')) : r;
    }
}

static K fmtC(I x, K r, I n){
    I c,hx=0;
    for(I i=0;i<n;i++){
      c=xC(x)[i];
      if(c>'~'||(c<' ' && c!='\n' && c!='\r' && c!='\t')){
       hx=1; break;
      }
    }
    if(hx){
      r=ucat(r,KCn("0x",2));
      for(I i=0;i<n;i++) r=fmthx(xC(x)[i],r,0);
      return r;
    } else {
      r=ucat(r,Ku('"'));
      for(I i=0;i<n;i++) r=fmtc(xC(x)[i],r,0);
      return ucat(r,Ku('"'));
    }
}

static K fmtIr(I a, I n){
    K r=mk(T_Ct, 0);
    /*if(!a){
      r=ucat(r, Ku('!'));
      return ucat(r, si(n));
    }*/
    if(n==1) r=ucat(r,Ku(','));
    for(I i=0;i<n;i++){
      if(i) r=ucat(r,Ku(' '));
      r=ucat(r,si(a+i));
    } return r;
}

static K Kst(K x){
    T xt=tp(x);
    if(xt==T_it || xt==T_ft) return Str(rx(x));
    K r=mk(T_Ct, 0);
    I xn=(xt>6) ? nn(x) : 0;
    if(!xn){
      switch(xt){
        case T_Ft: return KCn("0#0.",4);
        case T_It: return KCn("!0",2);
        case T_St: return KCn("0#`",3);
        case T_ct: return fmtc((I)x,r,1);
        case T_st: r=ucat(r,Ku('`'));
          return ucat(r,cs(x));
      }
    }
    if(xn==1 && xt>16 && xt<=T_Lt) r=ucat(r, Ku(','));
    if(xt==T_It){ if(rr(x)){ dx(r); return fmtIr(xI(x)[0],xn); } return fmtI((I)x,r,xn); }
    if(xt==T_Ft) return fmtF((I)x,r,xn);
    if(xt==T_Tt){
      xt=T_Dt; r=ucat(r,Ku('+'));
    }
    if(xt==T_Dt) return fmtD(x,r);
    if(xt==T_Lt) return fmtL((I)x,r,xn);
    if(xt==T_St) return fmtS((I)x,r,xn);
    if(xt==T_Ct) return fmtC((I)x,r,xn);
    dx(r);
    return Str(rx(x));
}

static K Fmt(K f, K x){
    // TODO: implement
    dx(x);
    return f;
}

static K tvar(void){
    K r;
    I c=xC(PP)[0];
    if(!is(c,0x2)) return (K)0;
    PP++;
    r=Ku((K)c);
    for(;PP<PE;PP++){
      c=xC(PP)[0];
      if(!is(c,0x6)) break;
      r=cat1(r,((K)(c) | ((K)(T_ct)<<59)));
    }
    return sc(r);
}

static I hx(I c){ return is(c,0x4)?(c-'0'):(c-'W'); }
static K thex(void){
    I c;
    K r=mk(T_Ct,0);
    for(;PP<PE-1;PP+=2){
      c=xC(PP)[0];
      if(!is(c,0x80)) break; // 0123456789abcdef
      r=cat1(r,Kc(((hx(c)<<4)+hx(xC(1+PP)[0]))));
    }
    if(nn(r)==1){ return Fst(r); }
    return r;
}

static I cq(I c){
    if(c=='t'){ return '\t'; }
    if(c=='n'){ return '\n'; }
    if(c=='r'){ return '\r'; }
    return c;
}

static K tchr(void){ // parse T_Ct or T_ct
    K r;
    if(xC(PP)[0]=='0' && PP<PE && xC(1+PP)[0]=='x'){ // parse hex char
      PP+=2;
      return thex();
    }
    if(xC(PP)[0]!='"'){ return (K)0; } // must start with "
    PP++;
    r=mk(T_Ct,0); // make string
    for(I q=0, c;;){
      if(PP==PE) return trap(E_Parse);
      c=xC(PP++)[0];
      if(c=='"' && !q){ break; } // stop at unquoted "
      if(c=='\\' && !q){ q=1; continue; }
      if(q){ c=cq(c); q=0; } // unquote
      r=cat1(r,Kc(c)); // append char to string
    }
    if(nn(r)==1) return Fst(r); // if length 1, return char
    return r;
}

static K tvsm(void){
    K r;
    I c=xC(PP)[0];
    if(!is(c,0x6)) return (K)0;
    PP++;
    r=Ku((K)c);
    for(;PP<PE;PP++){
      c=xC(PP)[0];
      if(!is(c,0x6)) break;
      r=cat1(r,((K)(c) | ((K)(T_ct)<<59)));
    }
    return sc(r);
}

static K tsym(void){
    K r=0,s;
    for(;xC(PP)[0]=='`';){
      ++PP;
      if(!r) r=mk(T_St,0);
      s=0;
      if(PP<PE){
        s=tchr();
        if(s==0) s=tvsm();
        else if(tp(s)==T_ct) s=sc(Enl(s));
        else s=sc(s);
      }
      if(!s) s=(K)(T_st)<<59;
      r=cat1(r,s);
      if(PP==PE) break;
    }
    return r;
}

static J pu(void){
    J r=0;
    for(I c; PP<PE; PP++){
      c=xC(PP)[0];
      if(!is(c,0x4)) break;
      r=10*r+(J)(c-'0');
    }
    return r;
}

static F pexp(F f){
    I c;
    J e=1;
    if(++PP<PE){
      c=xC(PP)[0];
      if(c=='-' || c=='+'){
        ++PP; if(c=='-') e=-1;
      }
    }
    e=e*pu();
    return f * pow_(10.,(F)e);
}

static K ppi(F f){
    ++PP; return Kf(pi*f);
}

static K pflt(J i){
    I c=0;
    F d=1.,f=(F)i;
    PP++;
    for(; PP<PE; PP++){
      c=xC(PP)[0];
      if(!is(c,0x4)){ break; }
      d=d/10.;
      f+=d*(F)(c-'0');
    }
    if(PP<PE){
      c=xC(PP)[0];
      if(c=='e' || c=='E'){ f=pexp(f); }
    }
    if(PP<PE){
      c=xC(PP)[0];
      if(c=='p'){ return ppi(f); }
    }
    return Kf(f);
}

static K tunm(void){
    K q;
    I p=PP,c;
    J r=pu();
    if(!r && p==PP){
      return (xC(p)[0]=='.' && is(xC(1+p)[0],0x4)) ? pflt(r) : (K)0;
    }
    if(PP<PE){
      c=xC(PP)[0];
      if(c=='.'){ return pflt(r); }
      if(c=='p'){ return ppi((F)r); }
      if(c=='e' || c=='E'){ return Kf(pexp((F)r)); }
      if(!r){
        if(c=='N'){
          ++PP; return missing(T_it);
        }
        if(c=='n' || c=='w'){
          q=Kf(0.); xJ(q)[0]=(J)0x7ff8000000000001ll;
          if(c=='w'){ xF(q)[0]=F64inf; }
          ++PP; return q;
        }
      }
    }
    return Ki((I)r);
}
static K tnum(void){
    K r;
    I c=xC(PP)[0];
    if((c=='-' || c=='.') && is(xC(PP-1)[0],0x40)){ return (K)0; }
    if(c=='-' && PP++<(1+PE)){
      r=tunm();
      if(!r){
        --PP; return (K)0;
      }
      switch(tp(r)){
        case T_ct: return Kc(-(C)r);
        case T_ft: return Kf(-xF(r)[0]);
        default: return Ki(-(I)r);
      }
    }
    return tunm();
}
static K tnms(void){
    K x,r;
    r=tnum();
    for(;PP<PE-1 && xC(PP)[0]==' ';){
      ++PP; x=tnum();
      if(!x) break;
      r=ncat(r,x);
    }
    return r;
}
static K prs(T t, K y){
    K r=0;
    I yp,yn,p,e;
    yp=(I)y; yn=nn(y);
    p=PP; e=PE;
    PP=yp; PE=yp+yn;
    if((t&15)==2) return (t==T_Ct)?y:Fst(y);
    if(t==T_st){
      r=Fst(tsym());
    } else if(t>2 && t<=6){
      r=tnum();
      if(tp(r)<t && r!=0) r=uptype(r,t);
    }
    if(t>T_Ct && t<T_Lt){
      if(PP==PE){
        r=mk(t,0);
      } else if(t==T_St){
        r=tsym();
      } else {
        r=tnms();
        if((tp(r)&15)<(t&15) && r!=0){
          r=uptype(r, t&15);
        }
      }
      if(tp(r)==t-16) r=Enl(r);
    }
    if(tp(r)!=t || PP<PE){
      dx(r); r=0;
    }
    PP=p; PE=e;
    dx(y);
    return r;
}
static T ts(K x){ // char -> T
    I c=(I)Fst(cs(x));
    for(I i=0;i<26;i++){ if(c==Types[i]) return (T)i; }
    trap(E_Value);
    return (T)0;
}

static K tvrb(void){
    I o,c=xC(PP)[0];
    if(!is(c, 0x1)) return (K)0;
    ++PP; o=1;
    if(PP<PE && xC(PP)[0]==':'){
      ++PP; o=65;
      if(is(c, 0x8)) return trap(E_Parse);
    }
    for(I i=0;i<23;i++){ if(Verbs[i]==c) return (K)(o+i); }
    return trap(E_Parse);
}

static K tpct(void){
    I c=xC(PP)[0];
    if(is(c,0x30)){ // ([{}]);
      ++PP; return (K)c;
    }
    if(c=='\n'){
      ++PP; return (K)';';
    }
    return (K)0;
}

static K Flp(K x){
    K m,t;
    T xt=tp(x);
    I n,xp;
    switch(xt){
      case T_Lt: { // Transpose, with replacements
        n=nn(x); xp=(I)x;
        m=Ki(maxcount(xp,n));
        K l=mk(T_Lt,0),e,a;
        for(I c=0; c<(I)m; c++){
          e=mk(T_Lt,0);
          for(I r=0; r<n; r++){
            a = ati(rx(x), r);
            if(tp(a)<16) e=Cat(e,l1(a));
            else {
              I an = nn(a);
              if(!an){ e=Cat(e,l1(a)); }
              else e=Cat(e, ati(rx(a), c%an));
            }
          }
          l=Cat(l,l1(e));
        }
        dx(x);
        return l;
      }
      case T_Dt: return td(x);
      case T_Tt:
        t=x0(x);
        return key(t,r1(x),T_Dt);
      default: return x;
    }
}

static K odo(K x){
    K y, r;
    I n,w=1,s=0,c,rps;
    n=maxi(nn(x),0);
    r=mk(T_Lt,0);
    if(!n) return r;
    I* xp=xI(x);
    for(I i=0; i<n; i++){
      c=xp[i];
      s+=c; w*=c;
    }
    rps=s;
    I* yp;
    for(I i=0; i<n; i++){
      c=xp[i];
      y=mk(T_It, w); yp=xI(y);
      rps=rps-c;
      for(I j=0,k=0,l=0; k<w; k++){
        ++l; yp[i]=j;
        if(!rps || l==rps){
          l=0; if(++j==c){ j=0; }
        }
      }
      r=cat1(r,y);
    }
    return r;
}

static K Til(K x){
    T xt=tp(x);
    if(xt>T_Lt) return r0(x);
    if(xt==T_it) return seq((I)x);
    if(xt==T_It) return odo(x);
    return trap(E_Type);
}

static K Unq(K x){
    I xn;
    K r,xi;
    T xt=tp(x);
    if(xt<16){ return trap(E_Type); }
    if(xt>=T_Lt){
      if(xt==T_Dt){ return trap(E_Type); }
      if(xt==T_Tt){
        r=x0(x); x=r1(x);
        return key(r,Flp(Unq(Flp(x))),xt);
      }
      return uqf(x);
    }
    xn=nn(x);
    r=mk(xt,0);
    for(I i=0;i<xn;i++){
      xi=ati(rx(x),i);
      if(!(I)In(rx(xi),rx(r))){ r=cat1(r,xi); }
      else { dx(xi); }
    }
    dx(x);
    return r;
}

static K ntake(I n, K y){
    I yp,s,yn;
    F f;
    K r=0;
    T t=tp(y);
    if(n==nai){ n=t<16 ? 1 : nn(y); }
    if(n<0){
      if(tp(y)<16){ return ntake(-n,y); }
      n+=nn(y);
      return (n<0) ? ucat(ntake(-n,missing(t-16)),y) : ndrop(n,y);
    }
    yp=(I)y;
    if(t<5){
      t+=16; s=sz(t);
      r=mk(t,n);
      if(s==1){
        Memoryfill((I)r,yp,n);
      } else { I* rp=xI(r);
        for(I i=0;i<n;i++){ rp[i]=(I)y; }
      }
      return r;
    } else if(t==T_ft){
      r=mk(T_Ft,n); F* rp=xF(r);
      f=xF(y)[0];
      for(I i=0;i<n;i++){ rp[i]=f; }
      dx(y);
      return r;
    } else if(t<16){
      r=mk(T_Lt,n); K* rp=xK(r);
      for(I i=0;i<n;i++){ rp[i]=rx(y); }
      dx(y);
      return r;
    }
    yn=nn(y);
    if(rr(y)){
      if(n>yn){
        I a=xI(y)[0]; dx(y); K r=mk(T_It, n); I* rp=xI(r);
        for(I i=0,j=0; i<n; i++){
          rp[i]=a+j; if(++j>=yn) j=0;
        } return r;
      }
      y=use(y);
      xI(y)[-3]=n;
      return y;
    }
    // TODO: inline?
    r=seq(n);
    if(n>yn && yn>0){ r=idiv(r,Ki(yn),1); }
    return atv(y,r);
}

static K Wer(K x){
    I xn,n;
    K r=0;
    T t=tp(x);
    if(t<16){
      x=Enl(x); t=tp(x);
    } else if(t==T_Dt){
      r=x0(x); return Atx(r,Wer(r1(x)));
    }
    xn=nn(x);
    if(t==T_It){
      n=sumi((I)x,xn);
      r=mk(T_It,n);
      I*rp=xI(r),*xp=xI(x);
      for(I i=0,j=0,k=0; i<xn; i++){
        j+=xp[i];
        for(; k<j; k++) rp[k]=i;
      }
    } else {
      r=(!xn) ? mk(T_It,0) : trap(E_Type);
    }
    dx(x);
    return r;
}

static K taki(I x, K y){
    K r;
    T yt=tp(y);
    if(yt==T_Dt){
      r=x0(y); y=r1(y);
      r=taki(x,r); y=taki(x,y);
      return key(r,y,T_Dt);
    } else if(yt==T_Tt){
      I yn;
      r=x0(y);
      y=dvals(y); yn=nn(y);
      K v=mk(T_Lt,yn); K* vp=xK(v);
      for(I i=0;i<yn;i++){ vp[i]=taki(x,ati(rx(y),i)); }
      dx(y);
      return key(r,uf(v),T_Tt);
    }
    return ntake(x,y);
}

static K ndrop(I n, K y){
    I yn,rn,s,yp;
    K r=0;
    T yt=tp(y);
    if(yt<16 || yt>T_Lt) return trap(E_Type);
    yn=nn(y);
    if(n<0) return ntake(maxi(0,yn+n),y);
    rn=yn-n;
    if(rn<0){
      dx(y); return mk(yt,0);
    }
    s=sz(yt);
    yp=(I)y;
    if(rr(y)){
      xI(y)[0]+=n; xI(y)[-3]=rn;
      return y;
    }
    if(xI(y)[-1]==1 && bucket(s*yn)==bucket(s*rn) && yt<T_Lt){
      r=rx(y);
      xI(y)[-3]=rn;
      memmove(M_+(I)r, M_+(yp+s*n), (size_t)(s*rn));
    } else {
      r=mk(yt, rn);
      Memorycopy((I)r, yp+s*n, s*rn);
    }
    if(yt==T_Lt){
      rl(r); r=uf(r);
    }
    dx(y);
    return r;
}

static K drp_rec(K x, K y){
    K r=0;
    I yn;
    T yt,t;
    yt=tp(y);
    if(yt<16) return Drp(x,y);
    if(yt>T_Lt){
      t=dtypes(x,y);
      r=dkeys(x,y);
      return key(r,drp_rec(dvals(x),dvals(y)),t);
    }
    yn=nn(y);
    r=mk(T_Lt,yn); K* rp=xK(r);
    for(I i=0;i<yn;i++){ rp[i]=Drp(rx(x),ati(rx(y),i)); }
    dx(x); dx(y);
    return uf(r);
}
static K Drp(K x, K y){
    K r;
    T xt=tp(x),yt=tp(y);
    if(yt>T_Lt){
      if(yt==T_Dt || (yt==T_Tt && (xt&15)==T_st)){
        r=x0(y); y=r1(y);
        if(xt<16) x=Enl(x);
        x=rx(Wer(notI(In(rx(r),x))));
        return key(Atx(r,x),Atx(y,x),yt);
      } else {
        T t=dtypes(x,y);
        K r=dkeys(x,y);
        K xv=dvals(x);
        K yv=dvals(y);
        return key(r,drp_rec(xv,yv),t);
      }
    }
    if(xt==T_it) return ndrop((I)x,y);
    if(xt>16 && xt==yt) return atv(y,Wer(notI(In(rx(y),x))));
    if(yt==T_it) return atv(x,Wer(notI(eqle(y,seq(nn(x)),yt))));
    return trap(E_Type);
}

static K atn(K x, I a, I n){
    T xt=tp(x);
    if(xt==T_It && rr(x)){
      x=use(x);
      xI(x)[-3]=n;
      xI(x)[0]=a-xI(x)[0];
      return x;
    }
    K r=mk(xt,n);
    I i;
    switch(xt){
      case T_Ct:{ C*rp=xC(r),*xp=xC(x)+a; for(i=0; i<n; i++) rp[i]=xp[i]; }
        break;
      case T_St:
      case T_It:{ I*rp=xI(r),*xp=xI(x)+a; for(i=0; i<n; i++) rp[i]=xp[i]; }
        break;
      case T_Ft:{ F*rp=xF(r),*xp=xF(x)+a; for(i=0; i<n; i++) rp[i]=xp[i]; }
        break;
      case T_Lt:{ K*rp=xK(r),*xp=xK(x)+a; for(i=0; i<n; i++) rp[i]=xp[i]; }
        break;
      case T_Tt: {
        K v=x1(x); I vn=nn(v);
        r=mk(T_Lt,n); K *vp=xK(v),*rp=xK(r);
        for(i=0;i<vn;i++){ rp[i]=atn(rx(vp[i]), a, n); }
        r=key(x0(x),r,T_Tt);
      }
        break;
      default:
        return trap(E_Type);
    }
    dx(x);
    return r;
}

static K rcut(K x, K a, K b){
    K r;
    I n,o,on;
    n=nn(a);
    r=mk(T_Lt,n);
    for(I i=0; i<n; i++){
      o=xI(a)[i]; on=xI(b)[i]-o;
      if(on<0) return trap(E_Value);
      K t=atn(rx(x),o,on);
      xK(r)[i]=t;
    }
    dx(a); dx(b); dx(x);
    return r;
}

static K cuts(K x, K y){
    return rcut(y,x,cat1(ndrop(1,rx(x)),Ki(nn(y))));
}

static K split(K x, K y){
    I xn=1,n;
    T xt=tp(x),yt=tp(y);
    if(yt==(xt+16)){
      x=Wer(eqle(x,rx(y),xt));
    } else if(xt==yt && xt==T_Ct){
      xn=nn(x);
      x=Find(x,rx(y));
    } else {
      return trap(E_Type);
    }
    x=rx(x); n=nn(x);
    K z=mk(T_It,n+1);
    I *xp=xI(x),*zp=xI(z);
    zp[0]=0;
    for(I i=0;i<n;i++) zp[1+i]=xn+xp[i];
    return rcut(y,z,cat1(x,Ki(nn(y))));
}

static K splitn(K x){
    if(tp(x)==T_ct) return Enl(x); // TODO
    K r=mk(T_Lt, 0);
    I i=0,j=0,c=0;
    for(; j<nn(x); j++){
      if(xC(x)[j]=='\r'){ c=1; continue; }
      else if(xC(x)[j]=='\n'){
        r=cat1(r, atn(rx(x), i, j-i-c));
        i=j+1;
      }
      c=0;
    }
    if(j>i) r=cat1(r, atn(rx(x), i, j-i));
    dx(x);
    return r;
}

static K splits(K x){
    if(tp(x)==T_ct) return Enl(x); // TODO
    K r=mk(T_Lt, 0);
    I i=0,j=0,w=1;
    for(; j<nn(x); j++){
      switch(xC(x)[j]){
        case ' ':case '\t':case '\r':case '\n':
          if(w){ ++i; break; }
          r=cat1(r, atn(rx(x), i, j-i));
          i=j+1; w=1;
          break;
        default: w=0; break;
      }
    }
    if(j>i) r=cat1(r, atn(rx(x), i, j-i));
    dx(x);
    return r;
}

static K join(K x, K y){
    K r,v;
    I yp,yn;
    T xt=tp(x),yt;
    if(xt<16){
      x=Enl(x); xt=tp(x);
    }
    yt=tp(y);
    if(yt!=T_Lt) return trap(E_Type);
    yp=(I)y; yn=nn(y);
    r=mk(xt,0);
    for(I i=0; i<yn; i++, yp+=8){
      v=x0((K)yp);
      if(tp(v)!=xt) return trap(E_Type);
      if(i>0){ r=ucat(r,rx(x)); }
      r=ucat(r,v);
    }
    dx(x); dx(y);
    return r;
}

static I ibin(K x, K y, T t){
    I h=0,n,yp,j;
    F f;
    n=nn(x); j=n-1;
    yp=(I)y;
    switch(sz(t)>>2){
      case 0:
        for(I k=0;;){ if(k>j){ return k-1; }
          h=(k+j)>>1;
          if(xC(x)[h]>yp){ j=h-1; }
          else { k=h+1; }
        }
        break;
      case 1:
        for(I k=0;;){ if(k>j){ return k-1; }
          h=(k+j)>>1;
          if(xI(x)[h]>yp){ j=h-1; }
          else { k=h+1; }
        }
        break;
      default:
        f=xF(y)[0];
        for(I k=0;;){ if(k>j){ return (k-1); }
          h=(k+j)>>1;
          if(xF(x)[h]>f){ j=h-1; }
          else { k=h+1; }
        }
        break;
    }
    return 0;
}

static K win(I n, K x){
    K r=mk(T_Lt,0); I m=(1+nn(x))-n;
    for(I i=0; i<m; i++){ r=ucat(r,l1(atn(rx(x),i, n))); }
    dx(x);
    return r;
}

static C lc(C x){ return (x>='A' && x<='Z') ? x+32 : x; }
static K lower(K x){
    I i=0,n;
    x=use(x); n=nn(x);
    for(C*xp=xC(x); i<n; i++){ xp[i]=lc(xp[i]); }
    return x;
}

static K Rev(K x){
    K r=0;
    I xn;
    T t=tp(x);
    if(t<16) return x;
    if(t==T_Dt){
      r=x0(x);
      return key(Rev(r),Rev(r1(x)),T_Dt);
    }
    xn=nn(x);
    if(xn<2) return x;
    r=mk(T_It,xn); I* rp=xI(r);
    for(I i=0,j=xn-1; i<xn; i++,j--){
      rp[i]=j;
    }
    if(t==T_Tt){
      K y=x0(x);
      x=r1(x);
      {
        K a=l2(x,r),y,v;
        I an;
        y=x1(a); a=r0(a);
        an=nn(a);
        v=mk(T_Lt,an); K* vp=xK(v);
        for(I i=0; i<an; i++){
          vp[i]=Atx(ati(rx(a),i),rx(y));
        }
        dx(a); dx(y);
        r=uf(v);
      }
      return key(y,r,T_Tt);
    }
    return atv(x,r);
}

static I fwh(I x, I n){
    I* xp=xI(x);
    for(I i=0;i<n;i++) if(xp[i]) return i;
    return nai;
}

static K Typ(K x){ // T -> char
    T t=tp(x); dx(x);
    return (t>25) ? sc(Enl(Kc(0x0))) : sc(Enl(Kc(Types[(I)t])));
}

static K eval(K x){
    IP=0;
    I xn,asn=0;
    if(nn(x)<0) trap(E_Length);
    x=parse(tok(x));
    xn=nn(x);
    if(xn<0) trap(E_Length);
    if(xn>2 && xK(x)[xn-1]==F_Asn+32){ asn=1; }
    x=exec(x);
    if(asn){
      dx(x); return (K)0;
    }
    return x;
}

static frame_t* rec(I op){
    if(frame==frame_end){ trap(E_Limit); return NULL; }
    ++frame;
    frame->op=op; return frame;
}
#define REC(s,o) frame_t*s;if(!(s=rec(o)))return
static void Asn_(K x, K y){ frame->a=Asn(x,y); }
static void Val_(K x){ frame->a=Val(x); }
static void Max_(K x, K y){ frame->a=Max(x,y); }
static void Min_(K x, K y){ frame->a=Min(x,y); }
static void Add_(K x, K y){ frame->a=Add(x,y); }
static void Neg_(K x){ frame->a=Neg(x); }
static void Sub_(K x, K y){ frame->a=Sub(x,y); }
static void Mul_(K x, K y){ frame->a=Mul(x,y); }
static void Mod_(K x, K y){ frame->a=Mod(x,y); }
static void Div_(K x, K y){ frame->a=Div(x,y); }
static void Eql_(K x, K y){ frame->a=Eql(x,y); }
static void Mor_(K x, K y){ frame->a=Mor(x,y); }
static void Sin_(K x){ frame->a=nfmath(F_Sin,x,0); }
static void Cos_(K x){ frame->a=nfmath(F_Cos,x,0); }
static void Exp_(K x){ frame->a=nfmath(F_Exp,x,0); }
static void Log_(K x){ frame->a=nfmath(F_Log,x,0); }
static void Fst_(K x){ frame->a=Fst(x); }
static void Cnt_(K x){ frame->a=Cnt(x); }
static void Not_(K x){ ((tp(x)&15)==T_st)?Eql_(Ks(0),x):Eql_(Ki(0),x); }
static void Enc_(K x, K y){ frame->a=Enc(x,y); }
static void Dec_(K x, K y){ frame->a=Dec(x,y); }
static void com_(K x, K y){ frame->a=(K)((I)l2(y,x)) | ((K)(T_cf)<<59); }
static void Enl_(K x){ frame->a=Enl(x); }
static void Cat_(K x, K y){ frame->a=Cat(x,y); }
static void lst_(K n){ frame->a=lst(n); }
static void nul_(K x){ push(x); frame->a=0; }
static void In_(K x, K y){ frame->a=In(x,y); }
static void Dsc_(K x){ frame->a=Dsc(x); }
static void Srt_(K x){ frame->a=Srt(x); }
static void Str_(K x){ frame->a=Str(x); }
static void Idy_(K x){ frame->a=x; }
static void Flp_(K x){ frame->a=Flp(x); }
static void Til_(K x){
    if((tp(x)&15)==T_ct) frame->a=splits(x);
    else frame->a=Til(x);
}
static void Wer_(K x){ frame->a=Wer(x); }
static void Rev_(K x){
  T xt=tp(x);
  if(xt>0&&xt<16){
    if(xt<T_ft) frame->a=(K)((I)x<0?-(I)x:(I)x)|(K)xt<<59;
    else {
      K y=Kf(F64abs(xF(x)[0]));
      frame->a=y; dx(x);
    }
    return;
  }
  frame->a=Rev(x);
}
static void Typ_(K x){ frame->a=Typ(x); }
static void Dex_(K x, K y){ dx(x);frame->a=y; }
static void Cal_(K x, K y){
    T xt=tp(x);
    y=explode(y);
    if(isfunc(xt)) cal_(x,y);
    else atdepth_(x,y);
}
static void Les_(K x, K y){
    if(tp(x)==T_st && tp(y)==T_Ct){
#ifdef SPESH_IO_VERBS
      frame->a=writeio((I)x?cs(x):Kc(0),y);
#else //!SPESH_IO_VERBS
      trap(E_Io);
#endif //SPESH_IO_VERBS
      return;
    }
    frame->a=Les(x,y);
}
static void Pow_(K y, K x){
    if((tp(x)&15)==T_it && tp(y)==T_it && (I)(y)>=0){
      frame->a=ipow(x,(I)y); return;
    }
    frame->a=nfmath(F_Pow+32,x,y);
}
static void Lgn_(K x, K y){
    F xf=fk(x);
    if(F64eq(xf,10.)){ xf=0.4342944819032518; }
    else { xf=(F64eq(xf,2.)) ? 1.4426950408889634 : 1./log_(xf); }
    REC(s,O_MUL); s->x=Kf(xf);
    Log_(y);
}
static void Las_(K x){
    T t=tp(x);
    if(t<16){ frame->a=x; return; }
    if(t==T_Dt) x=Val(x);
    I n=nn(x);
    frame->a=(!n) ? Fst(x) : ati(x,(n-1));
}
static void fix_(K f, K x, K l){
    REC(s,O_FIX);
    s->b=f; s->x=x; s->y=rx(x);
    s->i=(I)l; s->j=(l)?tp(l):0;
    Atx_(rx(f),rx(x));
}
static void Ecr_(K f, K x){
    K r=0,y;
    I yn;
    T yt,t;
    y=x1(x); x=r0(x);
    yt=tp(y);
    if(yt<16){ cal_(f,l2(x,y)); return; }
    if(yt>T_Lt){
      t=dtypes(x,y);
      r=dkeys(x,y);
      REC(s,O_KEY);
      s->b=r; s->p=t;
      Ecr_(f,l2(dvals(x),dvals(y)));
      return;
    }
    yn=nn(y);
    r=mk(T_Lt,yn);
    REC(s,O_ECR);
    s->b=f; s->i=(I)r;
    s->x=x; s->y=y;
    s->p=0; s->e=yn;
}
static void ec2_(K f, K x, K y){
    K r=0;
    I n;
    T t=dtypes(x,y);
    if(t>T_Lt){
      r=dkeys(x,y);
      REC(s,O_KEY);
      s->b=r; s->p=t;
      ec2_(f,dvals(x),dvals(y));
      return;
    }
    n=conform(x,y);
    switch(n){
      case 0:
        Cal_(f,l2(x,y));
        return;
      case 1: n=nn(y); break;
      default: n=nn(x); break;
    }
    r=mk(T_Lt,n);
    REC(s,O_EC2);
    s->b=f; s->i=(I)r;
    s->x=x; s->y=y;
    s->p=0; s->e=n;
}
static void Ech_(K f, K x);
static void ecn_(K f, K x){
    K r;
    if(nn(x)==2){
      r=x0(x); x=r1(x);
      if(r==0){ Ech_(f,l1(x)); return; }
      if(tp(f)==0 && (I)f==F_Cat && tp(r)==T_Tt && tp(x)==T_Tt){
        if(nn(r)!=nn(x)){ trap(E_Length); }
        f=Cat(x0(r),x0(x));
        frame->a=key(f,Cat(r1(r),r1(x)),T_Tt);
        return;
      }
      ec2_(f,r,x);
      return;
    }
    Ech_(F_Cal,l2(f,Flp(x)));
}
static void rdn_(K f, K x, K l){
    K r=Fst(rx(x));
    x=Flp(ndrop(1,x));
    I n=nn(x);
    REC(s,O_RDN);
    s->x=x; s->a=r;
    s->b=f; s->y=l;
    s->p=0; s->e=n;
}
static void Rdc_(K f, K x){
    K r=0;
    I a,xn,fp;
    T t,xt;
    t=tp(f);
    if(!isfunc(t)){
      if(nn(x)==2) trap(E_Nyi); // TODO: implement lin?
      x=Fst(x);
      if((t&15)==T_ct) frame->a=join(f,x);
      else Dec_(f,x);
      return;
    }
    a=arity(f);
    if(a!=2){ (a>2)?rdn_(f,x,0):fix_(f,Fst(x),0); return; }
    if(nn(x)==2){ Ecr_(f,x); return; }
    x=Fst(x); xt=tp(x);
    if(xt==T_Dt){
      x=Val(x); xt=tp(x);
    }
    if(xt<16){
      dx(f);
      frame->a=x;
      return;
    }
    xn=nn(x);
    if(!t){
      void* fspc[]={(void*)sum,(void*)rd0,(void*)prd,(void*)rd0,(void*)min,(void*)max};
      fp=(I)f;
      if(fp>=F_Add && fp<=F_Max){
        if(xt==T_Tt){
          Ech_(df_rdc(f),l1(Flp(x)));
          return;
        }
        r=((K(*)(K,I,I))fspc[fp-F_Add])(x,xt,xn);
        if(r!=0){
          dx(x);
          frame->a=r; return;
        }
      }
      if(fp==F_Cat){
        if(xt<T_Lt){ frame->a=x; return; }
        r=ucats(x);
        if(r!=0){ frame->a=r; return; }
      }
    }
    if(!xn){ frame->a=ov0(f,x); return; }
    REC(s,O_RDC);
    s->a=ati(rx(x),0);
    s->b=f; s->x=x;
    s->p=1; s->e=xn;
}
static void Ecl_(K f, K x){
    K y,r;
    I xn;
    y=x1(x); x=r0(x);
    if(tp(x)<16){ cal_(f,l2(x,y)); return; }
    xn=nn(x);
    r=mk(T_Lt,xn);
    REC(s,O_ECL);
    s->b=f; s->i=(I)r;
    s->x=x; s->y=y;
    s->p=0; s->e=xn;
}
static void Scn_(K f, K x){
    K r=0,z;
    I a,xn,fp;
    T t=tp(f),xt;
    if(!isfunc(t)){
      if(nn(x)!=1) trap(E_Rank);
      x=Fst(x);
      if((t&15)==T_ct){
        frame->a=split(f,x);
      } else Enc_(f,x);
      return;
    }
    a=arity(f);
    if(a!=2){
      if(a>2){ rdn_(f,x,mk(T_Lt,0)); return; }
      x=rx(Fst(x));
      fix_(f,x,Enl(x));
      return;
    }
    if(nn(x)==2){ Ecl_(f,x); return; }
    x=Fst(x);
    xt=tp(x);
    if(xt<16){
      dx(f);
      frame->a=x;
      return;
    }
    xn=nn(x);
    if(!xn){
      dx(f);
      frame->a=x;
      return;
    }
    if(xt==T_Dt){
      r=x0(x);
      REC(s,O_KEY);
      s->p=T_Dt; s->b=r;
      Scn_(f,l1(r1(x)));
      return;
    }
    if(!tp(f)){
      void* fspc[3]={(void*)sums,(void*)rd0,(void*)prds};
      fp=(I)f;
      if(fp==F_Add || fp==F_Mul){
        if(xt==T_Tt){
          REC(s,O_FLP);
          Ech_(df_scn(f),l1(Flp(x)));
          return;
        }
        r=((K(*)(I,I,I))fspc[fp-F_Add])((I)(x),xt,xn);
        if(r){
          dx(x);
          frame->a=r;
          return;
        }
      }
    }
    r=mk(T_Lt,xn);
    z=ati(rx(x),0);
    xK(r)[0]=rx(z);
    REC(s,O_SCN);
    s->a=z; s->b=f; s->x=x; s->y=r;
    s->p=1; s->e=xn;
}
static void prj_(K f, K x){
    K r=0,a,y;
    I xn,ar,an;
    if(!isfunc(tp(f))){ atdepth_(f,x); return; }
    xn=nn(x); K* xp=xK(x);
    a=mk(T_It,0);
    for(I i=0; i<xn; i++){ if(xp[i]==0){ a=cat1(a,Ki(i)); } }
    ar=arity(f);
    for(I i=xn; i<ar; i++){
      a=cat1(a,Ki(i));
      x=cat1(x,0);
    }
    an=nn(a);
    if(tp(f)==T_pf){
      r=x1(f); y=x2(f); f=r0(f);
      x=stv(r,rx(y),x);
      a=Drp(a,y);
    }
    r=l3(f,x,a);
    xI(r)[-3]=an;
    frame->a=(K)((I)r) | ((K)(T_pf)<<59);
}
static void Dmd_(K x, K i, K v, K y);
static void Amd_(K x, K i, K v, K y){
    K r;
    I n;
    T xt=tp(x),it;
    if(tp(x)==T_st){
      REC(s,O_ASN); s->x=x;
      Amd_(Val(x),i,v,y);
      return;
    }
    if(xt<16){ trap(E_Type); }
    it=tp(i);
    if(it==T_Lt){
      n=nn(i);
      REC(s,O_AMD);
      s->a=x; s->b=i; s->x=v; s->y=y;
      s->p=0; s->e=n;
      return;
    }
    if(xt>T_Lt){
      r=x0(x); x=r1(x);
      REC(s,O_KEY);
      s->p=xt;
      if((xt==T_Tt)&&((it&15)==T_it)){
        if(tp(y)>T_Lt){ y=Val(y); }
        s->b=r;
        Dmd_(x,l2(0,i),v,y);
        return;
      }
      r=Unq(Cat(r,rx(i)));
      s->b=r;
      Amd_(ntake(nn(r),x),Fnd(rx(r),i),v,y);
      return;
    }
    if(i==0){
      if(v==F_Dex){
        if(tp(y)<16){ y=ntake(nn(x),y); }
        dx(x);
        frame->a=y;
        return;
      }
      Cal_(v,l2(x,y));
      return;
    }
    REC(s,O_AMD2);
    s->a=y; s->x=x;
    s->b=i;
    if(tp(v)!=0 || v!=F_Dex){
      REC(r,O_CAL);
      r->b=v; r->x=y;
      Atx_(rx(x),rx(i));
    }
}
static void Dmd_(K x, K i, K v, K y){
    K f,t;
    I n;
    if(tp(x)==T_st){
      REC(s,O_ASN); s->x = x;
      Dmd_(Val(x),i,v,y);
      return;
    }
    i=explode(i);
    f=Fst(rx(i));
    if(nn(i)==1){
      dx(i);
      Amd_(x,f,v,y);
      return;
    }
    if(!f) f=seq(nn(x));
    i=ndrop(1,i);
    if(tp(f)>16){
      n=nn(f);
      if(nn(i)!=1){ trap(E_Rank); }
      i=Fst(i);
      if((tp(f)==T_It)&&(tp(x)==T_Tt)){
        t=rx(x0(x));
        REC(s,O_KEY);
        s->b=t; s->p=T_Tt;
        Dmd_(r1(x),l2(Fnd(t,i),f),v,y);
        return;
      }
      if(tp(f)!=T_It || tp(x)!=T_Lt){ trap(E_Nyi); } // TODO: implement?
      x=use(x);
      REC(s,O_DMD);
      s->p=0; s->e=n;
      s->i=(I)x; s->y=y;
      s->b=i; s->x=v;
      s->j=(I)f;
      return;
    }
    x=rx(x);
    REC(a,O_AMD1);
    a->x=x;
    a->b=f; a->y=1;
    a->p=0;
    REC(d,O_DMD1);
    d->b=i; d->x=v;
    d->y=y;
    Atx_(x,f);
    return;
}
static void Grp_(K x){
    T xt=tp(x);
    if((xt&15)==T_ct){ frame->a=splitn(x); return; }
    if(xt == T_Dt){ frame->a=grpdt(x); return; }
    if(xt < 16){ trap(E_Type); return; }
    frame->a=grpl(x);
}
static void Mtc_(K x, K y){
    frame->a=Ki(match(x,y));
    dx(x); dx(y);
}
static void Fnd_(K x, K y){
    T xt=tp(x),yt=tp(y);
    if(xt<16){
      if(yt==T_Tt) frame->a=grptt(x,y);
      else if(yt>16) frame->a=In(x,y);
      else frame->a=deal(x,y);
      return;
    }
    frame->a=Fnd(x,y);
}
static void Atx_(K x, K y){
    K r=0;
    T xt=tp(x),yt=tp(y);
    if(xt<16 && isfunc(xt)){
      cal_(x,l1(y)); return;
    }
    if(xt>T_Lt && yt<T_Lt){
      r=x0(x);
      x=r1(x);
      if(xt==T_Tt && (yt&15)==T_it){
        REC(s,O_KEY);
        s->b=r; s->p=T_Dt+(I)(yt==T_It);
        Ecl_(F_Atx,l2(x,y));
        return;
      }
      Atx_(x,Fnd(r,y));
      return;
    }
    if(yt<T_It){
      y=uptype(y,T_it);
      yt=tp(y);
    }
    if(yt==T_It){
      frame->a=atv(x,y);
      return;
    }
    if(yt==T_it){
      frame->a=ati(x,(I)y);
      return;
    }
    if(yt==T_Lt){ Ecr_(F_Atx,l2(x,y)); return; }
    if(yt==T_Dt){
      r=x0(y);
      REC(s,O_KEY);
      s->p=T_Dt; s->b=r;
      Atx_(x,r1(y));
      return;
    }
    trap(E_Type);
}
static void Sqr_(K x){
    T xt=tp(x);
    if((xt&15)!=T_ft){
      x=Add(Kf(0.), x);
    }
    frame->a=nmonad(spcSqr,x);
}
static void Asc_(K x){
    if(tp(x)==T_st){
#ifdef SPESH_IO_VERBS
      frame->a=readio(cs(x));
#else //!SPESH_IO_VERBS
      trap(E_Io);
#endif //SPESH_IO_VERBS
      return;
    }
    frame->a=Asc(x);
}
static void Cst_(K x, K y){
    if(tp(x)==T_Ct){ frame->a=Fmt(x,y); return; }
    T yt=tp(y);
    if(yt>=T_Lt){
      Ecr_(F_Cst,l2(x,y)); return;
    }
    if(tp(x)!=T_st){ trap(E_Type); return; }
    if(!(I)x){ // `$"abc" -> `abc
      if(yt==T_ct){ frame->a=sc(Enl(y)); return; }
      if(yt!=T_Ct){ trap(E_Type); return; }
      frame->a=sc(y); return;
    }
    T dst=ts(x);
    if(dst==T_it || dst==T_ft || dst==T_ct) {
      if(dst==yt){ frame->a=y; return; }
      I yp=(I)y;
      if(yt<16){
        if(yt==T_ft){ yp=(I)F64floor(xF(y)[0]); }
        switch(dst){
          case T_it: frame->a=Ki(yp); return;
          case T_ft: frame->a=Kf(yp); return;
          case T_ct: frame->a=Kc(yp); return;
        }
      } else if(yt==T_It || yt==T_Ft || yt==T_Ct){
        if(yt-16==dst){ frame->a=y; return; }
        I yn=nn(y);
        K r=mk(dst+16,yn);
        if(yt==T_Ct){ C* yp=xC(y);
          if(dst==T_it){ I* rp=xI(r);
            for(I i=0;i<yn;i++){ rp[i]=yp[i]; }
          } else { F* rp=xF(r);
            for(I i=0;i<yn;i++){ rp[i]=yp[i]; }
          }
        } else if(yt==T_It){ I* yp=xI(y);
          if(dst==T_ct){ C* rp=xC(r);
            if(rr(y)) for(I i=0,a=yp[0];i<yn;i++){ rp[i]=(C)(a+i); }
            else      for(I i=0;        i<yn;i++){ rp[i]=(C)yp[i]; }
          } else { F* rp=xF(r);
            if(rr(y)) for(I i=0,a=yp[0];i<yn;i++){ rp[i]=a+i; }
            else      for(I i=0;        i<yn;i++){ rp[i]=yp[i]; }
          }
        } else if(yt==T_Ft){ F* yp=xF(y);
          if(dst==T_it){ I* rp=xI(r);
            for(I i=0;i<yn;i++){ rp[i]=(I)F64floor(yp[i]); }
          } else { C* rp=xC(r);
            for(I i=0;i<yn;i++){ rp[i]=(C)F64floor(yp[i]); }
          }
        }
        frame->a=r; return;
      }
    }
    if(yt==T_ct){
      y=Enl(y); yt=T_Ct;
    }
    if(yt!=T_Ct) trap(E_Type);
    frame->a=prs(dst,y);
}
static void Unq_(K x){
    if(tp(x)<16) frame->a=roll(x);
    else frame->a=Unq(x);
}
static void Uqs_(K x){
    if(tp(x)<16) trap(E_Type);
    frame->a=Srt(uqf(x));
}
static void Key_(K x, K y){
    if(tp(x)==T_it) return Mod_(y,x);
    frame->a=key(x,y,T_Dt);
}
static void Tak_(K x, K y){
    K r;
    T xt=tp(x),yt=tp(y);
    if(frame==frame_end){ trap(E_Limit); return; }
    if(yt==T_Dt){
      x=rx(x);
      if(xt==T_it){
        r=x0(y); y=r1(y);
        REC(k,O_KEY); k->p=T_Dt;
        REC(p1, O_POP); // a -> b, write a
        Tak_(x,r);
        REC(p2, O_PUSH); // store a
        Tak_(x,y);
      } else {
        REC(k,O_KEY);
        k->p=T_Dt; k->b=x;
        Atx_(y,x);
      }
      return;
    } else if(yt==T_Tt){
      if((xt&15)==T_st){
        if(xt==T_st){ x=Enl(x); }
        x=rx(x);
        REC(k,O_KEY);
        k->b=x; k->p=yt;
        Atx_(y,x);
      } else {
        Ecr_(F_Tak,l2(x,y));
      }
      return;
    }
    if(xt==T_it){
      frame->a=ntake((I)x,y);
      return;
    }
    y=rx(y);
    if(xt>16 && xt==yt){
      frame->a=atv(y,Wer(In(y,x)));
      return;
    }
    REC(a,O_ATX); a->x=y;
    REC(w,O_WER);
    Cal_(x,l1(y));
}
static void Drp_(K x, K y){
    K r;
    T xt=tp(x),yt=tp(y);
    if(frame==frame_end){ trap(E_Limit); return; }
    if(yt>T_Lt){
      if(yt==T_Dt || (yt==T_Tt && (xt&15)==T_st)){
        r=x0(y); y=r1(y);
        if(xt<16){ x=Enl(x); }
        x=rx(Wer(notI(In(rx(r),x))));
        REC(k,O_KEY); k->p = yt;
        REC(p1,O_POP);
        Atx_(r,x);
        REC(p2,O_PUSH);
        Atx_(y,x);
        return;
      } else {
        Ecr_(F_Drp,l2(x,y));
        return;
      }
    }
    if(xt==T_it){ frame->a=ndrop((I)x,y); }
    else if((xt&15)==T_ct && (yt&15)==T_ct){ frame->a=trimp(x,y); }
    else if(xt>16 && xt==yt){ frame->a=atv(y,Wer(notI(In(rx(y),x)))); }
    else if(yt==T_it){ frame->a=atv(x,Wer(notI(eqle(y,seq(nn(x)),yt)))); }
    else {
      REC(a,O_ATX); a->x=y;
      REC(w,O_WER);
      REC(n,O_NOT);
      Cal_(x,l1(rx(y)));
    }
}
static void Cut_(K x, K y){
    K r;
    I xp,n;
    T yt=tp(y),xt;
    if(yt==T_it || yt==T_ft){ Pow_(y,x); return; }
    xt=tp(x);
    if(xt==T_It){ frame->a=cuts(x,y); return; }
    if(xt==T_Ct && yt==T_Ct){
      x=rx(Wer(In(rx(y),x)));
      K z=mk(T_It, nn(x)+1);
      I *xp=xI(x),*zp=xI(z);
      zp[0]=0;
      for(I i=0; i<nn(x); i++){
        zp[1+i]=1+xp[i];
      }
      frame->a=rcut(y,z,Cat(x,Ki(nn(y))));
      return;
    }
    if(xt!=T_it || yt<16) trap(E_Type);
    xp=(I)x;
    if(xp<=0){ xp=nn(y)/-xp; }
    r=mk(T_Lt,xp); K* rp=xK(r);
    n=nn(y)/xp; I rn=nn(r);
    xp=0; I i=0;
    do rp[i]=atn(rx(y),xp,n);while(xp+=n,++i<rn);
    dx(y);
    frame->a=r;
}
static void Bin_(K x, K y){
    K r=0;
    T xt=tp(x),yt=tp(y);
    if(xt<16 || xt>T_Ft){
      if(xt==T_it && yt>16){
        frame->a=win((I)x,y);
        return;
      }
      trap(E_Type);
      return;
    }
    if(xt==yt || yt==T_Lt){
      Ecr_(F_Bin,l2(x,y)); return;
    } else if(xt==yt+16){
      r=Ki(ibin(x,y,xt));
    } else {
      trap(E_Type);
    }
    dx(x); dx(y);
    frame->a=r;
}
static void Flr_(K x){
    I xn;
    K r;
    T xt=tp(x);
    if(xt<16){
      switch(xt){
        case T_ct: frame->a=Kc(lc((C)x)); return;
        case T_it: frame->a=x; return;
        case T_st: frame->a=sc(lower(cs(x))); return;
        case T_ft:
          dx(x);
          frame->a=Ki((I)F64floor(xF(x)[0]));
          return;
        default: frame->a=x; return;
      }
    }
    xn=nn(x);
    switch(xt){
      case T_Ct: r=lower(x); break;
      case T_It: r=x; break;
      case T_Ft: r=mk(T_It,xn);
        for(I i=0;i<xn;i++){ xI(r)[i]=(I)F64floor(xF(x)[i]); }
        dx(x);
        break;
      default: Ech_(F_Flr,l1(x)); return;
    }
    frame->a=r;
}
static void Fwh_(K x){
    if(tp(x)==T_It){
      dx(x);
      frame->a=Ki(fwh((I)x, nn(x)));
    } else frame->a=Fst(Wer(x));
}
static void Ech_(K f, K x){
    I xn;
    T t=tp(f),xt;
    K r=0;
    if(!isfunc(t)){
      Bin_(f,Fst(x));
      return;
    }
    if(nn(x)==1) x=Fst(x);
    else { ecn_(f,x); return; }
    if(tp(x)<16) trap(E_Type);
    xt=tp(x);
    if(xt==T_Dt){
      r=x0(x);
      REC(s,O_KEY);
      s->b=r; s->p=T_Dt;
      Ech_(f,l1(r1(x)));
      return;
    }
    if(xt==T_Tt) x=explode(x);
    xn=nn(x);
    r=mk(T_Lt,xn);
    REC(s,O_ECH);
    s->b=f;
    s->x=x; s->y=r;
    s->p=0; s->e=xn;
}

static I step_(void){
    frame_t *s = frame;
    switch(s->op){
      case O_ASN:
        s->a=Asn(s->x,s->a);
        return 1;
      case O_KEY:
        s->a=key(s->b, s->a, (T)s->p);
        return 1;
      case O_ATX: --frame;
        Atx_(s->x,s->a);
        break;
      case O_AMD1: --frame;
        Amd_(s->x,s->b,s->y,s->a);
        return 0;
      case O_AMD2: {
        T yt=tp(s->a);
        T xt=tp(s->x);
        if((xt&15)!=(yt&15)) s->x=explode(s->x), xt=T_Lt;
        if(tp(s->b)==T_it){
          if(xt!=yt+16) s->x=explode(s->x);
          s->a=sti(s->x,(I)s->b,s->a);
        } else {
          if(yt<16) s->a=ntake(nn(s->b),s->a);
          if(xt==T_Lt) s->a=explode(s->a);
          s->a=stv(s->x,s->b,s->a);
        }
        return 1;
      } break;
      case O_AMD:
        if(s->p < s->e){
          Amd_(s->a,ati(rx(s->b),s->p),rx(s->x),ati(rx(s->y),s->p));
          s->p++;
          return 0;
        }
        dx(s->b); dx(s->x); dx(s->y);
        return 1;
      case O_DMD1: --frame;
        Dmd_(s->a,s->b,s->x,s->y);
        return 0;
      case O_DMD: {
        I rp;
        if(s->p > 0){
          if(rr(s->j)) rp=xI(s->j)[0]+s->p-1;
          else rp=xI(s->j)[s->p-1];
          xK(s->i)[rp]=s->a;
        }
        if(s->p < s->e){
          if(rr(s->j)) rp=xI(s->j)[0]+s->p;
          else rp=xI(s->j)[s->p];
          Amd_(xK(s->i)[rp],rx(s->b),rx(s->x),ati(rx(s->y),s->p++));
        } else {
          s->a=(K)s->i|(K)T_Lt<<59;
          dx(((K)s->j)|(K)T_It<<59); dx(s->b); dx(s->x); dx(s->y);
          return 1;
        }
      } break;
      case O_MUL: --frame;
        Mul_(s->x,s->a);
        break;
      case O_WER: --frame;
        Wer_(s->a);
        break;
      case O_NOT: --frame;
        Not_(s->a);
        break;
      case O_PUSH: --frame;
        push(s->a);
        break;
      case O_POP:
        (frame-1)->b=s->a;
        s->a=pop();
        return 1;
      case O_FLP: --frame;
        Flp_(s->a);
        break;
      case O_LAMBDA:
        dx(pop());
        epi_();
        return 1;
      case O_TRAIN:
        if(s->p++ < s->e) cal_(x0(s->b+8),l1(s->a));
        else return 1;
        break;
      case O_ECH:
        if(s->p > 0){
          xK(s->y)[s->p-1]=s->a;
        }
        if(s->p < s->e){
          Atx_(rx(s->b),ati(rx(s->x),s->p++));
        } else {
          dx(s->b); dx(s->x);
          s->a=uf(s->y);
          return 1;
        }
        break;
      case O_RDC:
        if(s->p < s->e){
          cal_(rx(s->b),l2(s->a,ati(rx(s->x),s->p++)));
        } else {
          dx(s->x); dx(s->b);
          return 1;
        }
        break;
      case O_SCN:
        if(s->p > 1){
          xK(s->y)[s->p-1]=rx(s->a);
        }
        if(s->p < s->e){
          cal_(rx(s->b),l2(s->a,ati(rx(s->x),s->p++)));
        } else {
          dx(s->a); dx(s->x); dx(s->b);
          s->a=uf(s->y);
          return 1;
        }
        break;
      case O_ECR:
        if(s->p > 0){
          xK(s->i)[s->p-1]=s->a;
        }
        if(s->p < s->e){
          cal_(rx(s->b),l2(rx(s->x),ati(rx(s->y),s->p++)));
        } else {
          dx(s->b); dx(s->x); dx(s->y);
          s->a=uf((K)s->i|(K)T_Lt<<59);
          return 1;
        }
        break;
      case O_ECL:
        if(s->p > 0){
          xK(s->i)[s->p-1]=s->a;
        }
        if(s->p < s->e){
          cal_(rx(s->b),l2(ati(rx(s->x),s->p++),rx(s->y)));
        } else {
          dx(s->b); dx(s->x); dx(s->y);
          s->a=uf((K)s->i|(K)T_Lt<<59);
          return 1;
        }
        break;
      case O_EC2:
        if(s->p > 0){
          xK(s->i)[s->p-1]=s->a;
        }
        if(s->p < s->e){
          Cal_(rx(s->b),l2(ati(rx(s->x),s->p),ati(rx(s->y),s->p)));
          s->p++;
        } else {
          dx(s->b); dx(s->x); dx(s->y);
          s->a=uf((K)s->i|(K)T_Lt<<59);
          return 1;
        }
        break;
      case O_RDN:
        if(s->p > 0 && s->y != 0){
          s->y=cat1(s->y,rx(s->a));
        }
        if(s->p < s->e){
          Cal_(rx(s->b),Cat(l1(s->a),ati(rx(s->x),s->p++)));
        } else {
          dx(s->b); dx(s->x);
          if(s->y!=0){
            dx(s->a);
            s->a=uf(s->y);
          } return 1;
        }
        break;
      case O_FIX:
        if(match(s->a,s->x) || match(s->a,s->y)){
          dx(s->b); dx(s->a); dx(s->y);
          if(s->j){ dx(s->x); s->a=(K)s->i|(K)s->j<<59; }
          else s->a=s->x;
          return 1;
        }
        dx(s->x);
        s->x=s->a;
        if(s->j) s->i=(I)cat1((K)s->i|(K)s->j<<59,rx(s->x));
        Atx_(rx(s->b),rx(s->x));
        break;
      case O_CAL: --frame;
        cal_(s->b, l2(s->a, s->x));
        return 0;
      default:
        trap(E_Err);
        break;
    }
    return 0;
}

static void zfuncs(void){
    #define ZF(n,f) dx(Asn(sc(KC(n)),f));
    ZF("sin",F_Sin) ZF("cos",F_Cos) ZF("exp",F_Exp) ZF("log",F_Log) ZF("mod",F_Mod)
}

static void *Fn_[96]={
#define $ (void*)
#define ____ nyi
#define _XX_ nyi
    $ nul_,$ Idy_,$ Flp_,$ Neg_,$ Fst_,$ Sqr_,$ Wer_,$ Rev_,$ Asc_,$ Dsc_,$ Grp_,$ Not_,$ Til_,$ Enl_,$ Srt_,$ Cnt_,
    $ Flr_,$ Str_,$ Unq_,$ Typ_,$ Val_,$ _XX_,$ _XX_,$ _XX_,$ Fwh_,$ Las_,$ Exp_,$ lst_,$ Uqs_,$ Log_,$ Sin_,$ Cos_,
    // 32
    $ Asn_,$ Dex_,$ Add_,$ Sub_,$ Mul_,$ Div_,$ Min_,$ Max_,$ Les_,$ Mor_,$ Eql_,$ Mtc_,$ Key_,$ Cat_,$ Cut_,$ Tak_,
    $ Drp_,$ Cst_,$ Fnd_,$ Atx_,$ Cal_,$ _XX_,$ _XX_,$ _XX_,$ Bin_,$ Mod_,$ Pow_,$ com_,$ prj_,$ Lgn_,$ In_ ,$ ____,
    // 64
    $ ____,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,
    $ ____,$ ____,$ ____,$ Amd_,$ Dmd_,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,$ ____,
};

static void cal_(K f, K x){
    I fp=(I)f,xn=nn(x),a;
    K r=0,z=0,y,d;
    T t=tp(f);
    if(t<T_df){ // 0(verb) or T_cf(train)
      switch(xn-1){
        case 0: x=Fst(x);
          break;
        case 1: r=x1(x); x=r0(x);
          break;
        case 2: r=x1(x); z=x2(x); x=r0(x);
          break;
      }
    }
    switch(t){
      case 0:
        switch(xn-1){
          case 0:
            if     (fp==F_ech) r=df_ech(x);
            else if(fp==F_rdc) r=df_rdc(x);
            else if(fp==F_scn) r=df_scn(x);
            else {
              ((void(*)(K))Fn_[fp])(x);
              return;
            }
            break;
          case 1:
            ((void(*)(K,K))Fn_[fp+32])(x,r);
            return;
          case 2:
            ((void(*)(K,K,K,K))Fn_[fp+64])(x,r,1,z);
            return;
          case 3:
            r=x0(x); y=x1(x); z=x2(x);
            ((void(*)(K,K,K,K))Fn_[fp+64])(r,y,z,r3(x));
            return;
          default: trap(E_Rank); return;
        }
        break;
      case T_cf:
        switch(xn-1){
          case 0: train_(f,x,0); return;
          case 1: train_(f,x,r); return;
          default: trap(E_Rank); return;
        }
        break;
      case T_df:
        d=x0(f);
        a=F_Ech+(I)xK(fp)[1];
        if(a == F_Rdc) Rdc_(d,x);
        else if(a == F_Ech) Ech_(d,x);
        else if(a == F_Scn) Scn_(d,x);
        dx(f);
        return;
      case T_pf: // prj
        {
          I fn=nn(f);
          if(fn!=xn){
            if(xn<fn) prj_(rx(f),x);
            else trap(E_Rank);
          } else {
            Cal_(x0(f), stv(x1(f),x2(f),x));
          }
          dx(f);
          return;
        }
        break;
      case T_lf: lambda_(f,x); return;
      case T_xf: native_(f,x); return;
      default: trap(E_Type); return;
    }
    dx(f);
    frame->a=r;
    return;
}

static I step(void){
    frame_t *s = frame;
    if(s->op){
      if(step_()){
        (--frame)->a=s->a;
        if(frame->op) return 1;
      } else return 1;
    } else if(IP<IE){
      K u=xK(IP)[0];
      IP+=8;
      if(tp(u)){ // literal
        push(s->a);
        s->a=rx(u);
      } else { // run instruction
        switch((I)(u)>>5){
        case 0: // 0..31 monadic
          if     ((I)u==F_ech) s->a=df_ech(s->a);
          else if((I)u==F_rdc) s->a=df_rdc(s->a);
          else if((I)u==F_scn) s->a=df_scn(s->a);
          else {
            ((void(*)(K))Fn_[(I)u])(s->a);
            return 1;
          }
          break;
        case 1: // 32..63 dyadic
          ((void(*)(K,K))Fn_[(I)u])(s->a,pop());
          return 1;
        case 2: // 64..95 tetradic
          s->b=pop();
          s->x=pop();
          ((void(*)(K,K,K,K))Fn_[(I)u])(s->a,s->b,s->x,pop());
          return 1;
        case 3: // 96 dyadic indirect
          s->b=pop();
          Cal_(s->a,l2(s->b,pop()));
          return 1;
        case 4: // 128 drop
          dx(s->a);
          s->a=pop();
          break;
        case 5: // 160 jmp
          if(u!=160){ // tail/return @(161) .(162)
            s->b=pop();
            if(tp(s->a)==T_lf){
              if(u==161) s->b=l1(s->b); // :f@x -> :f.,x
              --frame;
              pop(); epi_();
              --frame;
              lambda_(s->a,s->b);
              return 1;
            }
            if(s->a != 0){
              --frame;
              Cal_(s->a,s->b);
              return 1;
            }
          } else {
            IP+=(I)s->a;
            s->a=pop();
          }
          break;
        case 6: // 192 jif
          u=pop();
          s->j=0;
          if((I)(u)==0){ IP+=(I)(s->a); s->j=1; }
          dx(u);
          s->a=pop();
          break;
        case 7: // 224 jnj
          if(s->j==0){ IP+=(I)(s->a); }
          dx(s->a);
          s->a=pop();
          break;
        case 8: s->j=0; break; // 256 snj
        default: // 288+ quoted verb
          push(s->a);
          s->a=rx(unquote(u));
          break;
        }
      }
    }
    if(IP<IE) return 1;
    if(frame>&frames[0]){
      --frame;
      frame->a = s->a;
      return 1;
    }
    return 0;
}

I kinit(void){
    K x,y,z;
    if(!Memory()) return 0;

    SP=POS_STACK;
    xK(POS_SRC)[0]=mk(T_Ct,0);
    loc_=0;
    mod_=0; key_=mk(T_Lt,0);
    for(I m=0;m<SPESH_MAX_MODULES;++m) val_[m]=mk(T_Lt,0);
    sc(Ku(0));
    x=sc(Ku('x'));
    y=sc(Ku('y'));
    z=sc(Ku('z'));
    xyz_=cat1(Cat(x,y),z);
    zfuncs();
    return 1;
}

I spesh_init(void){
    math_init();
    rand_=1592653589;
    return kinit();
}

void kfree(void){
    free(U_);
    U_=0; I_=0; M_=0;
    memorysize_=0; memorycount_=0;
    frame=&frames[0];
    ERR[0]=0;
    loc_=0; xyz_=0;
    key_=0; mod_=0;
    for(I m=0;m<SPESH_MAX_MODULES;++m) val_[m]=0;
    IP=IE=PP=PE=SP=SRCP=PS=0;
}

#ifdef SPESH_NATIVE_C
// extend (register external function)
void KR(const C *name, void *fp, int arity){
    K s=KC(name);
    K r=l2(KCn((C *)&fp, 8), rx(s));
    xI(r)[-3]=(I)arity;
    dx(Asn(sc(s),((K)(I)r) | (((K)T_xf)<<59)));
}

K Native(K x, K y){
    I n=nn(y);
    void **p=(void **)(M_+(I)x);
    void *f=*p;
    rl(y); dx(y);
    K *m=(K*)(M_+(I)y);
    switch(n){
      case 0: return ((K(*)())f)();
      case 1: return ((K(*)(K))f)(m[0]);
      case 2: return ((K(*)(K,K))f)(m[0],m[1]);
      case 3: return ((K(*)(K,K,K))f)(m[0],m[1],m[2]);
      case 4: return ((K(*)(K,K,K,K))f)(m[0],m[1],m[2],m[3]);
      case 5: return ((K(*)(K,K,K,K,K))f)(m[0],m[1],m[2],m[3],m[4]);
      case 6: return ((K(*)(K,K,K,K,K,K))f)(m[0],m[1],m[2],m[3],m[4],m[5]);
      case 7: return ((K(*)(K,K,K,K,K,K,K))f)(m[0],m[1],m[2],m[3],m[4],m[5],m[6]);
      case 8: return ((K(*)(K,K,K,K,K,K,K,K))f)(m[0],m[1],m[2],m[3],m[4],m[5],m[6],m[7]);
      case 9: return ((K(*)(K,K,K,K,K,K,K,K,K))f)(m[0],m[1],m[2],m[3],m[4],m[5],m[6],m[7],m[8]);
    }
    return trap(E_Nyi);
}
#elif !defined(SPESH_NATIVE_DIY)
K Native(K x, K y){ return 0*(x+y); }
#endif //SPESH_NATIVE
