CC?=gcc
CXX?=g++
CFLAGS+= -std=c99 -g -Wall -Wextra -fsanitize=address -fno-omit-frame-pointer -Walloc-zero -Wcast-qual -Wconversion -Wformat=2 -O2
CXXFLAGS+= -std=c++17 -Wwrite-strings

spesh: examples/repl.c spesh.h Makefile
	$(CC) $(CFLAGS) $< -o $@
cinterface: examples/interface.c spesh.h Makefile
	$(CC) $(CFLAGS) $< -o $@
cpplib: examples/cpp_lib.cpp spesh.h Makefile
	$(CXX) $(CXXFLAGS) $< -o $@
test: spesh
	@chmod +x ./scripts/test.sh
	@./scripts/test.sh ./spesh
clean:
	@rm -f spesh cinterface cpplib

