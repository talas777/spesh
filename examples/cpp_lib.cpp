// Minimal C++ Usage Example
// License: CC0 (Public Domain)
#define SPESH_NATIVE_C
extern "C" {
#include "../spesh.h"
#include "../speshio.h"
}
#include <string>
#include <iostream>
#include <stdexcept>

void panic(I et) { (void)et; throw(std::runtime_error("panic")); }

K hello(K x){
    T t=tp(x);
    if(t==T_It) std::cout << "Hello int list: ";
    else if(t==T_Ct) std::cout << "Hello string: ";
    else std::cout << "Hello something else: ";
    dx(Out(x));
    return x;
}

int main(int args, char **argv){
    (void)args; (void)argv;
    spesh_init();

    // test freeing memory
    kfree(); kinit();
    kfree(); kinit();

    KR("hello", (void*)hello, 1);

    std::string code = "hello[{x+4}'1 2 3]";
    try {
      K r = eval(KC(code.c_str()));
      dx(Out(r));
    } catch(std::runtime_error& e){
      std::cout << "Caught error in Spesh call" << std::endl;
      std::cout << ERR << std::endl;
      return 1;
    }
    return 0;
}
