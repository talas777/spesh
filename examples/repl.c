// Minimal Spesh Terminal
// License: CC0 (Public Domain)
#define SPESH_NATIVE_C
#define SPESH_IO_VERBS
#include "../spesh.h"
#include "../speshio.h"
#define Exit exit
#include<setjmp.h>

static int    args_;
static char **argv_;
static jmp_buf jb_;
static jmp_buf tjb_;
I errortest=0;
I err=0;
UJ* U2_;

void panic(I e){
    err=e;
    printf("%s", ERR);
    if(errortest) longjmp(tjb_,1);
    else longjmp(jb_,1);
}

K readio(K x){
    return readfile(x);
}

K writeio(K x, K y){
    if((I)x) return writefile(x,y);
    write(rx(y)); return y;
}

I usedmem(void){
    return memorycount_<16?1:1<<(memorycount_-16);
}

K key2_;
K val2_[SPESH_MAX_MODULES+1];
I mod2_;

void store(void){
    if(!U2_) U2_=(UJ*)calloc((size_t)memorysize_,64*1024);
    memcpy(U2_, U_, 64*1024*(size_t)(usedmem()));
    key2_=key_; mod2_=mod_;
    for(I m=0;m<SPESH_MAX_MODULES;++m) val2_[m]=val_[m];
}

void restore(void){
    memcpy(U_, U2_, 64*1024*(size_t)(usedmem()));
    SP=POS_STACK; err=0; frame=&frames[0];
    key_=key2_; mod_=mod2_;
    for(I m=0;m<SPESH_MAX_MODULES;++m) val_[m]=val2_[m];
}

static I Arg(I i, I r){
    if(i>=args_) return 0;
    if(r ==   0) return (I)strlen(argv_[i]);
    memcpy(M_+r,argv_[i],strlen(argv_[i]));
    return 0;
}

static K getargv(void){
    K r,s;
    I n=args_;
    r=mk(T_Lt,n);
    for(I i=0; i<n; i++){
      s=mk(T_Ct,Arg(i,0));
      Arg(i,(I)s);
      xK(r)[i]=s;
    } return r;
}

static void test(K x);
static void etest(K x);
static void dofile(K x, K c){
    K kk=KC(".k"); K tt=KC(".t"); K ee=KC(".e");
    K xe=ntake(-2,rx(x)); // file extension (hopefully)
    if(match(xe,kk)){ dx(eval(c)); } // file .k (execute)
    else if(match(xe,tt)){ test(c); } // file .t (test)
    else if(match(xe,ee)){ etest(c); } // file .e (error test)
    else { dx(Asn(sc(rx(x)),c)); } // file (assign file:bytes..)
    dx(xe); dx(x); dx(tt); dx(kk);
}

static K Lst(K x){
    /*
    `"pad.":{x@\\!|/#'x}
    `"lxy.":{ /k
    kt:{[x;y;k;T]x:$[`T~@x;T[x;k];`pad("";"-"),$x];(x,'"|"),'T[y;k]}
    d:{[x;k;kt;T]r:!x;x:.x;$[`T~@x;kt[r;x;k;T];,'[,'[`pad(k'r);"|"];k'x]]}
    T:{[x;k]$[`L?@'.x;,k x;(,*x),(,(#*x)#"-"),1_x:" "/'+`pad@'$(!x),'.x]}
    t:@y;
    k:`kxy@*x;h:*|x
    dd:("";,"..")h<#y:$[(@y)?`L`D`T;y;y~*y;y;[t:`L;,y]]
    y:$[y~*y;y;(h&#y)#y]
    $[`D~t;d[y;k;kt;T];`T~t;T[y;k];y~*y;,k y;k'y],dd}
    `"l.":`lxy 70 20
    */
    // Should return T_Lt of T_Ct (list of strings)
    return Enl(Kst(x));
}

void dw(K x){
    write(rx(x)); dx(x);
}

void repl(K x){
    I n=nn(x),s=0,c;
    if(n>0){
      s=xC(x)[0];
      if(s=='\\' && n>1){
        c=xC(x)[1];
        if(c=='\\'){
          Exit(0);
        } else if(c=='m'){ // mem usage
          dx(x); dx(Out(Ki(usedmem())));
        } else if(c=='M'){ // module info
          dx(x); dx(Out(Ki(mod_)));
        } else if(c=='v'){ // variables
          dx(x);
          I kn=nn(key_);
          K r=mk(T_Ct, 0);
          for(I i=0, n=0; i<kn; i++){
            if(!xK(val_[mod_])[i]){ continue; }
            if(n++){ r=ucat(r, Ku(' ')); }
            r=ucat(r, rx(xK(key_)[i]));
          }
          dx(Out(rx(r)));
        } else if(c=='p'){ // debug parse
          K r=mk(T_Ct, 0);
          I i=(xC(x)[2]==' ') ? 3 : 2;
          for(; i<n; i++){
            r=cat1(r, rx((K)xC(x)[i]));
          } dx(x);
          x=parse(tok(r));
          dx(Out(x)); dx(x);
        } else if(c=='h'){ // refcard help
          dx(x);
          dw(KC("+ flp add        ' each both bin win        c char     \"x\"   \"ab\"\n"));
          dw(KC("- neg sub        / over eachr fix           i int      2     1 2\n"));
          dw(KC("* fst mul        \\ scan eachl fixs          s symbol   `a    ``c`d\n"));
          dw(KC("% sqr div        / join decode              f float    2.    1. 2.\n"));
          dw(KC("! til key mod    \\ split encode             L list     (1;2 3)\n"));
          dw(KC("& wer min                                   D dict     `a`b!1 2)\n"));
          dw(KC("| rev max abs    $[cond;x;...]              T table    +`a`b!..\n"));
          dw(KC("< asc les        while[c;x;..]              v verb     +\n"));
          dw(KC("> dsc mor        if[c] elif[x] else[y]      m comp     1+/*%\n"));
          dw(KC("= grp eql                                   d derived  +/\n"));
          dw(KC("~ not mtc        @[x;i;+;y]      amend      p proj     1+\n"));
          dw(KC(", enl cat        .[x;i;+;y]      dmend      l lambda   {x+y}\n"));
          dw(KC("^ srt cut        k?t             group      x native   c-extension\n"));
          dw(KC("# cnt tak        k!t               key\n"));
          dw(KC("_ flr drp        t,d t,t t,'t(h)  join\n"));
          dw(KC("$ str cst\n"));
          dw(KC("? unq fnd in     c:<`file         read\n"));
          dw(KC("@ typ atx        `file<c         write\n"));
          dw(KC(". val cal\n"));
          dw(KC("\n"));
          dw(KC("sin cos exp log find\n"));
          dw(KC("rnd: ?n(uniform) n?n(with) -n?n(w/o) n?L\n"));
        }
        return;
      }
    }
    x=eval(x);
    if(x==0ull){ return; }
    if(s==' '){ // skip formatting with Lst
      dx(Out(x));
    } else {
      write(cat1(join(Kc('\n'),Lst(x)),Kc('\n')));
    }
}

static void doargs(void){
    K a=ndrop(1,getargv()),x;
    I an=nn(a);
    K ee=KC("-e");
    for(I i=0; i<an; i++){
      x=rx(xK(a)[i]);
      if(match(x,ee)){ // -e (exit)
        if(i<an-1){
          dx(x); dx(ee);
          if(!setjmp(jb_)){ repl(rx(xK(a)[i+1])); }
        }
        Exit(err);
      }
      if(!setjmp(jb_)){ dofile(x,readfile(rx(x))); }
    }
    dx(ee);
}

static void testi(K l, I i){
    K x,y;
    x=split(KCn(" /",2),ati(split(Kc('\n'),l),i));
    if(nn(x)!=2){ trap(E_Length); }
    y=x1(x); x=r0(x);
    dx(Out(ucat(ucat(rx(x),KCn(" /",2)),rx(y))));
    x=Kst(eval(x));
    if(!match(x,y)){
      x=Out(x); trap(E_Err);
    }
    if(SP!=POS_STACK) trap(E_Stack);
    if(frame!=&frames[0]) trap(E_Limit);
    dx(x); dx(y);
}

static void test(K x){
    if(tp(x)!=T_Ct){ trap(E_Type); }
    K l=ndrop(-1,split(Kc('\n'),rx(x)));
    I n=nn(l);
    dx(l);
    for(I i=0;i<n;i++) testi(rx(x),i);
    dx(x);
}

static void etesti(K l, I i){
    K x,y; I e;
    x=split(KCn(" /",2),ati(split(Kc('\n'),l),i));
    if(nn(x)!=2) trap(E_Length);
    y=x1(x); x=r0(x);
    dx(Out(ucat(ucat(rx(x),KCn(" /",2)),rx(y))));
    store();
    errortest=1;
    if(!setjmp(tjb_)){ (void)eval(x); }
    errortest=0;
    e=err;
    restore();
    dx(x);
    switch(e){
      case E_Err: x=KC("Err"); break;
      case E_Type: x=KC("Type"); break;
      case E_Value: x=KC("Value"); break;
      case E_Index: x=KC("Index"); break;
      case E_Length: x=KC("Length"); break;
      case E_Rank: x=KC("Rank"); break;
      case E_Parse: x=KC("Parse"); break;
      case E_Stack: x=KC("Stack"); break;
      case E_Grow: x=KC("Grow"); break;
      case E_Unref: x=KC("Unref"); break;
      case E_Io: x=KC("Io"); break;
      case E_Limit: x=KC("Limit"); break;
      case E_Nyi: x=KC("Nyi"); break;
      default: trap(E_Err); return;
    }
    if(!match(x,y)){
      x=Out(x); trap(E_Err);
    }
    err=0;
    dx(x); dx(y);
}

static void etest(K x){
    if(tp(x)!=T_Ct) trap(E_Type);
    K l=ndrop(-1,split(Kc('\n'),rx(x)));
    I n=nn(l);
    dx(l);
    for(I i=0;i<n;i++) etesti(rx(x),i);
    dx(x);
}

K hello(K x){
    printf("Hello %i\n", (I)x);
    return x;
}
K nadd(K x, K y){
    return Neg(Add(x, y));
}
K module(K x){
    if(tp(x)!=T_it)trap(E_Type);
    if((I)x<0||(I)x>=SPESH_MAX_MODULES)trap(E_Limit);
    mod_=(I)x;
    return (K)0;
}

static K read_stdin(void){
    K r=mk(T_Ct,504);
    return ntake(ReadIn((I)r,504),r);
}

int main(int args, char **argv){
    K x;
    args_=(I)args; argv_=argv;
    if(!spesh_init()){ return 1; }
    KR("hello", (void*)hello, 1);
    KR("nadd", (void*)nadd, 2);
    KR("module", (void*)module, 1);
    doargs();
    write(KC("spesh/k\n"));
    store();
    for(;;){ // The repl loop
      write(Ku(' '));
      x=read_stdin();
      if(!nn(x)){ write(Ku('\n')); break; }
      if(!setjmp(jb_)){ repl(x); store(); }
      if(err){ restore(); }
    }
    kfree();
    return 0;
}
