// Spesh Interface Example
// License: CC0 (Public Domain)
#define SPESH_NATIVE_C
#include "../spesh.h"
#include "../speshio.h"
#include<setjmp.h>

static jmp_buf jb_;
int err=0;

void panic(I et) { (void)et; err=1; longjmp(jb_,1); }

static K call_spesh(K c){
    return eval(c);
}

static I equalC(char* c, I x, I n){
    for(I i=0; i<n; i++){
      if(xC(x)[i]!=c[i]) return 0;
    } return 1;
}

K get_callable(char* c){
    K k,v;
    I kn=nn(key_);
    I cn=(I)strlen(c);
    K *kp=xK(key_),*vp=xK(val_[mod_]);
    for(I i=0; i<kn; i++){
      v=vp[i];
      if(!v || tp(v)<T_cf || tp(v)>T_xf) continue;
      k=kp[i];
      if(nn(k)!=cn || !equalC(c, (I)k, cn)) continue;
      return v;
    }
    return (K)0;
}

int get_arity(K callable){
    return (int)nn(callable);
}

K hello(K x){
    T t=tp(x);
    if(t==T_It) printf("Hello int list: ");
    else if(t==T_Ct) printf("Hello string: ");
    else printf("Hello something else: ");
    dx(Out(x));
    return x;
}
K nadd(K x, K y){
    return Neg(Add(x, y));
}

int on_trigger(char* trigger_name, int args, char **argv){
    // Look for callback
    K cb=get_callable(trigger_name);
    if(!cb){ return 0; } // This is OK
    int arity = get_arity(cb);
    if(arity>args){ return 1; }

    // Create callback expression
    char call[255];
    size_t c=strlen(trigger_name);
    size_t l;
    memcpy(call, trigger_name, ++c);
    call[c++]='[';
    int n=0;
    while(arity>n){
      if(n)call[c++]=';';
      l=strlen(argv[n]);
      if(c+l>250){ return 1; }
      memcpy(call+c, argv[n], l);
      c+=l; n++;
    }
    call[c++]=']';

    // Do callback
    K r;
    if(!setjmp(jb_)){
      r=call_spesh( KCn(call, (I)c) );
      printf("Result: "); dx(Out(r));
    }
    return err;
}

int load_script(char* file){
    K f=KC(file);
    if(!setjmp(jb_)){ dx(call_spesh(readfile(f))); }
    return !err;
}

int main(int args, char **argv){
    (void)args; (void)argv;
    char* cb_name="mycallback";
    char* cb_argv[2]={"\"Test\"", "1 2 3"};
    spesh_init();

    // test freeing memory
    kfree(); kinit();

    // Register native functions
    KR("hello", (void*)hello, 1);
    KR("nadd", (void*)nadd, 2);

    if(!load_script("examples/interface.k")){
      printf("Failed to load script\n");
      return 1;
    }

    (void)on_trigger(cb_name, 2, cb_argv);

    return 0;
}
