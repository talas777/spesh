// Convenience input / output functions
// License: CC0 (Public Domain)
#include "stdio.h"
static I Read(I file, I nfile, I dst){
    static C *filebuf = NULL;
    static size_t      n = 0;
    if(dst != 0){ memcpy(M_+dst,filebuf,n); return 0; }
    C name[512];
    if(nfile>511) return -1;
    memcpy(name, M_+file, (size_t)nfile);
    name[nfile]=(C)0;
    FILE *fp = fopen(name, "rb");
    if(fp==NULL){ if(filebuf!=NULL)free(filebuf); filebuf=NULL; n=0; return -1; }
    fseek(fp, 0, SEEK_END);
    n=(size_t)ftell(fp);
    fseek(fp, 0, SEEK_SET);
    if(filebuf!=NULL){ free(filebuf); filebuf=NULL; }
    filebuf = (C*)malloc(n);
    if(n!=fread(filebuf,1,n,fp)){ fclose(fp); return -1; }
    fclose(fp);
    return (I)n;
}

static I Write(I file, I nfile, I src, I n){
    if(nfile==0){ fwrite(M_+src, 1, (size_t)n, stdout); return 0; }
    C name[512];
    memcpy(name, M_+file, (size_t)nfile);
    name[nfile]=(C)0;
    FILE *fp = fopen(name, "wb");
    if(fp==NULL){ return -1; }
    fwrite(M_+src, 1, (size_t)n, fp);
    fclose(fp);
    return 0;
}

static I ReadIn(I dst, I n){
    C *r = fgets(M_+dst, n, stdin);
    if(r==NULL){ //TODO: eof
      return 0;
    } else {
      const C* found = (const C*)memchr(M_+dst, '\0', (size_t)n);
      return found ? (I)(found-(M_+dst)) : n;
    }
}

static K readfile(K x){
    K r=0;
    if(!nn(x)){
      r=mk(T_Ct,496);
      return ntake(ReadIn((I)r,496),r);
    }
    I n=Read((I)x,nn(x),0);
    if(n<0){
      dx(x); return mk(T_Ct,0);
    }
    r=mk(T_Ct,n);
    Read((I)x,nn(x),(I)r);
    dx(x); return r;
}

static K writefile(K x, K y){
    I r=Write((I)x, nn(x), (I)y, nn(y));
    if(r){ trap(E_Io); }
    dx(x); return y;
}

static void write(K x){
    Write(0,0,(I)x,nn(x));
    dx(x);
}

static K Out(K x){
    write(cat1(Kst(rx(x)),Kc('\n')));
    return x;
}

