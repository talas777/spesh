"a"+"b" /"ab"
"ab"+"c" /"abc"
"a"+"bc" /"abc"
"ab"+"cd" /"abcd"
"a"-"b" /"a"
"a"-"a" /""
"abc"-"c" /"ab"
"abc"-"bc" /,"a"
"a"-"aa" /"a"
"a"_"b" /"b"
"a"_"a" /""
"a"_"abc" /"bc"
"ab"_"abc" /,"c"
"aa"_"a" /"a"
="a\nbc\ndef" /(,"a";"bc";"def")
!"a  bc def  " /(,"a";"bc";"def")
"aaabcabca"?"ab" /2 5
"aaaaaaa"?"aa" /0 2 4
"xab"?"ab" /,1
""?"ab" /!0
"ab"?"" /!0
"abc"?"al" /!0
"hei"*2 /"heihei"
"b"*3 /"bbb"
5!"ok" /"ok   "
-5!"ok" /"   ok"
-" ok! \n  " /" ok!"
